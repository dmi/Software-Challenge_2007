Software Challenge
==================

The Software Challenge a.k.a. "b+m Software Challenge" or "Software Challenge Germany" is an annual programming competition for teams from German high schools ("Gymnasiale Oberstufe").
It is held yearly since 2004, when it started with only one school, while nowadays, there are usually about 100 teams per season.

<img src="About/ProfSchimmler.jpg" height="100">
<img src="About/ModeratedGame.jpg" height="100">
<img src="About/Software-Challenge_09_klein.jpg" height="100">
<img src="About/Software-Challenge_22_klein.jpg" height="100">
<img src="About/Software-Challenge_25_klein.jpg" height="100">

In each year, a chess-like game (which may also include small elements of chance) is chosen and the teams (with a minimum size of 1 person) have about 4 months to prepare a first algorithm that can play the game.
Usually starting after Christmas, these algorithms play against each other in a league system comparable to soccer leagues, where the best teams qualify for the final tournament. Here, in a play-offs competition that is usually held in a public location in Kiel, the best player is determined. Students can win monetary prices for their school and scholarships for computer science studies at the University of Kiel sponsored by various companies.
During all phases of the competition, teams are allowed to submit improved versions of their algorithms at any time, which guarantees the evolution of better players towards the end of the season.


Software Challenge 2007
=======================

In 2007, the chosen game was "Packeis am Pol" a.k.a. "[Hey, That's My Fish](https://en.wikipedia.org/wiki/Hey,_That%27s_My_Fish!)" and 26 teams from various cities of Schleswig-Holstein participated (about 40 applied).

<img src="About/2007_GamePresentation.jpg" height="100">
<img src="About/2007_Development.jpg" height="100">
<img src="About/2007_IslandDetection.jpg" height="100">
<img src="About/2007_Scholarship_DanielMissal.jpg" height="100">
<img src="About/Software-Challenge_23_klein.jpg" height="100">

I developed the algorithm for the Käthe-Kollwitz-Schule Kiel and managed to pass the qualification league at rank 7 of 26. Finally, after severe improvements of the algorithm,
I won the play-offs tournament beating the team of Gymnasium Elmschenhagen 3:0 and could later also beat the winner of the corresponding competition for students at University of Kiel in an official comparison match (3:1).  
I won a scholarship to study computer science for one semester at University of Kiel and won 1000 € for the Käthe-Kollwitz-Schule.  
The following German article was printed in the "Kieler Nachrichten" at 05 June 2007, reporting about the final tournament held publicly at the "Sophienhof" Kiel.
<img src="GermanNewspaperArticles/FinalArticle.jpg" height="437" width="1332">



The Algorithm
-------------

The final version of my algorithm, "Sophomore 8.01", which includes heuristics for field strengths, enemy-caption, cluster-detection, an adapted multilevel MinMax-algorithm and finally multiple brute force engines for the late part of the game, is included in the source-file [PlayingAlgorithm/Sophomore 8.01.dpr](PlayingAlgorithm/Sophomore 8.01.dpr).  
It can be compiled using the free cross-platform visual IDE [Lazarus](https://en.wikipedia.org/wiki/Lazarus_(software)).  
A precompiled version for Microsoft Windows is included in the folder

"CustomServer v4.3 online/Clients"

and can be used as described in the following section.



How to Play a Game
------------------

The folder "CustomServer v4.3 online" contains a user friendly framework written by Wolfgang Pappa to play games with humans and computer algorithms on Microsoft Windows.
Use the executable file "CustomServer.exe" to open the user interface.
In the settings menu, the players (humans or algorithms) can be set up.
