object Form_Server: TForm_Server
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Packeis am Pol -  Server'
  ClientHeight = 578
  ClientWidth = 791
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001002020000000000000A80800001600000028000000200000004000
    0000010008000000000080040000000000000000000000000000000000000000
    000000010400000405000105060000020B000A0B0B0010100F0000011300000C
    130000021A00000B18000110120001151A0000191E001816150012121B00191F
    1F00000424000008260000022B000006280000052F0000082C00021922000316
    29001A1D2400191C2B0000063100000A3400000B3B000B153F0010193D000423
    2C001F292C0004263200062C3600052D39001F24390005313D0008313C001130
    38002423230027292B00282B37002A3439003435350034363B003B3B3C004841
    3E00000D420000064D00000D4A00051143000B16400002104B000D194D00000C
    54000011530001135800151E500016214D00093542000738470009394500073D
    4B00093D4A00133A4400103D48001E2851001723580020274500242A4500232B
    4C00273D42003D3E43003A3E4C002E345A00353B5800383E5800000F6200000D
    6D000014630000166E0001186E0000127700011A750000137D00011A7C000B40
    4E0014404A001C414A000B435100104E5F0027444B002B454B00364245003F43
    450022495300284951003A4551003C4C510021505C000D596C001A5C6C002057
    65003D4773001B6375002E606C00386E7A004645440042494B004C4B4B005151
    4E00494F5900465255004D5252004E56580054535300585757005A5A56005358
    59005B5B5B00605E5D0060605F00424960004D526100515665005F5F60005D68
    6A005D627F004B717B0058737B0063626200696564006C6C6C0071706D006B6D
    7200737474007B7C7C00021C840000198F00001F920000159A000F2484000320
    8C000321940005259D00273A8800001FA1000526A1000022A7000124A7000628
    A5000829A5000226AD000429AF000A2BAA001736A9000124B0000329B3000529
    B0000228B500052AB4000A2DB2000125B900052BBC00092EB9000D30B6000B30
    B9001435B0003A468300166D83001770870017748B001A748A00157D95001D7A
    9100187E98002D44A000052DC400052ECB000C34C6000730CF000A32CC00163C
    C500153CCE000633DA000A35DB000E3ADB00153DD5000E3AE2000D3CED00113F
    EE000F3EF2001E45D7001B44DD002348CE00284DCB00214AD600224ADF002B53
    D500325AD8001540E1001442EB001A46E8001443F4001B49F5001243F9001949
    FC00274FE800214DF400204EFB002C56F7002353FC002957FB002D5BFE00305D
    F6003462FC003E67F9003968FE00466EDE00426DF300406FFE004470FF004875
    FD004E79FD001C849E00228396001D94B4002391AC00249EBD002CA5C30025A7
    C8002BABCC0037B4C80029B1D3002EB7DB002DB9DC0034B9DB0038BFE1005A87
    FF005F8DFF008182810084868D00969693009FA09E009294A2009FA0A000A2A2
    A300AAA9A800B6B6B700B8B7B300B8B9C100C8C8C800D6D5D100DCDCDA000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000004000400000004000000000000000000000000
    00000000000000000000000C222741413D273F270C0000000000000000000000
    00000000000000000000175B585B5B5B41415941200000000000000000000000
    00000000000000000000275B41585B413D3F413F200800000000000000000000
    00000000000004182724244141415B413F3F413D230800000000000000000000
    00000000000B26415B5B263F595A5A5A423F3F27200400000009000000000000
    000000000A415B5B41596264737373736E5F49280D0000001B1D000000000000
    000000005BAB5B5B6274798479797975756F6F2F2A06001C391D000000000000
    00000826ADE6AE61788584847F7B7976756F6E302F1A1D393616000000000000
    000026E4EAE5EBB0657284848479797575706F4A2531361D1613000000000000
    000017AFE3AEEEE9B05C5E7884847A77734B461E1D311D121211000000000000
    00000004196CEBEEE7E2663D5E744D4437333131341C16121607000000000000
    000000053082E8EFEEE7AE435E634439333631311D1616121100000000000000
    000000057583E6EDEFE7682A2F7970473336341D1C1616120900000000000000
    000000058480676A6A6B6E0E2D79702E3636311D1D1C12120900000000000000
    0000130F847D2C10607984757B796F2D343334311C1C12160700000000000000
    00045136487C2D297986898A8989842B363431311C1C12090000000000000000
    00048B8B3B797B79848AF5F8F9F87B353631341D1C1C16070000000000000000
    0004579493878AF2F2F3FCFFFD881F333636341D1C1C12070000000000000000
    0004549EB2F4F8FAF969AA814C333A51513A361D1C1612090000000000000000
    000013548FF6FEFB7E33518BA9C5C4A98B5139361D1C11040000000000000000
    0000000732454E3C579D9DC7C9DCC99C5753533A361D11040000000000000000
    00000007335156B8DDC89C9595929191908B5551393413040000000000000000
    000000003395BED9C9A3A3A6A8A8A39C95919055513616000000000000000000
    000000001D91A5B7A5B3B7BDC3C7C2B9A999918B533916000000000000000000
    0000000009579AA5B7BCCCD8E0E1DAD1B9A89890553911000000000000000000
    0000000000509EB6BBCBD7E0F1F1DED6C3B5A391553807000000000000000000
    0000000000118DB4BBCDD5DBDEDBD7D2CAB7A592521300000000000000000000
    00000000000033A4BAC1D0D5D5D3D0CBBEB79F8B1B0000000000000000000000
    00000000000004329ABABFCFCFCDC0BEB69E8D1D000000000000000000000000
    0000000000000000138E9FB3B6B4B39F9550110000000000000000000000FFFF
    FFFFFFFEBBFFFFF801FFFFF001FFFFF000FFFF0000FFFE0000EFFC0001CFFC00
    008FF000000FF000000FF000000FF800000FF800001FF800001FF800001FF000
    001FE000003FE000003FE000003FE000003FF000003FF800003FF800003FFC00
    007FFC00007FFC00007FFE00007FFE0000FFFF0001FFFF0003FFFFC007FF}
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SG_GameBoard: TStringGrid
    Left = 440
    Top = 0
    Width = 321
    Height = 233
    DefaultColWidth = 24
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    TabOrder = 2
    Visible = False
    OnSelectCell = SG_GameBoardSelectCell
    ColWidths = (
      24
      24
      24
      24
      24)
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object GB_Entwicklung: TGroupBox
    Left = 440
    Top = 248
    Width = 313
    Height = 185
    Caption = 'Entwickleransicht'
    TabOrder = 3
    object La_Ziel: TLabel
      Left = 15
      Top = 36
      Width = 33
      Height = 13
      Caption = 'La_Ziel'
    end
    object La_PinguinNummer: TLabel
      Left = 210
      Top = 100
      Width = 73
      Height = 13
      Caption = 'PinguinNummer'
    end
    object La_Path: TLabel
      Left = 15
      Top = 76
      Width = 39
      Height = 13
      Caption = 'La_Path'
    end
    object La_Ausgang: TLabel
      Left = 15
      Top = 20
      Width = 59
      Height = 13
      Caption = 'La_Ausgang'
    end
    object La_CheckPath: TLabel
      Left = 15
      Top = 60
      Width = 68
      Height = 13
      Caption = 'La_CheckPath'
    end
    object La_Direction: TLabel
      Left = 216
      Top = 44
      Width = 46
      Height = 13
      Caption = 'Richtung:'
    end
    object La_Movable: TLabel
      Left = 210
      Top = 112
      Width = 40
      Height = 13
      Caption = 'Movable'
    end
    object La_Position: TLabel
      Left = 210
      Top = 88
      Width = 37
      Height = 13
      Caption = 'Position'
    end
    object La_Fish: TLabel
      Left = 210
      Top = 124
      Width = 19
      Height = 13
      Caption = 'Fish'
    end
    object Btn_TargetsInDirection: TButton
      Left = 208
      Top = 16
      Width = 97
      Height = 25
      Caption = 'TargetsInDirection'
      TabOrder = 0
      OnClick = Btn_TargetsInDirectionClick
    end
    object SE_Direction: TSpinEdit
      Left = 264
      Top = 40
      Width = 41
      Height = 22
      MaxValue = 5
      MinValue = 0
      TabOrder = 1
      Value = 0
      OnChange = Btn_TargetsInDirectionClick
    end
    object Btn_GetAllTargets: TButton
      Left = 208
      Top = 64
      Width = 97
      Height = 17
      Caption = 'GetAllTargets'
      TabOrder = 2
      OnClick = Btn_GetAllTargetsClick
    end
    object Btn_Encode: TButton
      Left = 16
      Top = 96
      Width = 75
      Height = 25
      Caption = 'EncodeBoard'
      TabOrder = 3
      OnClick = Btn_EncodeClick
    end
    object Btn_Renumber: TButton
      Left = 96
      Top = 96
      Width = 97
      Height = 25
      Caption = 'RenumberPinguins'
      TabOrder = 4
      OnClick = Btn_RenumberClick
    end
    object Btn_ImgRefresh: TButton
      Left = 16
      Top = 128
      Width = 113
      Height = 25
      Hint = 'Passt auch die Formulargr'#246#223'e an und aktualisiert das Stringgrid'
      Caption = 'Bilder aktualisieren'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = Btn_ImgRefreshClick
    end
    object Btn_BoardRestart: TButton
      Left = 16
      Top = 152
      Width = 113
      Height = 25
      Hint = 'Passt auch die Formulargr'#246#223'e an und aktualisiert das Stringgrid'
      Caption = 'Spielfeld neustarten'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = Btn_BoardRestartClick
    end
    object Btn_Faktoren: TButton
      Left = 176
      Top = 128
      Width = 75
      Height = 25
      Caption = 'Faktoren'
      TabOrder = 8
      OnClick = Btn_FaktorenClick
    end
    object Btn_ClientTest: TButton
      Left = 176
      Top = 152
      Width = 129
      Height = 25
      Caption = 'Unit_TGameBoard_Client'
      TabOrder = 6
      OnClick = Btn_ClientTestClick
    end
  end
  object GB_Spiel: TGroupBox
    Left = 8
    Top = 360
    Width = 401
    Height = 73
    TabOrder = 4
    object La_Turn: TLabel
      Left = 15
      Top = 36
      Width = 42
      Height = 18
      Caption = 'Runde'
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object La_Score_0: TLabel
      Left = 344
      Top = 33
      Width = 7
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object La_Score_1: TLabel
      Left = 344
      Top = 49
      Width = 7
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object La_Player: TLabel
      Left = 15
      Top = 12
      Width = 47
      Height = 18
      Caption = 'Spieler'
      Font.Charset = GB2312_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnDblClick = La_PlayerDblClick
    end
    object La_ScoreName1: TLabel
      Left = 96
      Top = 49
      Width = 81
      Height = 13
      Caption = 'Punkte Spieler 1:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object La_ScoreName0: TLabel
      Left = 96
      Top = 33
      Width = 81
      Height = 13
      Caption = 'Punkte Spieler 0:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
  end
  object GB_GameShow: TGroupBox
    Left = 8
    Top = 436
    Width = 169
    Height = 41
    TabOrder = 1
    Visible = False
    object Btn_Next: TButton
      Left = 56
      Top = 15
      Width = 17
      Height = 17
      Caption = '>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = GameShow
    end
    object Btn_Previous: TButton
      Left = 40
      Top = 15
      Width = 17
      Height = 17
      Caption = '<'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = GameShow
    end
    object Btn_First: TButton
      Left = 8
      Top = 15
      Width = 25
      Height = 17
      Caption = '<<'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = GameShow
    end
    object Btn_Last: TButton
      Left = 80
      Top = 15
      Width = 25
      Height = 17
      Caption = '>>'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = GameShow
    end
    object Btn_Auto: TButton
      Left = 112
      Top = 13
      Width = 49
      Height = 21
      Caption = 'play'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = GameShow
      OnMouseMove = Btn_AutoMouseMove
    end
  end
  object GB_Client: TGroupBox
    Left = 8
    Top = 436
    Width = 257
    Height = 49
    Caption = 'Client'
    TabOrder = 0
    Visible = False
    object Btn_ClientTurn: TButton
      Left = 8
      Top = 16
      Width = 113
      Height = 25
      Hint = 'Send: 1 + EncodeBoard'
      Caption = 'Zug fordern'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = ClientButtons
    end
    object Btn_ClientReset: TButton
      Left = 136
      Top = 16
      Width = 41
      Height = 25
      Hint = 'Send 3'
      Caption = 'zur'#252'ck-setzen'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      WordWrap = True
      OnClick = ClientButtons
    end
    object CB_ClientAuto: TCheckBox
      Left = 200
      Top = 16
      Width = 49
      Height = 25
      Hint = 'Verz'#246'gerung:'
      Caption = 'Auto'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = ClientButtons
    end
    object Btn_Send: TButton
      Left = 176
      Top = 16
      Width = 17
      Height = 25
      Hint = 'Send String'
      Caption = 'S'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      WordWrap = True
      OnClick = ClientButtons
    end
  end
  object MainMenu: TMainMenu
    object MM_Datei: TMenuItem
      Caption = 'Datei'
      object MM_ContinueLastGame: TMenuItem
        Caption = 'Spiel fortsetzen'
        OnClick = MM_ContinueLastGameClick
      end
      object MM_Open: TMenuItem
        Caption = 'Spiel laden'
        OnClick = MM_OpenClick
      end
      object MM_Save: TMenuItem
        Caption = 'Spiel speichern'
        ShortCut = 16467
        OnClick = MM_SaveClick
      end
      object MM_Close: TMenuItem
        Caption = 'Beenden'
        OnClick = MM_CloseClick
      end
    end
    object MM_Ansicht: TMenuItem
      Caption = 'Ansicht'
      object MM_LastGameStats: TMenuItem
        Caption = 'Letzte Spielauswertung'
        Visible = False
        OnClick = MM_LastGameStatsClick
      end
      object MM_Entwicklung: TMenuItem
        AutoCheck = True
        Caption = 'Entwickleransicht'
        Checked = True
        ShortCut = 16453
        OnClick = MM_EntwicklungClick
      end
      object MM_Hex: TMenuItem
        AutoCheck = True
        Caption = 'Hexagonalfeld'
        Checked = True
        ShortCut = 16456
        OnClick = MM_HexClick
      end
      object MM_Debug: TMenuItem
        AutoCheck = True
        Caption = 'Debug-Nachrichtenfenster'
        ShortCut = 16452
        OnClick = MM_DebugClick
      end
      object MM_TimeStats: TMenuItem
        AutoCheck = True
        Caption = 'Client-Zeitstatistik'
        ShortCut = 16468
        OnClick = MM_TimeStatsClick
      end
      object MM_Transparency: TMenuItem
        AutoCheck = True
        Caption = 'Transparente Hexagonalfelder'
        OnClick = MM_TransparencyClick
      end
    end
    object MM_Game: TMenuItem
      Caption = 'Spiel'
      object MM_GetIntoGame: TMenuItem
        Caption = 'In das Spiel einsteigen'
        Visible = False
        OnClick = MM_GetIntoGameClick
      end
      object MM_Undo: TMenuItem
        Caption = 'R'#252'ckg'#228'ngig'
        Enabled = False
        ShortCut = 16474
        OnClick = MM_UndoClick
      end
      object MM_NewGame: TMenuItem
        Caption = 'Neues Spiel'
        ShortCut = 16462
        OnClick = NewGame
      end
      object MM_Options: TMenuItem
        Caption = 'Einstellungen'
        OnClick = MM_OptionsClick
      end
    end
    object MM_Calculation: TMenuItem
      Caption = 'Testreihen'
      object MM_Testreihe: TMenuItem
        Caption = 'Starten'
        OnClick = MM_TestreiheClick
      end
      object MM_CalcResult: TMenuItem
        Caption = 'Ergebnisse'
        OnClick = MM_CalcResultClick
      end
    end
    object MM_Info: TMenuItem
      Caption = '?'
      object MM_About: TMenuItem
        Caption = #220'ber'
        OnClick = MM_AboutClick
      end
      object MM_Help: TMenuItem
        Caption = 'Hilfe'
        ShortCut = 112
        OnClick = MM_HelpClick
      end
    end
  end
  object OpenDialog_Game: TOpenDialog
    Filter = 'Packeis am Pol - Spiel (*.pap)|*.pap|Alle Dateien|*'
    InitialDir = 'Saves'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing, ofDontAddToRecent]
    Title = 'Packeis am Pol - Spiel '#246'ffnen'
    OnCanClose = OpenDialog_GameCanClose
    Left = 32
  end
  object SaveDialog_Game: TSaveDialog
    DefaultExt = 'pap'
    Filter = 'Packeis am Pol - Spiel (*.pap)|*.pap|Alle Dateien|*'
    InitialDir = 'Saves'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing, ofDontAddToRecent]
    Title = 'Packeis am Pol - Spiel speichern'
    OnCanClose = SaveDialog_GameCanClose
    Left = 64
  end
  object Timer_GameShow: TTimer
    Enabled = False
    Interval = 1600
    OnTimer = Timer_GameShowTimer
    Left = 96
  end
end
