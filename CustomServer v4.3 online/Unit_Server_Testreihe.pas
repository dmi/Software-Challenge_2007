unit Unit_Server_Testreihe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin, Unit_TGameBoard, Buttons, Unit_CustomProc,
  Unit_CalcThd;

type
  TForm_Testreihe = class(TForm)
    Ed_ClientPath: TEdit;
    Btn_ClientPath: TButton;
    La_ClientPath: TLabel;
    OpenDialog_ClientPath: TOpenDialog;
    GB_Options: TGroupBox;
    Btn_Start: TButton;
    SE_Wait: TSpinEdit;
    CB_Wait: TCheckBox;
    La_ms1: TLabel;
    RG_AfterGame: TRadioGroup;
    RG_StartPlayer: TRadioGroup;
    Ed_AutoSave: TEdit;
    Btn_AutoSave: TButton;
    CB_AutoSave: TCheckBox;
    SaveDialog_AutoSave: TSaveDialog;
    La_Nummerierung: TLabel;
    GB_Clients: TGroupBox;
    ListBox_Player0: TListBox;
    La_Player0: TLabel;
    BitBtn_Add0: TBitBtn;
    BitBtn_Delete0: TBitBtn;
    Ed_Name: TEdit;
    ListBox_Player1: TListBox;
    La_Player1: TLabel;
    BitBtn_Add1: TBitBtn;
    BitBtn_Delete1: TBitBtn;
    La_Name: TLabel;
    Btn_LeftToRight: TButton;
    Btn_RightToLeft: TButton;
    La_ClientEinstellungen: TLabel;
    La_Games: TLabel;
    SE_Games: TSpinEdit;
    La_GameSum: TLabel;
    Btn_SaveSettings: TButton;
    CB_Java: TCheckBox;
    procedure CB_JavaClick(Sender: TObject);
    procedure Btn_SaveSettingsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SE_GamesChange(Sender: TObject);
    procedure GB_ClientsMoseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Btn_StartClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Checkbox(Sender: TObject);
    procedure AddItem(Sender: TObject);
    procedure DeleteClient(Sender: TObject);
    procedure ListBoxSelect(Sender:TObject);
    procedure ClientEditChange(Sender:TObject);
    procedure MoveItem(Sender: TObject);
    procedure PathButtonClick(Sender: TObject);
    procedure DialogCanClose(Sender:TObject; var CanClose:Boolean);
  private
    { Private-Deklarationen }
    FirstShown: Boolean;
  public
    { Public-Deklarationen }
    function GetTestSettings(var Check: Boolean): TTestSettings; //Check gibt an, ob auf Richtigkeit �berpr�ft werden soll
    procedure SetTestSettings(var Check: Boolean; Sets: TTestSettings); //Als Variablenparameter gibt es auch zur�ck, ob alles richtig war
  end;



var
  Form_Testreihe: TForm_Testreihe;
  Client0: TListClientArray; //Spieler 0
  Client1: TListClientArray; //Spieler 1

  Calculation: TCalcGame;


implementation

uses Unit_Server_Main, Unit_Server_Testreihe_AddClient;

{$R *.dfm}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_Testreihe.FormCreate(Sender: TObject);
begin
  FirstShown:=FALSE;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#####################################
//############ ALLGEMEIN ##############
//#####################################

procedure TForm_Testreihe.FormShow(Sender: TObject);
var Check: Boolean;
begin
//  ShowMessage('Dieses Feature ist noch nicht fertig.');

  IF FirstShown=FALSE
    THEN begin
      Check:=FALSE;
      SetTestSettings(Check,Board.TestSettings);
    end
    ELSE FirstShown:=TRUE;

  Check:=FALSE;
//  SetTestSettings(Check,Board.TestSettings);

  IF (Listbox_Player0.Count=0) AND (Board.Player[0].CPUControlled=TRUE)
    THEN AddClientToList(0,Board.Player[0].Name,Board.Player[0].ClientPath,Board.Player[0].JavaClient,TRUE);
  IF (Listbox_Player1.Count=0) AND (Board.Player[1].CPUControlled=TRUE)
    THEN AddClientToList(1,Board.Player[1].Name,Board.Player[1].ClientPath,Board.Player[1].JavaClient,TRUE);
  CheckBox(self);
  SE_GamesChange(self);
  IF (ListBox_Player1.Count>=1) AND (ListBox_Player0.ItemIndex=-1) AND (ListBox_Player1.ItemIndex=-1)
    THEN begin
      ListBox_Player1.ItemIndex:=0;

    end;
  IF (ListBox_Player0.Count>=1) AND (ListBox_Player0.ItemIndex=-1) AND (ListBox_Player1.ItemIndex=-1)
    THEN begin
      ListBox_Player0.ItemIndex:=0;
    end;
  ListBoxSelect(self);
  IF ListBox_Player0.ItemIndex<>-1 THEN ListBoxSelect(ListBox_Player0);
  IF ListBox_Player1.ItemIndex<>-1 THEN ListBoxSelect(ListBox_Player1);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.FormClose(Sender: TObject; var Action: TCloseAction);
var Check: Boolean;
begin
  Check:=FALSE;
  Board.TestSettings:=GetTestSettings(Check);
  Board.SaveTestSettingsInFile(GetTestSettings(Check),Board.TestSettingsPath);
  Form_Server.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------

function TForm_Testreihe.GetTestSettings(var Check: Boolean): TTestSettings;
var Wait,i,     h0, h1: Integer;//h�chste belegte Stellen in Clients-Arrays
    Starter: TStartPlayer;
    SavesPath,  Path: String;
    Check0:Boolean; //Checkwert vom Aufruf
begin
  Check0:=Check;
  //Speicherpfade
  SavesPath:=Ed_AutoSave.Text;
  IF Check0=TRUE
    THEN begin
//      IF CB_AutoSave.Checked=TRUE
//        THEN begin
//          IF SavesPath<>''
//            THEN begin
//              CheckFileEnding(SavesPath,'.pap',true);
//            end
//            ELSE ShowMessage('Angegebener Speicherpfad f�r die Spiele ist ung�ltig!');
//        end
//        ELSE
    end;
  //Wait
  IF CB_Wait.Checked=FALSE
    THEN Wait:=0
    ELSE Wait:=SE_Wait.Value;
  //StartPlayer
  IF RG_StartPlayer.ItemIndex=0
    THEN Starter:=2
    ELSE Starter:=RG_StartPlayer.ItemIndex-1;

  //#### ClientPfade  #####
  IF Check0=TRUE
    THEN begin
      //H�chste Stellen in Clients ermitteln
      h0:=0;
      While Client0[h0].Name<>''
        do inc(h0);
      IF Client0[h0].Name=''
        THEN h0:=h0-1;
      h1:=0;
      While Client1[h1].Name<>''
        do inc(h1);
      IF Client1[h1].Name=''
        THEN h1:=h1-1;
      //ClientPfade �berpr�fen
      FOR i:=0 to h0 do
        begin
          Path:=Client0[i].Path;
          IF (Client0[i].Java=FALSE)
            THEN begin
              IF(CheckClientPath(Path,FALSE)=FALSE)
                THEN begin
                  ShowMessage('Clientpfad f�r Spieler 0 an der Stelle '+IntToStr(i)+' ist ung�ltig.');
                  Check:=FALSE
                end;
            end;
          Client0[i].Path:=Path;
        end;
      FOR i:=0 to h1 do
        begin
          Path:=Client1[i].Path;
          IF (Client0[i].Java=FALSE)
            THEN begin
              IF (CheckClientPath(Path,FALSE)=FALSE)
                THEN begin
                  ShowMessage('Clientpfad f�r Spieler 1 an der Stelle '+IntToStr(i)+' ist ung�ltig.');
                  Check:=FALSE
                end;
            end;
          Client1[i].Path:=Path;
        end;
    end;
  //Sonstiges
  With result do
    begin
      Clients0:=Client0;
      Clients1:=Client1;
      AmountOfGames:=SE_Games.Value;
      WaitEachTurn:=Wait;
      AfterEachGame:=RG_AfterGame.ItemIndex;
      StartPlayer:=Starter;
      IF SavesPath='' THEN SaveGames:=FALSE;
      SavePath:=SavesPath;
      Index0:=0; Index1:=0;
      High0:=h0; High1:=h1;
    end;
end;
//------------------------------------------------------------------------------
procedure TForm_Testreihe.SetTestSettings(var Check: Boolean; Sets: TTestSettings);
var Check0: Boolean; //Der zu Anfang eingegbene Checkwert
    i: Integer;
begin
  Check0:=Check;
  //Clients in Listbox eintragen
  Listbox_Player0.Clear;
  Listbox_Player1.Clear;
  Client0:=Sets.Clients0;
  Client1:=Sets.Clients1;
  IF Sets.High0>-1
    THEN begin
      FOR i:=0 to Sets.High0 do
        IF AddClientToList(0,Client0[i].Name,Client0[i].Path,Client0[i].Java,Check0)=FALSE
          THEN begin
            IF Check0=TRUE
              THEN begin
                Check:=FALSE;
                ShowMessage('Fehler beim Einf�gen des '+IntToStr(i)+'. Clients in Listbox0');
                exit;
              end;
          end;
    end;
  IF Sets.High1>-1
    THEN begin
      FOR i:=0 to Sets.High1 do
        IF AddClientToList(1,Client1[i].Name,Client1[i].Path,Client1[i].Java,Check0)=FALSE
          THEN begin
            IF Check0=TRUE
              THEN begin
                Check:=FALSE;
                ShowMessage('Fehler beim Einf�gen des '+IntToStr(i)+'. Clients in Listbox1');
                exit;
              end;
          end;
    end;
  //Optionen
  SE_Games.Value:=Sets.AmountOfGames;
  IF Sets.WaitEachTurn<=0
    THEN CB_Wait.Checked:=FALSE
    ELSE begin
      CB_Wait.Checked:=TRUE;
      SE_Wait.Value:=Sets.WaitEachTurn;
    end;
  RG_AfterGame.ItemIndex:=Sets.AfterEachGame;
  IF Sets.StartPlayer=2
    THEN RG_StartPlayer.ItemIndex:=0
    ELSE RG_StartPlayer.ItemIndex:=Sets.StartPlayer+1;
  IF Sets.SavePath<>''
    THEN begin
      CB_AutoSave.Checked:=TRUE;
      Ed_AutoSave.Text:=Sets.SavePath
    end
    ELSE begin
      CB_AutoSave.Checked:=FALSE;
      Ed_AutoSave.Text:='';
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.Checkbox(Sender: TObject);
begin
  //Verz�gerter Zug
  SE_Wait.Enabled:=CB_Wait.Checked;
  La_ms1.Enabled:=CB_Wait.Checked;
  //Speichern      CB_AutoSave
  Ed_AutoSave.Enabled:=CB_AutoSave.Checked;
  Btn_AutoSave.Enabled:=CB_AutoSave.Checked;
  La_Nummerierung.Enabled:=CB_AutoSave.Checked;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#####################################
//############# CLIENTS ###############
//#####################################

procedure TForm_Testreihe.AddItem(Sender:TObject);
begin
  self.Enabled:=FALSE;
  IF Sender=BitBtn_Add0 THEN Unit_Server_Testreihe_AddClient.PlayerGlobal:=0;
  IF Sender=BitBtn_Add1 THEN Unit_Server_Testreihe_AddClient.PlayerGlobal:=1;
  Form_Testreihe_AddClient.Show;
//  IF Sender=BitBtn_Add0 THEN ListBoxSelect(ListBox_Player0);
//  IF Sender=BitBtn_Add1 THEN ListBoxSelect(ListBox_Player1);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.DeleteClient(Sender:TObject);
var i: integer;
begin
  IF (Sender=BitBtn_Delete0) AND (ListBox_Player0.ItemIndex>=0)
    THEN begin
      FOR i:=ListBox_Player0.ItemIndex to High(Client0)-1 do
        begin
          Client0[i]:=Client0[i+1];
        end;
      i:=ListBox_Player0.ItemIndex;
      ListBox_Player0.Items.Delete(ListBox_Player0.ItemIndex);
      IF i> ListBox_Player0.Count-1 THEN i:=ListBox_Player0.Count-1;
      ListBox_Player0.ItemIndex:=i;
    end;
  IF (Sender=BitBtn_Delete1) AND (ListBox_Player1.ItemIndex>=0)
    THEN begin
      FOR i:=ListBox_Player1.ItemIndex to High(Client1)-1 do
        begin
          Client1[i]:=Client1[i+1];
        end;
      i:=ListBox_Player1.ItemIndex;
      ListBox_Player1.Items.Delete(ListBox_Player1.ItemIndex);
      IF i> ListBox_Player1.Count-1 THEN i:=ListBox_Player1.Count-1;
      ListBox_Player1.ItemIndex:=i;
    end;
  ListBoxSelect(self);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.ListBoxSelect(Sender:TObject);
var i: Integer;
begin
  //Leere Eintr�ge l�schen
  IF ListBox_Player0.Count>=1
    THEN
      FOR i:=0 to ListBox_Player0.Count-1 do
        IF (ListBox_Player0.Items[i]='') OR (ListBox_Player0.Items[i]=' ')
          THEN ListBox_Player0.Items.Delete(i);
  IF ListBox_Player1.Count>=1
    THEN
      FOR i:=0 to ListBox_Player1.Count-1 do
        IF (ListBox_Player1.Items[i]='') OR (ListBox_Player1.Items[i]=' ')
          THEN ListBox_Player1.Items.Delete(i);
  //ItemIndex richtig setzen
  IF ListBox_Player0.Count=0 THEN ListBox_Player0.ItemIndex:=-1;
  IF ListBox_Player1.Count=0 THEN ListBox_Player1.ItemIndex:=-1;
  IF ListBox_Player0.ItemIndex>ListBox_Player0.Count
    THEN ListBox_Player0.ItemIndex:=ListBox_Player0.Count;
  IF ListBox_Player1.ItemIndex>ListBox_Player1.Count
    THEN ListBox_Player1.ItemIndex:=ListBox_Player1.Count;
  //Listbox ausw�hlen und Clienteinstellungen aktualisieren
  IF (Sender=ListBox_Player0) AND (ListBox_Player0.Count>0)
    THEN begin
      ListBox_Player1.ItemIndex:=-1;
      Ed_Name.Text:=Client0[ListBox_Player0.ItemIndex].Name;
      Ed_ClientPath.Text:=Client0[ListBox_Player0.ItemIndex].Path;
      CB_Java.Checked:=Client0[ListBox_Player0.ItemIndex].Java;
    end;
  IF (Sender=ListBox_Player1) AND (ListBox_Player1.Count>0)
    THEN begin
      ListBox_Player0.ItemIndex:=-1;
      Ed_Name.Text:=Client1[ListBox_Player1.ItemIndex].Name;
      Ed_ClientPath.Text:=Client1[ListBox_Player1.ItemIndex].Path;
      CB_Java.Checked:=Client0[ListBox_Player1.ItemIndex].Java;
    end;
  //Listboxen mit Client0/1 abgleichen
  IF ListBox_Player0.Count>=1
    THEN FOR i:=0 to ListBox_Player0.Count-1 do ListBox_Player0.Items[i]:=Client0[i].Name;
  IF ListBox_Player1.Count>=1
    THEN FOR i:=0 to ListBox_Player1.Count-1 do ListBox_Player1.Items[i]:=Client1[i].Name;
  //MoveItems-Buttons
  IF ListBox_Player0.ItemIndex=-1
    THEN Btn_LeftToRight.Enabled:=FALSE
    ELSE Btn_LeftToRight.Enabled:=TRUE;
  IF ListBox_Player1.ItemIndex=-1
    THEN Btn_RightToLeft.Enabled:=FALSE
    ELSE Btn_RightToLeft.Enabled:=TRUE;
  //Wenn in beiden Listboxen nichts angew�hlt, dann Edits auf disabled
  IF (ListBox_Player0.ItemIndex=-1) AND (ListBox_Player1.ItemIndex=-1)
    THEN begin
      La_Name.Enabled:=FALSE;
      Ed_Name.Enabled:=FALSE;
      Ed_Name.Text:='';
      La_ClientPath.Enabled:=FALSE;
      Ed_ClientPath.Enabled:=FALSE;
      Ed_ClientPath.Text:='';
      Btn_ClientPath.Enabled:=FALSE;
      CB_Java.Enabled:=FALSE;
    end
    ELSE begin
      La_Name.Enabled:=TRUE;
      Ed_Name.Enabled:=TRUE;
      La_ClientPath.Enabled:=TRUE;
      Ed_ClientPath.Enabled:=TRUE;
      Btn_ClientPath.Enabled:=TRUE;
      CB_Java.Enabled:=TRUE;
    end;
  //DeleteButtons
  IF (ListBox_Player0.ItemIndex=-1)
    THEN BitBtn_Delete0.Enabled:=FALSE
    ELSE BitBtn_Delete0.Enabled:=TRUE;
  IF (ListBox_Player1.ItemIndex=-1)
    THEN BitBtn_Delete1.Enabled:=FALSE
    ELSE BitBtn_Delete1.Enabled:=TRUE;
  //Spielzahl berechnen
  SE_GamesChange(self);
  //StartButton
  IF (Length(Client0)>=1) AND (Length(Client1)>=1)
    THEN Btn_Start.Enabled:=TRUE
    ELSE Btn_Start.Enabled:=FALSE;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.ClientEditChange(Sender:TObject);
begin
  Ed_ClientPath.Hint:=Ed_ClientPath.Text;
  IF ListBox_Player0.ItemIndex>=0
    THEN begin
      Client0[ListBox_Player0.ItemIndex].Name:=Ed_Name.Text;
      IF Ed_ClientPath.Focused=TRUE
        THEN Client0[ListBox_Player0.ItemIndex].Path:=Ed_ClientPath.Text;
    end;
  IF ListBox_Player1.ItemIndex>=0
    THEN begin
      Client1[ListBox_Player1.ItemIndex].Name:=Ed_Name.Text;
      IF Ed_ClientPath.Focused=TRUE
        THEN Client1[ListBox_Player1.ItemIndex].Path:=Ed_ClientPath.Text;
    end;
  ListBoxSelect(self);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.CB_JavaClick(Sender: TObject);
begin
  IF ListBox_Player0.ItemIndex>=0
    THEN Client0[ListBox_Player0.ItemIndex].Java:=CB_Java.Checked;
  IF ListBox_Player1.ItemIndex>=0
    THEN Client1[ListBox_Player1.ItemIndex].Java:=CB_Java.Checked;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.SE_GamesChange(Sender: TObject);
var i: integer;
begin
  IF TryStrToInt(SE_Games.Text,i)=TRUE
    THEN La_GameSum.Caption:='Spiele insgesamt: '+IntToStr(SE_Games.Value * Listbox_Player0.Count * Listbox_Player1.Count);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.GB_ClientsMoseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  ListBoxSelect(Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.MoveItem(Sender: TObject);
var Index: integer;
begin
  IF Sender=Btn_LeftToRight
    THEN begin
      IF AddClientToList(1,Client0[ListBox_Player0.Itemindex].Name,Client0[ListBox_Player0.Itemindex].Path,Client0[ListBox_Player0.Itemindex].Java,FALSE) = TRUE
        THEN begin
          Index:=ListBox_Player0.Itemindex;

          IF Index>ListBox_Player0.Count-1 THEN Index:=ListBox_Player0.Count-1;
          ListBox_Player0.Itemindex:=Index;
          DeleteClient(BitBtn_Delete0);
          ListBoxSelect(ListBox_Player1);

        end;
    end;
  IF Sender=Btn_RightToLeft
    THEN begin
      IF AddClientToList(0,Client1[ListBox_Player1.Itemindex].Name,Client1[ListBox_Player1.Itemindex].Path,Client1[ListBox_Player0.Itemindex].Java,FALSE) = TRUE
        THEN begin
          Index:=ListBox_Player1.Itemindex;

          IF Index>ListBox_Player1.Count-1 THEN Index:=ListBox_Player1.Count-1;
          ListBox_Player1.Itemindex:=Index;
          DeleteClient(BitBtn_Delete1);
          ListBoxSelect(ListBox_Player0);
        end;
    end;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#####################################
//############# DIALOGS ###############
//#####################################

procedure TForm_Testreihe.DialogCanClose(Sender:TObject; var CanClose:Boolean);
var Path: String;
begin
  IF Sender=OpenDialog_ClientPath
    THEN begin
      Path:=OpenDialog_ClientPath.FileName;
      IF CheckClientPath(Path,TRUE)=FALSE
        THEN CanClose:=FALSE
        ELSE begin
          Ed_ClientPath.SetFocus;
          Ed_ClientPath.Text:=Path;
          IF ListBox_Player0.ItemIndex>=0 THEN Client0[ListBox_Player0.ItemIndex].Path:=Ed_ClientPath.Text;
          IF ListBox_Player1.ItemIndex>=0 THEN Client1[ListBox_Player1.ItemIndex].Path:=Ed_ClientPath.Text;
        end;
    end;
  IF Sender=SaveDialog_AutoSave
    THEN begin
      Ed_AutoSave.SetFocus;
      Ed_AutoSave.Text:=SaveDialog_AutoSave.FileName;
    end;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//#####################################
//############# BUTTONS ###############
//#####################################

procedure TForm_Testreihe.PathButtonClick(Sender: TObject);
begin
  IF Sender=Btn_ClientPath
    THEN begin
      IF FileExists(Ed_ClientPath.Text)=TRUE THEN OpenDialog_ClientPath.FileName:=Ed_ClientPath.Text;
      OpenDialog_ClientPath.Execute;
    end;
  IF Sender=Btn_AutoSave
    THEN begin
      IF FileExists(Ed_AutoSave.Text)=TRUE THEN SaveDialog_AutoSave.FileName:=Ed_AutoSave.Text;
      SaveDialog_AutoSave.Execute;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.Btn_SaveSettingsClick(Sender: TObject);
var Check:Boolean;
begin
  Check:=FALSE;
  Board.TestSettings:=GetTestSettings(Check);
  Board.SaveTestSettingsInFile(GetTestSettings(Check),Board.TestSettingsPath);
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe.Btn_StartClick(Sender: TObject);
var Sets: TTestSettings;
    Check: Boolean;
begin
  Check:=TRUE;
  Sets:=GetTestSettings(Check);

  IF (Sets.High0<0) OR (Sets.High1<0)
    THEN begin
      ShowMessage('Es ist nicht f�r jede Seite mindestens ein Client definiert.');
      Check:=FALSE;
    end;

  IF  (Check=TRUE)
    THEN begin
      self.close;
      Calculation.Sets:=Sets;
      Calculation.CalculateGames;
    end;
end;



end.
