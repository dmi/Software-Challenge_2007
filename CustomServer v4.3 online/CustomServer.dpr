program CustomServer;

uses
  Forms,
  Unit_Server_Main in 'Unit_Server_Main.pas' {Form_Server},
  Unit_TGameBoard in 'Unit_TGameBoard.pas',
  Unit_Server_Options in 'Unit_Server_Options.pas' {Form_Options},
  Unit_CustomProc in 'Unit_CustomProc.pas',
  Unit_Server_About in 'Unit_Server_About.pas' {Form_About},
  Unit_Server_Finish in 'Unit_Server_Finish.pas' {Form_Finish},
  Unit_Server_Testreihe in 'Unit_Server_Testreihe.pas' {Form_Testreihe},
  Unit_Server_Progress in 'Unit_Server_Progress.pas' {Form_Progress},
  Unit_Server_Testreihe_AddClient in 'Unit_Server_Testreihe_AddClient.pas' {Form_Testreihe_AddClient},
  Unit_Server_TimeStats in 'Unit_Server_TimeStats.pas' {Form_TimeStats},
  Unit_Server_SendClient in 'Unit_Server_SendClient.pas' {Form_ClientSend},
  Unit_Server_Help in 'Unit_Server_Help.pas' {Form_Help},
  Unit_CalcThd in 'Unit_CalcThd.pas',
  Unit_CalcResult in 'Unit_CalcResult.pas' {Form_CalcResult},
  Unit_TestSets in 'Unit_TestSets.pas' {Form_TestSets},
  Unit_Timer in 'Unit_Timer.pas',
  DebugMemo in 'DebugMemo.pas',
  DebugForm in 'DebugForm.pas' {Form_Debug},
  Unit_Faktoren in 'Unit_Faktoren.pas' {Form_Faktoren};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'PaP-Server';
  Application.CreateForm(TForm_Server, Form_Server);
  Application.CreateForm(TForm_TimeStats, Form_TimeStats);
  Application.CreateForm(TForm_Options, Form_Options);
  Application.CreateForm(TForm_About, Form_About);
  Application.CreateForm(TForm_Finish, Form_Finish);
  Application.CreateForm(TForm_Testreihe, Form_Testreihe);
  Application.CreateForm(TForm_Progress, Form_Progress);
  Application.CreateForm(TForm_Testreihe_AddClient, Form_Testreihe_AddClient);
  Application.CreateForm(TForm_ClientSend, Form_ClientSend);
  Application.CreateForm(TForm_Help, Form_Help);
  Application.CreateForm(TForm_CalcResult, Form_CalcResult);
  Application.CreateForm(TForm_TestSets, Form_TestSets);
  Application.CreateForm(TForm_Debug, Form_Debug);
  Application.CreateForm(TForm_Faktoren, Form_Faktoren);
  Application.Run;
end.
