unit Unit_Server_TimeStats;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Unit_TGameBoard;

type
  TForm_TimeStats = class(TForm)
    Btn_OK: TButton;
    LB_Clients: TListBox;
    GB_First: TGroupBox;
    Btn_Delete: TButton;
    La_Average0: TLabel;
    La_Maximum0: TLabel;
    La_Minimum0: TLabel;
    GB_Other: TGroupBox;
    La_FirstMinimum0: TLabel;
    La_FirstMaximum0: TLabel;
    La_FirstAverage0: TLabel;
    La_Average: TLabel;
    La_Maximum: TLabel;
    La_Minimum: TLabel;
    La_FirstAverage: TLabel;
    La_FirstMaximum: TLabel;
    La_FirstMinimum: TLabel;
    GroupBox1: TGroupBox;
    La_SumMinimum0: TLabel;
    La_SumMaximum0: TLabel;
    La_SumAverage0: TLabel;
    La_SumAverage: TLabel;
    La_SumMaximum: TLabel;
    La_SumMinimum: TLabel;
    RB_Board: TRadioButton;
    RB_Players: TRadioButton;
    CB_Refresh: TCheckBox;
    Btn_Refresh: TButton;
    Btn_Cancel: TButton;
    La_Counter: TLabel;
    CB_Counter: TCheckBox;
    La_PartSum: TLabel;
    La_PartSum0: TLabel;
    La_CentiSec: TLabel;
    procedure CB_CounterClick(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure CB_RefreshClick(Sender: TObject);
    procedure Btn_RefreshClick(Sender: TObject);
    procedure LB_ClientsDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_DeleteClick(Sender: TObject);
    procedure LB_ClientsClick(Sender: TObject);
    procedure Btn_OKClick(Sender: TObject);
    procedure RefreshListBox(Sender: TObject);
  private
    { Private-Deklarationen }
    FShowOnly: Boolean;
    FStats: TClientTimeStatsArray;
    FStatsBoard: TClientTimeStatsArray;
    FBoard: TGameBoard;
    procedure SetShowOnly(Value: Boolean);
  public
    { Public-Deklarationen }
    property ShowOnly: Boolean read FShowOnly write SetShowOnly;
          //Nur anzeigen oder auch �nderungen erlauben?
    property Stats: TClientTimeStatsArray read FStats write FStats;
    property StatsBoard: TClientTimeStatsArray read FStatsBoard write FStatsBoard; //Die allgemeinen, die gespeichert werden.
    property Board: TGameBoard read FBoard write FBoard; //Das TGameBoard, in das die ge�nderten Statistiken �bertragen werden.
    procedure Show(Values: TClientTimeStatsArray);
  end;


var
  Form_TimeStats: TForm_TimeStats;

implementation

uses Unit_Server_Main;

{$R *.dfm}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_TimeStats.Show(Values: TClientTimeStatsArray);
begin
  StatsBoard:=Values;
  IF RB_Board.Checked=TRUE
    THEN Stats:=StatsBoard;

  inherited Show;

  RefreshListBox(self);
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.SetShowOnly(Value: Boolean);
begin
  IF Value=TRUE
    THEN begin
      RB_Board.Checked:=TRUE;
      RefreshListBox(self);
      RB_Players.Visible:=FALSE;
      RB_Board.Visible:=FALSE;
      Btn_Delete.Visible:=FALSE;
      Btn_Refresh.Visible:=FALSE;
      CB_Counter.Visible:=FALSE;
      LA_Counter.Visible:=FALSE;
      Btn_OK.Visible:=FALSE;
      Btn_Cancel.Caption:='OK';
    end
    ELSE begin
      RB_Players.Visible:=TRUE;
      RB_Board.Visible:=TRUE;
      Btn_Delete.Visible:=TRUE;
      Btn_Refresh.Visible:=TRUE;
      CB_Counter.Visible:=TRUE;
      LA_Counter.Visible:=TRUE;
      Btn_OK.Visible:=TRUE;
      Btn_Cancel.Caption:='Abbrechen';
    end;
  FShowOnly:=Value;
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.RefreshListBox(Sender: TObject);
var i, oldIndex: Integer;
begin
  oldIndex:=LB_Clients.ItemIndex;
  IF RB_Board.Checked=TRUE
    THEN begin
      La_PartSum0.Visible:=FALSE;
      La_PartSum.Visible:=FALSE;
      La_SumAverage0.Visible:=TRUE;
      La_SumAverage.Visible:=TRUE;
      La_SumMaximum0.Visible:=TRUE;
      La_SumMaximum.Visible:=TRUE;
      La_SumMinimum0.Visible:=TRUE;
      La_SumMinimum.Visible:=TRUE;
      Stats:=StatsBoard;
    end;
  IF RB_Players.Checked=TRUE
    THEN begin
      La_PartSum0.Visible:=TRUE;
      La_PartSum.Visible:=TRUE;
      La_SumAverage0.Visible:=FALSE;
      La_SumAverage.Visible:=FALSE;
      La_SumMaximum0.Visible:=FALSE;
      La_SumMaximum.Visible:=FALSE;
      La_SumMinimum0.Visible:=FALSE;
      La_SumMinimum.Visible:=FALSE;
      FStats.length:=0;
      FOR i:=0 to 1
        do begin
          IF (Board<>nil) AND (Board.Player[i].CPUControlled=TRUE)
            THEN begin
              Inc(FStats.length);
              FStats.Arr[i]:=Board.Player[i].TimeStats;
            end;
          end;
    end;
  IF ShowOnly=FALSE
    THEN begin
      CB_Refresh.Visible:=RB_Players.Checked;
      Btn_Refresh.Visible:=RB_Board.Checked;
    end;
  //Listbox
  LB_Clients.Clear;
  IF Stats.length=0
    THEN begin
      LB_ClientsClick(self);
      exit;
    end;
  FOR i:=0 to Stats.length-1 do
    begin
      LB_Clients.Items.Add(Stats.Arr[i].Path);
      IF (length(Stats.Arr[i].Path)) > 100
        THEN LB_Clients.Items[i]:=Copy(LB_Clients.Items[i],1,29)
            +  ' ... '
            +  Copy(LB_Clients.Items[i],length(LB_Clients.Items[i])-44,45);
    end;
  IF (oldIndex>Stats.length-1) OR (oldIndex<0)
    THEN LB_Clients.ItemIndex:=0
    ELSE LB_Clients.ItemIndex:=oldIndex;
  LB_ClientsClick(self);
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.LB_ClientsClick(Sender: TObject);
var i: Integer;
const Einheit = '';
      sec = ' sec';
begin
  i:=LB_Clients.ItemIndex;
  IF (i<0) OR (i>Stats.length-1) 
    THEN begin
      La_FirstAverage.Caption:=' - '+Einheit;
      La_FirstMaximum.Caption:=' - '+Einheit;
      La_FirstMinimum.Caption:=' - '+Einheit;
      //Sonstige Z�ge
      La_Average.Caption:=' - '+Einheit;
      La_Maximum.Caption:=' - '+Einheit;
      La_Minimum.Caption:=' - '+Einheit;
      //Gesamtes Spiel
      La_SumAverage.Caption:=' - '+Einheit;
      La_SumMaximum.Caption:=' - '+Einheit;
      La_SumMinimum.Caption:=' - '+Einheit;
      Btn_Delete.Enabled:=FALSE;
    end
    ELSE begin
      //Erster Zug
      La_FirstAverage.Caption:=IntToStr(Stats.Arr[i].FirstAverage)+Einheit;
      La_FirstMaximum.Caption:=IntToStr(Stats.Arr[i].FirstMaximum)+Einheit;
      La_FirstMinimum.Caption:=IntToStr(Stats.Arr[i].FirstMinimum)+Einheit;
      //Sonstige Z�ge
      La_Average.Caption:=IntToStr(Stats.Arr[i].Average)+Einheit;
      La_Maximum.Caption:=IntToStr(Stats.Arr[i].Maximum)+Einheit;
      La_Minimum.Caption:=IntToStr(Stats.Arr[i].Minimum)+Einheit;
      IF Stats.Arr[i].Maximum > 500 //Wenn l�nger als 5 Sekunden
        THEN La_Maximum.Font.Color:=clRed
        ELSE La_Maximum.Font.Color:=clBlack;
      //Gesamtes Spiel
      La_SumAverage.Caption:=IntToStr(Stats.Arr[i].SumAverage div 100)+sec;
      La_SumMaximum.Caption:=IntToStr(Stats.Arr[i].SumMaximum div 100)+sec;
      La_SumMinimum.Caption:=IntToStr(Stats.Arr[i].SumMinimum div 100)+sec;
      La_PartSum.Caption:=IntToStr(Stats.Arr[i].PartSum)+sec;
      IF Stats.Arr[i].PartSum > 6000 //Wenn l�nger als eine Minute
        THEN La_PartSum.Font.Color:=clRed
        ELSE La_PartSum.Font.Color:=clBlack;
      IF Stats.Arr[i].SumMaximum > 6000 //Wenn l�nger als eine Minute
        THEN La_SumMaximum.Font.Color:=clRed
        ELSE La_SumMaximum.Font.Color:=clBlack;
      Btn_Delete.Enabled:=TRUE;
    end;
  //DeleteButton
  IF RB_Players.Checked=TRUE
    THEN Btn_Delete.Enabled:=FALSE;
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //MainMenu
  Form_Server.MM_TimeStats.Checked:=FALSE;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_TimeStats.Btn_DeleteClick(Sender: TObject);
var i: IntegeR;
begin
  IF (MessageDlg('Eintrag wirklich  entfernen?',mtConfirmation,[mbOK,mbCancel],0)=mrOK)
    THEN begin
      FOR i:=LB_Clients.ItemIndex to Stats.length
        do FStats.Arr[i]:=Stats.Arr[i+1];
      dec(FStats.length);
      LB_Clients.Items.Delete(LB_Clients.ItemIndex);
      IF (LB_Clients.ItemIndex<0) OR (LB_Clients.ItemIndex>Stats.length-1)
        THEN begin
          IF Stats.length=0
            THEN LB_Clients.ItemIndex:=-1
            ELSE LB_Clients.ItemIndex:=Stats.length-1;
          LB_ClientsClick(self);
        end;
    end;
  IF RB_Board.Checked=TRUE THEN StatsBoard:=Stats;
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.Btn_OKClick(Sender: TObject);
begin
  //Einstellungen sichern
  IF FBoard<>nil
    THEN begin
      FBoard.ClientTimeStats:=StatsBoard;
      FBoard.SaveTimeStatsInFile(StatsBoard,Board.ClientTimeStatsPath);
      close;
    end;
end;
//------------------------------------------------------------------------------
procedure TForm_TimeStats.Btn_CancelClick(Sender: TObject);
begin
  close;
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.LB_ClientsDblClick(Sender: TObject);
begin
  ShowMessage(Stats.Arr[LB_Clients.ItemIndex].Path);
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.Btn_RefreshClick(Sender: TObject);
begin
  IF Board<>nil THEN StatsBoard:=Board.ClientTimeStats;
  RefreshListBox(self);
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.CB_RefreshClick(Sender: TObject);
begin
  IF CB_Refresh.Checked=TRUE
    THEN RefreshListbox(self);
end;

//------------------------------------------------------------------------------

procedure TForm_TimeStats.CB_CounterClick(Sender: TObject);
begin
  IF CB_Counter.Checked=FALSE
    THEN La_Counter.Caption:='Counter';
end;

end.
