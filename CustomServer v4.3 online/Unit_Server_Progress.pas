unit Unit_Server_Progress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Unit_TGameBoard, Unit_TestSets;

type
  TSyncPara = record
    Index0,Index1,Counter,AvgTime: Integer;
  end;

  TForm_Progress = class(TForm)
    ProgressBar: TProgressBar;
    Btn_Cancel: TButton;
    La_Player0: TLabel;
    La_Player1: TLabel;
    La_FileName0: TLabel;
    La_FileName1: TLabel;
    La_Game: TLabel;
    La_Percent: TLabel;
    GB_Options: TGroupBox;
    La_Wait: TLabel;
    La_WaitTime: TLabel;
    La_AfterGame: TLabel;
    La_GamePath: TLabel;
    GB_Time: TGroupBox;
    La_AverageGameTime0: TLabel;
    La_AverageGameTime: TLabel;
    La_Versus: TLabel;
    La_Rest: TLabel;
    La_Rest0: TLabel;
    La_Counter: TLabel;
    CB_Counter: TCheckBox;
    Btn_ShowSets: TButton;
    CB_Debug: TCheckBox;
    Btn_ShowDebug: TButton;
    procedure Btn_ShowDebugClick(Sender: TObject);
    procedure Btn_ShowSetsClick(Sender: TObject);
    procedure CB_CounterClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Show(Settings: TTestSettings);
  private
    { Private-Deklarationen }
    FBegin: Boolean; //Wird gerade das erste Spiel berechnet(0%)?
    FSets: TTestSettings;
    FSyncParameter: TSyncPara;
    AllGames: Integer; //Gesamtanzahl der Spiele
    AvgTime: Integer; //Durchschnittsdauer eines Spiels
    FForm_Settings: TForm_TestSets;
    procedure SetSets(Value:TTestSettings);
  public
    { Public-Deklarationen }
    SyncParameter: TSyncPara;
    property Sets: TTestSettings read FSets write SetSets;       
    procedure ProgressRefreshSync; //Wird f�r Synchronize ben�tigt, da es keine Parameter haben darf
    procedure ProgressRefresh (Index0,Index1, Counter, Time: Integer);
    procedure LabelRefresh(CurrentClient0,CurrentClient1,CurrentGame,AverageGameTimeInSecs: Integer; SavePath:String);
  end;

var
  Form_Progress: TForm_Progress;

implementation

uses Unit_Server_Testreihe, Unit_Server_Main, Unit_CustomProc, Unit_CalcThd, DebugForm;

{$R *.dfm}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_Progress.Show(Settings: TTestSettings);
begin
  IF FForm_Settings=nil
    THEN FForm_Settings:=TForm_TestSets.Create(self);
  FForm_Settings.Sets:=Settings;
  Form_Server.Enabled:=FALSE;
  FBegin:=TRUE;
  Sets:=Settings;
  inherited Show;
end;

procedure TForm_Progress.SetSets(Value: TTestSettings);
begin     
  AllGames:=(Value.High0+1)*(Value.High1+1)*Value.AmountOfGames;
  //Optionen
  IF Value.WaitEachTurn=0
    THEN begin
      La_Wait.Caption:='Verz�gerter Zug: Nein';
      La_WaitTime.Caption:='';
    end
    ELSE begin
      La_Wait.Caption:='Verz�gerter Zug: Ja';
      La_WaitTime.Caption:=IntToStr(Value.WaitEachTurn)+' ms';
    end;
  CASE Value.AfterEachGame of
    0: La_AfterGame.Caption:='Nach jedem Spiel: Clients neustarten';
    1: La_AfterGame.Caption:='Nach jedem Spiel: Clients zur�cksetzen';
    2: La_AfterGame.Caption:='Nach jedem Spiel: Nichts tun';
  end;
  IF Value.SavePath=''
    THEN La_GamePath.Caption:='Spiel: Keine Speicherung'
    ELSE La_GamePath.Caption:='Spiel: '+Value.SavePath;

  FSets:=Value;
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.ProgressRefreshSync;
begin
  ProgressRefresh(SyncParameter.Index0,SyncParameter.Index1,SyncParameter.Counter,SyncParameter.AvgTime);
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.ProgressRefresh (Index0,Index1, Counter, Time: Integer);
var Game: Integer; //Die Nummer des aktuellen Spiels
    Percent, RestTime: Integer;
    PrgSets: TTestSettings; //Zum Er- und �bermitteln der n�chsten Kontrahenten f�r Progressbar
    hr,min,sec: Integer; //Resttime
begin
  Game:=Sets.AmountOfGames*Index0*(Sets.High1+1) + Sets.AmountOfGames*Index1 + Counter+1;
  IF FBegin=TRUE
    THEN begin
      FBegin:=FALSE;
      Game:=0;
      La_AverageGameTime.Caption:='-';
      La_Rest.Caption:='-';
    end;
  Percent:=(Game * 100) div AllGames;

  //Progressbar
  Progressbar.Position:=Percent;
  La_Percent.Caption:=IntToStr(Percent)+'%';
  //Label
  La_Game.Caption:='Spiel '+IntToStr(Game)+'/'+IntToStr(AllGames);

  //Aktuelle Kontrahenten
  PrgSets:=Sets;
  PrgSets.Index0:=Index0; PrgSets.Index1:=Index1; PrgSets.Counter:=Counter;
  IF (Game<AllGames) AND (Counter >= Sets.AmountOfGames)
    THEN PrgSets:=GetNextCalcClients(PrgSets);

  //Spieler
  La_Player0.Caption:=Sets.Clients0[PrgSets.Index0].Name;
  La_Player1.Caption:=Sets.Clients1[PrgSets.Index1].Name;
  La_FileName0.Caption:=GetLastPartOfFilePath(Sets.Clients0[PrgSets.Index0].Path);
  La_FileName1.Caption:=GetLastPartOfFilePath(Sets.Clients1[PrgSets.Index1].Path);
  //Zeit
  IF Time>0
    THEN begin
      AvgTime:=(AvgTime*Game+Time) div (Game+1);
    end;
  IF AvgTime>0
    THEN La_AverageGameTime.Caption:=IntToStr(AvgTime div 10)+','+IntToStr(AvgTime mod 10)+' sec'
    ELSE La_AverageGameTime.Caption:='- sec';
  RestTime:=round(AvgTime*(AllGames-Game)/10); //In Sekunden
  hr:=RestTime div 3600;
  min:=(RestTime - (3600*hr)) div 60;
  sec:=(RestTime - (3600*hr) - (60*min));

  La_Rest.Caption:='';
  IF hr>0
    THEN La_Rest.Caption:=IntToStr(hr)+' std ';
  IF (hr>0) OR (min>0)
    THEN La_Rest.Caption:=La_Rest.Caption + IntToStr(min)+' min ';
  La_Rest.Caption:=La_Rest.Caption + IntToStr(sec)+' sec';
  IF AvgTime=0 THEN La_Rest.Caption:='???';
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.LabelRefresh(CurrentClient0,CurrentClient1,CurrentGame,AverageGameTimeInSecs: Integer;
                                        SavePath:String);
begin

  //Progressbar
  La_Game.Caption:='Spiel '+IntToStr(CurrentGame)+'/'+IntToStr(AllGames);
  Progressbar.Position:=round((CurrentGame / AllGames) * 100);
  La_Percent.Caption:=IntToStr(Progressbar.Position)+'%';
  //Statistik
  La_AverageGameTime.Caption:=IntToStr(AverageGameTimeInSecs)+' sec';
  //Optionen
  IF SavePath=''
    THEN La_GamePath.Caption:='Spiel: Keine Speicherung'
    ELSE La_GamePath.Caption:='Spiel: '+SavePath;
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.Btn_CancelClick(Sender: TObject);
begin
  IF MessageDlg('Berechnung wirklich abbrechen?',mtConfirmation,[mbYes,mbCancel],0)=mrYes
    THEN begin
      Calculation.Abort;
      self.Close;
      ShowMessage('Die angezeigte Auswertung ist aufgrund des Abbruchs an einigen Stellen fehlerhaft!');
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.CB_CounterClick(Sender: TObject);
begin
  IF CB_Counter.Checked=FALSE
    THEN La_Counter.Caption:='Counter';
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.Btn_ShowSetsClick(Sender: TObject);
begin
  with FForm_Settings do
    begin
      IF Visible=TRUE
        THEN Hide
        ELSE Show;
    end;
  FForm_Settings.Left:=self.Left+self.Width;
  FForm_Settings.Top:=self.Top;
  self.SetFocus;
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.Btn_ShowDebugClick(Sender: TObject);
begin
  IF Form_Debug.Visible=TRUE
    THEN Form_Debug.Visible:=FALSE
    ELSE Form_Debug.Visible:=TRUE;
  Form_Server.MM_Debug.Checked:=Form_Debug.Visible;
  self.SetFocus;
end;

//------------------------------------------------------------------------------

procedure TForm_Progress.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  IF FForm_Settings.Visible=TRUE
    THEN FForm_Settings.Close;
  Form_Server.Enabled:=TRUE;
end;



end.
