unit Unit_CalcThd;

interface

uses
  Classes, dialogs, SysUtils, Unit_TGameBoard, ExtCtrls, Unit_Timer;

type
  StringArray = Array of String;

  TAvgGameStats = record //Durchschnittsstatistik f�r zwei Spieler
    Wins: array[0..1] of Integer; //Die Anzahl der Siege f�r jeden Spieler
    Vorsprung: array[0..1] of Integer; //Durchschnittlicher Vorsprung (nur die Siege z�hlen)
    SchollenLeft: Integer; //Durchschnittliche Anzahl der �brig gebliebenen Schollen in den Spielen
    Unentschieden: Integer; //Anazhl der Spiele, die unentschieden ausgegangen sind.
  end;
  TAGSArray = array[0..32,0..32] of TAvgGameStats;

  TCalcResult = record {*.cst}
    Sets: TTestSettings;
    AGS: TAGSArray; //AGS = Average Game Statistic
    TimeStats: TClientTimeStatsArray;
  end;

  TCalcFinish = procedure of object;

  TCalcThd = class(TThread)
  private
    { Private-Deklarationen }
    FCBoard:TGameBoard;
    Sets: TTestSettings;
    FCalculating: Boolean; //Wird gerade ein Spiel berechnet?
    FOnCalcFinish : TCalcFinish;
    FAGSArr: TAGSArray;
    FAbort: Boolean;
    FPaused: Boolean; //pausiert?
    FMyTimer: TMyTimer;
    procedure Execute; override;
    procedure Calculate;
    procedure GameFinish(GameStats: TGameStats; Index0,Index1,Counter,Time: Integer);
        //Muss direkt im Thread stehen, um synchronize ausf�hren zu k�nnen.
  public
    property CBoard:TGameBoard read FCBoard;
    property Calculating: Boolean read FCalculating; //Wird gerade ein Spiel berechnet?
    property OnCalcFinish: TCalcFinish read FOnCalcFinish write FOnCalcFinish;
            //Bei Beenden der Berechnungsreihe
    property AGSArr: TAGSArray read FAGSArr;
    procedure Abort;
    procedure Pause; //Pausieren noch nicht fertig!
    procedure UnPause;
  end;


  TCalcGame = class
  private
    FCalcThread: TCalcThd;
    FThreadsRunning: byte;
    FMaxRunning: byte;
    FSets: TTestSettings;
    FErrorReport: StringArray;
    procedure CalcFinish; //Beim Beenden des Berechnungsthreads
    procedure StartThread(ThdSets:TTestSettings);
    procedure CalcThreadOnTerminate(Sender:TObject);
    procedure OnTimeOut(Sender: TObject; Client: Unit_TGameBoard.TBit; Time: Integer);
    procedure OnErrReport(Msg: String);
  public
    constructor Create;

    property CalcThread: TCalcThd read FCalcThread write FCalcThread; //Der Berechnungsthread
    property ThreadsRunning: byte read FThreadsRunning; //Anzahl der laufenden Threads
    property MaxRunning: byte read FMaxRunning; //Maximale Anzahl der laufenden Threads
    property Sets:TTestSettings read FSets write FSets;
    property ErrorReport: StringArray read FErrorReport write FErrorReport; //Die Ausgabe der aufgetretenen Fehler am Ende der Berechnung

    procedure CalculateGames; //Berechnet ein Spiel mit angegbenen Einstellungen
    procedure Abort;
  end;

  function GetNextCalcClients(TestSettings: TTestSettings): TTestSettings;
      //�ndert die Indizes und Counter so, dass die n�chste Begegnung in Indizes und Counter in result ist.



  
const Dbg:ShortString = 'Berechnung: ';



implementation

uses DebugForm, Unit_Server_Main, Unit_Server_Progress, Unit_CalcResult;

{ Wichtig: Methoden und Eigenschaften von Objekten in visuellen Komponenten d�rfen 
  nur in einer Methode namens Synchronize aufgerufen werden, z.B.

      Synchronize(UpdateCaption);

  und UpdateCaption k�nnte folgenderma�en aussehen:

    procedure TCalcThd.UpdateCaption;
    begin
      Form1.Caption := 'Aktualisiert in einem Thread';
    end; }

{ TCalcThd }

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

constructor TCalcGame.Create;
begin
  FThreadsRunning:=0;
  FMaxRunning:=1;
end;

//------------------------------------------------------------------------------

procedure TCalcGame.StartThread(ThdSets:TTestSettings);
var i: TBit;
begin
  //Thread erstellen
  IF ThreadsRunning<MaxRunning
    THEN begin
      CalcThread:=TCalcThd.Create(false);
      with CalcThread do
        begin
          OnTerminate:=CalcThreadOnTerminate;
          Sets:=ThdSets;

          FCBoard:=TGameBoard.Create(nil,nil);
          FOR i:=0 to 1 do
            begin
              with FCBoard.Player[i] do
                begin
                  IF Sets.WaitEachTurn>0
                    THEN begin
                      ClientWait:=TRUE;
                      WaitSleep:=Sets.WaitEachTurn;
                    end ELSE ClientWait:=FALSE;
                  AutoMove:=TRUE;
                end;
            end;

          with FCBoard do
            begin
              Calculating:=TRUE;
              SaveTurnOption:=Sets.SaveGames;
              StartPlayer:=Sets.StartPlayer;
              TestSettings:=Sets;
              Hexvisible:=FALSE;
              QuadVisible:=FALSE;
              ClientTimeOut:=Board.ClientTimeOut;
              OnGameFinish:=GameFinish;
              OnClientTimeOut:=self.OnTimeOut;
              OnErrorReport:= self.OnErrReport;
            end;
          OnCalcFinish:=CalcFinish;

          FMyTimer:=TMyTimer.Create;
          FMyTimer.ResetTime;
        end;

      inc(FThreadsRunning);
    end
    ELSE SendDebug(Dbg+'Es ist bereits ein Thread offen');
end;

//------------------------------------------------------------------------------

procedure TCalcGame.CalculateGames;
begin
  //H�chste Stellen in Clients ermitteln
  FSets.High0:=0;
  While (Sets.High0<High(Sets.Clients0)) AND (Sets.Clients0[Sets.High0].Name<>'')
    do inc(FSets.High0);
  IF FSets.Clients0[Sets.High0].Name=''
    THEN FSets.High0:=Sets.High0-1;
  FSets.High1:=0;
  While (Sets.High1<High(Sets.Clients1)) AND (Sets.Clients1[Sets.High1].Name<>'')
    do inc(FSets.High1);
  IF FSets.Clients1[Sets.High1].Name=''
    THEN FSets.High1:=Sets.High1-1;

  FSets.Index0:=0; FSets.Index1:=0; FSets.Counter:=0;

  //Formular zeigen
  Form_Progress.Show(Sets);
  Form_Progress.ProgressRefresh(0, 0, 0, 0);

  //ErrorReport l�schen
  setlength(FErrorReport,0);

  //Starten
  IF ThreadsRunning<MaxRunning THEN StartThread(self.Sets);
  CalcThread.Calculate;
end;

//------------------------------------------------------------------------------

procedure TCalcGame.CalcThreadOnTerminate(Sender:TObject);
begin

end;

//------------------------------------------------------------------------------

procedure TCalcGame.CalcFinish;
//Zum Sichern der Einstellungen vom HauptBoard:
var CalcResult: TCalcResult;
    i: Integer;
begin
  Form_Progress.Close;
  //Auswertung eingeben
  CalcResult.Sets:=self.CalcThread.Sets;
  CalcResult.AGS:=self.CalcThread.AGSArr;
  CalcResult.TimeStats:=self.CalcThread.CBoard.ClientTimeStats;
  //Auswertung anzeigen
  Form_CalcResult.CalcResult:=CalcResult;
  Form_CalcResult.Show;

  //Errorreport ausgeben
  IF length(ErrorReport)>0
    THEN begin
      ShowMessage(IntToStr(length(ErrorReport))+' Fehler in der Berechnung aufgetreten.');
      FOR i:=0 to High(ErrorReport) do Form_Debug.Memo.Send(dbg+'Fehler! '+ErrorReport[i]);
    end;

  //schlie�en
  FCalcThread.CBoard.Player[0].SendToClient(2);
  FCalcThread.CBoard.Player[0].SendToClient(2);
  FCalcThread.CBoard.Free;
  dec(FThreadsRunning);
  SendDebug(dbg+'geschlossen');

  //HauptBoard restarten
  Form_Server.Btn_BoardRestartClick(self);


end;

//------------------------------------------------------------------------------

procedure TCalcGame.OnTimeOut(Sender: TObject; Client: Unit_TGameBoard.TBit; Time: Integer);
var Name:String;
begin
  IF Client=0
    THEN Name:=Sets.Clients0[CalcThread.CBoard.TestSettings.Index0].Name
    ELSE Name:=Sets.Clients1[CalcThread.CBoard.TestSettings.Index1].Name;
  OnErrReport('Zeit�berschreitung('+IntToStr(round(Time/100))+' sec). Client: '+Name);
end;

//------------------------------------------------------------------------------

procedure TCalcGame.OnErrReport(Msg: String);
begin
  setlength(FErrorReport,length(ErrorReport)+1);
  ErrorReport[High(ErrorReport)]:=Msg;
end;

//------------------------------------------------------------------------------

procedure TCalcGame.Abort;
begin
  IF FCalcThread<>nil
    THEN FCalcThread.Abort;
end;




















//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



procedure TCalcThd.Execute;
begin
  try
    FAbort:=FALSE;

    SendDebug(Dbg+'gestartet');
                                       
    WHILE Terminated=FALSE
      do begin

        IF FAbort=TRUE
          THEN begin
            Terminate;
            Break;
          end;
      end;
    IF self.Terminated=TRUE THEN exit;
  except
    on exception do
      begin
        Form_Debug.Memo.SendString:='Fehler im BerechnungsThread';
        Synchronize(Form_Debug.Memo.Send);
      end;
  end;
end;

//------------------------------------------------------------------------------

procedure TCalcThd.Calculate;
begin
  with self.FCBoard do
    begin
      Player[0].JavaClient:=Sets.Clients0[Sets.Index0].Java;
      Player[1].JavaClient:=Sets.Clients0[Sets.Index0].Java;
      Player[0].CPUControlled:=TRUE;
      Player[1].CPUControlled:=TRUE;
      Player[0].Name         :=Sets.Clients0[Sets.Index0].Name;
      Player[1].Name         :=Sets.Clients1[Sets.Index1].Name;
      Player[0].ClientPath   :=Sets.Clients0[Sets.Index0].Path;
      Player[1].ClientPath   :=Sets.Clients1[Sets.Index1].Path;
    end;

  SendDebug(Dbg+'laufend...');
  CBoard.New;
  FCalculating:=TRUE;
  FCBoard.Player[CBoard.CurrentPlayer].SendToClient(1);
end;

//------------------------------------------------------------------------------

procedure TCalcThd.GameFinish(GameStats: TGameStats; Index0,Index1,Counter,Time: Integer);
var Number: ShortString;
begin
  IF Form_Progress.CB_Debug.Checked=TRUE
    THEN begin
      Number := IntToStr(Counter+1);
      if (Counter+1 < 10) and
        (Sets.AmountOfGames >=10) then
        Number := '0' + Number;
      if (Counter+1 < 100) and
        (Sets.AmountOfGames >=100) then
        Number := '0' + Number;
      Form_Debug.Memo.SendString:='('+GameStats.Player[0].Name+'<->'+GameStats.Player[1].Name+') '+Number+' - Winner: '+GameStats.Player[GameStats.Winner].Name;
      Synchronize(Form_Debug.Memo.Send);
//      Form_Debug.SyncParameter:=('      Vorsprung: '+IntToStr(GameStats.Vorsprung)+'; Schollen: '+IntToStr(60-GameStats.Turns));
//      Synchronize(Form_Debug.SyncSendDebug);
    end;

  //Progress
  Form_Progress.SyncParameter.Index0:=Index0;
  Form_Progress.SyncParameter.Index1:=Index1;
  Form_Progress.SyncParameter.Counter:=Counter;
  Form_Progress.SyncParameter.AvgTime:=round(FMyTimer.CheckTime / 100);
  FMyTimer.ResetTime; //Timer

  Synchronize(Form_Progress.ProgressRefreshSync);



  //###### Durchschnitts-Spiel-Statistik #####
  CASE GameStats.Winner of
    0: begin
         inc(FAGSArr[Index0,Index1].Wins[0]);
         IF Counter=0
          THEN FAGSArr[Index0,Index1].VorSprung[0]:=GameStats.VorSprung
          ELSE FAGSArr[Index0,Index1].VorSprung[0]
                       :=round((FAGSArr[Index0,Index1].VorSprung[0]*(Counter+1)+GameStats.VorSprung) / (Counter+2));
       end;
    1: begin
         inc(FAGSArr[Index0,Index1].Wins[1]);
         IF Counter=0
          THEN FAGSArr[Index0,Index1].VorSprung[1]:=GameStats.VorSprung
          ELSE FAGSArr[Index0,Index1].VorSprung[1]
                        :=round((FAGSArr[Index0,Index1].VorSprung[1]*(Counter+1)+GameStats.VorSprung) / (Counter+2));
       end;
    2: inc(FAGSArr[Index0,Index1].Unentschieden);
  end;
  //�brige Schollen
  IF Counter=0
    THEN FAGSArr[Index0,Index1].SchollenLeft:=60-GameStats.Turns
    ELSE FAGSArr[Index0,Index1].SchollenLeft
                        :=round( (FAGSArr[Index0,Index1].SchollenLeft*(Counter+1) + (60-GameStats.Turns)) / (Counter+2));

  //Bei Ende der Berechnung
  IF (Index0=Sets.High0) AND (Index1=Sets.High1) AND (Counter=Sets.AmountOfGames-1)
    THEN begin
      Form_Debug.Memo.SendString:=dbg+'Die Testreihe wurde erfolgreich beendet.';
      synchronize(Form_Debug.Memo.Send);
      Abort;
    end; 
end;

//------------------------------------------------------------------------------

procedure TCalcThd.Abort;
begin
  try
    IF Assigned(OnCalcFinish) THEN Synchronize(OnCalcFinish);
    IF (self.Terminated=FALSE)
      THEN self.FAbort:=TRUE;
  except
    on Exception do SendDebug(dbg+'Fehler beim Abbrechen.');
  end;
end;

//------------------------------------------------------------------------------

procedure TCalcThd.Pause;
begin
  FPaused:=TRUE;
end;
//------------------------------------------------------------------------------
procedure TCalcThd.UnPause;
begin
  FPaused:=FALSE;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


function GetNextCalcClients(TestSettings: TTestSettings): TTestSettings;
      //�ndert die Indizes und Counter so, dass die n�chste Begegnung in Indizes und Counter in result ist.
begin
  IF TestSettings.Counter >= TestSettings.AmountOfGames-1
    THEN begin
      //Indexwerte erh�hen
      IF TestSettings.Index1 < TestSettings.High1 //F�r Spieler 1 einen weitergehen
        THEN inc(result.Index1)
        ELSE begin //F�r Spieler 0 weitergehen Spieler 1 neu anfangen
          IF (TestSettings.Index0 <= TestSettings.High0)
            THEN begin
              inc(result.Index0);
              result.Index1:=0;
            end;
        end;
      result.Counter:=0;
    end
    ELSE inc(result.Counter);
end;

end.
