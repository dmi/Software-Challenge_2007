object Form_Progress: TForm_Progress
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Testreihe - Fortschritt'
  ClientHeight = 181
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object La_Versus: TLabel
    Left = 182
    Top = 10
    Width = 23
    Height = 13
    Caption = '<->'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object La_Game: TLabel
    Left = 9
    Top = 56
    Width = 3
    Height = 13
  end
  object La_Percent: TLabel
    Left = 184
    Top = 56
    Width = 30
    Height = 13
    Caption = '80 %'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object La_FileName1: TLabel
    Left = 208
    Top = 28
    Width = 66
    Height = 13
    Caption = 'La_FileName1'
    Transparent = True
  end
  object La_FileName0: TLabel
    Left = 110
    Top = 28
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'La_FileName0'
    Transparent = True
  end
  object La_Player1: TLabel
    Left = 208
    Top = 8
    Width = 72
    Height = 16
    Caption = 'La_Player1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object La_Player0: TLabel
    Left = 104
    Top = 8
    Width = 72
    Height = 16
    Alignment = taRightJustify
    Caption = 'La_Player0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object La_Counter: TLabel
    Left = 36
    Top = 157
    Width = 39
    Height = 13
    Caption = 'Counter'
  end
  object ProgressBar: TProgressBar
    Left = 8
    Top = 72
    Width = 393
    Height = 17
    Position = 80
    Smooth = True
    TabOrder = 0
  end
  object Btn_Cancel: TButton
    Left = 296
    Top = 152
    Width = 105
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 1
    OnClick = Btn_CancelClick
  end
  object GB_Options: TGroupBox
    Left = 144
    Top = 264
    Width = 217
    Height = 105
    Caption = 'Optionen'
    TabOrder = 2
    Visible = False
    object La_Wait: TLabel
      Left = 8
      Top = 16
      Width = 96
      Height = 13
      Caption = 'Verz'#246'gerter Zug: Ja'
      ShowAccelChar = False
    end
    object La_WaitTime: TLabel
      Left = 120
      Top = 16
      Width = 28
      Height = 13
      Caption = '20 ms'
      ShowAccelChar = False
    end
    object La_AfterGame: TLabel
      Left = 8
      Top = 32
      Width = 181
      Height = 13
      Caption = 'Nach jedem Spiel: Client zur'#252'cksetzen'
    end
    object La_GamePath: TLabel
      Left = 8
      Top = 48
      Width = 201
      Height = 49
      AutoSize = False
      Caption = 'Spiel: Keine Speicherung'
      WordWrap = True
    end
  end
  object GB_Time: TGroupBox
    Left = 8
    Top = 96
    Width = 297
    Height = 49
    Caption = 'Zeit'
    TabOrder = 3
    object La_AverageGameTime0: TLabel
      Left = 8
      Top = 16
      Width = 153
      Height = 25
      AutoSize = False
      Caption = 'Durchschnittliche Berechnungs- dauer eines Spiels:'
      WordWrap = True
    end
    object La_AverageGameTime: TLabel
      Left = 126
      Top = 28
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = '8,15 sec'
    end
    object La_Rest: TLabel
      Left = 258
      Top = 28
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = '10 sec'
    end
    object La_Rest0: TLabel
      Left = 184
      Top = 16
      Width = 105
      Height = 17
      AutoSize = False
      Caption = 'Gesch'#228'tzte Restzeit:'
      Transparent = True
      WordWrap = True
    end
  end
  object CB_Counter: TCheckBox
    Left = 16
    Top = 156
    Width = 17
    Height = 17
    Hint = 'Aktuelle Timer-Zeit anzeigen?'
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 4
    OnClick = CB_CounterClick
  end
  object Btn_ShowSets: TButton
    Left = 312
    Top = 100
    Width = 81
    Height = 23
    Caption = 'Einstellungen'
    TabOrder = 5
    OnClick = Btn_ShowSetsClick
  end
  object CB_Debug: TCheckBox
    Left = 136
    Top = 152
    Width = 121
    Height = 25
    Caption = 'Spielergebnisse im Debugfenster'
    Checked = True
    State = cbChecked
    TabOrder = 6
    WordWrap = True
  end
  object Btn_ShowDebug: TButton
    Left = 312
    Top = 122
    Width = 81
    Height = 23
    Caption = 'Debugfenster'
    TabOrder = 7
    OnClick = Btn_ShowDebugClick
  end
end
