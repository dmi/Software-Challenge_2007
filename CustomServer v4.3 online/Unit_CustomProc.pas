unit Unit_CustomProc;

interface

  uses Types, SysUtils, Math, Dialogs;

  type

    EColorError = CLASS(Exception); //HSV,RGB
    TReal = DOUBLE; //HSV,RGB

    TPointArray = array of TPoint;
    TBit = 0..1;


  function AddPointArrays(Array1,Array2: TPointArray; Filter:Boolean): TPointArray;
      //Addiert zwei PunkteArrays. Array1 kommt zuerst, dann Array2
      //Filter gibt an, ob gleiche Werte im Array herausgefiltert werden sollen
  procedure DeleteOutofPointArray(var PointArray: TPointArray; const Index: Integer);
      //L�scht einen Eintrag an der Stelle Index aus dem Array,
      //schiebt die anderen nach vorne und schneidet Ende ab
  function CheckFileEnding(var Path: String; const FileType: String; const Change: Boolean): Boolean;
      //�berpr�ft die Dateiendung, ob sie FileType entspricht, wenn nicht, wird gegebenenfalls (Change)
      //die Endung ge�ndert.   Beispiel f�r FileTye: '.pap'
  function RemoveLastPartFromFilepath(Path: String): String;
      //Entfernt den letzten Teil eine Dateipfades:
      //z.B. D:\\dir1\dir2\test.txt wird zu  D:\\dir1\dir2\
      //oder z.B: D:\\dir1\dir2\ wird zu D:\\dir1\
  function GetLastPartOfFilePath(Path:String): String;
  function BoolToBit(Bool: Boolean): TBit;
      //TRUE ist 1; FALSE ist 0
  function BitToBool(Bit:TBit): Boolean;
      //TRUE ist 1; FALSE ist 0
  PROCEDURE RGBToHSV (CONST R,G,B: TReal; VAR H,S,V: TReal);
  PROCEDURE HSVtoRGB (CONST H,S,V: TReal; VAR R,G,B: TReal);
  procedure CountUp(var S: String; const Seperator: ShortString);
      //Z�hlt das Ende eines Strings, sofern dieses ein Integer ist, rauf.
      //andernfalls wird hinter dem Seperator ein neue Zahl eingetragen.
      //Beispiel: S='Client_02' -> 'Client_03'
      //Beispiel: S='Client', Seperator=' - '  ->  'Client - 01'



implementation

Const Version: String = '01';


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

function AddPointArrays(Array1,Array2: TPointArray; Filter:Boolean): TPointArray;
var i,j:integer;
    InArray: Boolean; //Ist der bearbeitete Wert aus dem Array schon im resultArray vorhanden?
begin
  //Arrays addieren
  result:=Array1;
  setlength(result,length(result) + length(Array2));
  FOR i:=0 to length(Array2)-1 do
    begin
      result[i+length(Array1)]:= Array2[i];
    end;     
  //Redundante Werte herausfiltern
  IF Filter=TRUE
    THEN begin
      FOR i:=0 to length(result)-1 do
        begin
          InArray:=FALSE;
          FOR j:=i+1 to length(result)-1 do
            begin
              IF (result[j].X=result[i].X) AND (result[j].Y=result[i].Y) //Doppelten Wert gefunden
                THEN begin
                  InArray:=TRUE;
                  //Dann alle Arraywerte um einen nach vorne schieben und das Ende l�schen
                  DeleteOutofPointArray(result,i);
                end;
            end;
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure DeleteOutofPointArray(var PointArray: TPointArray; const Index: Integer);
  //L�scht einen Eintrag an der Stelle Index aus dem Array,
  //schiebt die anderen nach vorne und schneidet Ende ab
var i: Integer;
begin
  IF Index<length(PointArray)
    THEN begin
      FOR i:=Index to length(PointArray)-1 do
        begin
          PointArray[i]:=PointArray[i+1];
        end;
      //Ende abschneiden
      setlength(PointArray,length(PointArray)-1);
    end;
end;

//------------------------------------------------------------------------------

function CheckFileEnding(var Path: String; const FileType: String; const Change: Boolean): Boolean;
var CurrentEnding: ShortString;
    tempChar: Char;
    i,j:integer;
begin
  CurrentEnding:='';
  i:=length(path)+1;
  REPEAT
    dec(i);
    tempChar:=Path[i];
    CurrentEnding:=tempChar+CurrentEnding;
  until (tempChar='.') or (i=0);

  //Punkte und Leerstellen am Ende l�schen
  IF Change=TRUE
    THEN begin
      While (Path[i-1]='.') or (Path[i-1]=' ') do
        begin
          Delete(Path,i-1,1);
          dec(i);
        end;
    end;

  IF CurrentEnding=FileType
    THEN result:=TRUE
    ELSE begin
      result:=FALSE;
      IF (Change=TRUE)
        THEN begin
          IF i<>0
            THEN Delete(Path,i,length(path)-i+1);
          Path:=Path+FileType;
        end;
    end;
end;

//------------------------------------------------------------------------------

function RemoveLastPartFromFilepath(Path: String): String;
var i:integer;
    tempChar: Char;
begin
  IF length(Path)=0
    THEN begin
      result:='';
      exit;
    end;
  i:=length(path);
  REPEAT
    dec(i);
    tempChar:=Path[i];
  until (tempChar='\');
  Delete(Path,i,length(path)-i+1);
  result:=Path;
end;

//------------------------------------------------------------------------------

function GetLastPartOfFilePath(Path:String): String;
var i:integer;
    tempChar: Char;
begin
  IF length(Path)=0
    THEN begin
      result:='';
      exit;
    end;
  i:=length(path);
  REPEAT
    dec(i);
    tempChar:=Path[i];
  until (tempChar='\') OR (i=0);
  IF i=0
    THEN begin
      result:=Path;
      exit;
    end;
  result:=Copy(Path,i+1,length(path)-i+1);
end;

//------------------------------------------------------------------------------

function BoolToBit(Bool: Boolean): TBit;
begin
  IF Bool=TRUE
    THEN result:=1
    ELSE result:=0;
end;
//------------------------------------------------------------------------------
function BitToBool(Bit:TBit): Boolean;
begin
  IF Bit=1
    THEN result:=TRUE
    ELSE result:=FALSE;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//##########################################
//################  FARBEN  ################
//##########################################

// RGB, each 0 to 255, to HSV.
// H = 0.0 to 360.0 (corresponding to 0..360.0 degrees around hexcone)
// S = 0.0 (shade of gray) to 1.0 (pure color)
// V = 0.0 (black) to 1.0 {white)

// Based on C Code in "Computer Graphics -- Principles and Practice,"
// Foley et al, 1996, p. 592. 

PROCEDURE RGBToHSV (CONST R,G,B: TReal; VAR H,S,V: TReal);
  VAR
    Delta: TReal;
    Min : TReal;
BEGIN
  Min := MinValue( [R, G, B] );    // USES Math
  V := MaxValue( [R, G, B] );

  Delta := V - Min;

  // Calculate saturation: saturation is 0 if r, g and b are all 0
  IF       V = 0.0
  THEN S := 0
  ELSE S := Delta / V;

  IF       S = 0.0
  THEN H := NaN    // Achromatic: When s = 0, h is undefined
  ELSE BEGIN       // Chromatic
    IF       R = V
    THEN // between yellow and magenta [degrees]
      H := 60.0 * (G - B) / Delta
    ELSE
      IF       G = V
      THEN // between cyan and yellow
        H := 120.0 + 60.0 * (B - R) / Delta
      ELSE
        IF       B = V
        THEN // between magenta and cyan
          H := 240.0 + 60.0 * (R - G) / Delta;

    IF H < 0.0
    THEN H := H + 360.0
  END
END {RGBtoHSV};

//------------------------------------------------------------------------------

// Based on C Code in "Computer Graphics -- Principles and Practice,"
// Foley et al, 1996, p. 593.
//
// H = 0.0 to 360.0 (corresponding to 0..360 degrees around hexcone)
// NaN (undefined) for S = 0
// S = 0.0 (shade of gray) to 1.0 (pure color)
// V = 0.0 (black) to 1.0 (white)

PROCEDURE HSVtoRGB (CONST H,S,V: TReal; VAR R,G,B: TReal);
  VAR
    f : TReal;
    i : INTEGER;
    hTemp: TReal; // since H is CONST parameter
    p,q,t: TReal;
BEGIN
  IF       S = 0.0    // color is on black-and-white center line
  THEN BEGIN
    IF       IsNaN(H)
    THEN BEGIN
      R := V;           // achromatic: shades of gray
      G := V;
      B := V
    END
    ELSE RAISE EColorError.Create('HSVtoRGB: S = 0 and H has a value');
  END

  ELSE BEGIN // chromatic color
    IF       H = 360.0         // 360 degrees same as 0 degrees
    THEN hTemp := 0.0
    ELSE hTemp := H;

    hTemp := hTemp / 60;     // h is now IN [0,6)
    i := TRUNC(hTemp);        // largest integer <= h
    f := hTemp - i;                  // fractional part of h

    p := V * (1.0 - S);
    q := V * (1.0 - (S * f));
    t := V * (1.0 - (S * (1.0 - f)));

    CASE i OF
      0: BEGIN R := V; G := t;  B := p  END;
      1: BEGIN R := q; G := V; B := p  END;
      2: BEGIN R := p; G := V; B := t   END;
      3: BEGIN R := p; G := q; B := V  END;
      4: BEGIN R := t;  G := p; B := V  END;
      5: BEGIN R := V; G := p; B := q  END
    END
  END
END {HSVtoRGB};

//------------------------------------------------------------------------------

procedure CountUp(var S: String; const Seperator: ShortString);
var Int,i,n,l: Integer;
    IntS, AddStr: ShortString;
begin
  IntS:='';
  i:=0;
  REPEAT
    inc(i);
    IntS:=Copy(S,i,length(S)-i+1);
  until (TryStrToInt(IntS, Int)=TRUE) OR (length(IntS)=0);
  //Neue Nummer einf�gen
  IF intS='xe' THEN IntS:=''; //Irgendeine Besonderheit, die ich nicht verstehe....
  IF length(IntS)=0
    THEN begin
      S:=S+Seperator+'01';
    end
  //Nummer raufz�hlen
    ELSE begin
      //Die Nullen vor der letzten Nummer ber�cksichtigen
      n:=1;
      WHILE IntS[n]='0' do
        begin
          i:=i+n;
          inc(n);
        end;
      dec(n); //n=Anzahl der Nullen vor der momentanen Zahl
      
      inc(Int);
      AddStr:=IntToStr(Int);
      l:=length(IntS); //l=minimale L�nge des zuk�nftigen AddStr
      i:=i-n; //Nullen l�schen
      IF n>1 THEN i:=i-n+1;
      WHILE length(AddStr)<l do
        begin
          AddStr:='0'+AddStr;
        end;
      Delete(S,i,length(S)-i+1); //Die Zahl am Ende abschneiden
      S:=S+AddStr;
    end;
end;

end.
