unit Unit_CalcResult;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls, Unit_CalcThd, Unit_Server_TimeStats,
  Unit_TestSets;

type
  TForm_CalcResult = class(TForm)
    La_Player1Name: TLabel;
    La_Player0Name: TLabel;
    La_Player0Path: TLabel;
    La_Player1Path: TLabel;
    RG_Player0: TRadioGroup;
    RG_Player1: TRadioGroup;
    RG_General: TRadioGroup;
    Btn_Save: TButton;
    Btn_Load: TButton;
    CB_Colorize: TCheckBox;
    Btn_TimeStats: TButton;
    Btn_TestSettings: TButton;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    SG_Result: TStringGrid;
    procedure Btn_TestSettingsClick(Sender: TObject);
    procedure CB_ColorizeClick(Sender: TObject);
    procedure La_PathClick(Sender: TObject);
    procedure DialogCanClose(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Btn_TimeStatsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_SaveLoadClick(Sender: TObject);
    procedure SG_ResultSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure RadioGroupSelection(Sender:TObject);
  private
    { Private-Deklarationen }
    FForm_TestSets: TForm_TestSets;
    FTimeForm: TForm_TimeStats;
    FCalcResult: TCalcResult;
    FLoaded: Boolean; //Sind schon CalcResults geladen?
    FSGColor: Array[0..32,0..32] of TColor; //Die Farben des StringGrids
    procedure SG_ResultOnDrawCells(Sender: TObject; ACol,ARow: Integer; Rect: TRect; State: TGridDrawState); //Einf�rben
  public
    { Public-Deklarationen }
    property CalcResult: TCalcResult read FCalcResult write FCalcResult;
    procedure Show;
    procedure Load(CR: TCalcResult);
    procedure Refresh;

    procedure SaveToFile(CR: TCalcResult; Path: String);
    function LoadFromFile(Path: String): TCalcResult;
  end;

var
  Form_CalcResult: TForm_CalcResult;

const
  clRedInactive: TColor = $009696C8;
  clRedActive: TColor = $000000D7;
  clBlueInactive: TColor = $00C89696;
  clBlueActive: TColor = clBlue;

implementation

uses Unit_Server_Main, Unit_CustomProc;

{$R *.dfm}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_CalcResult.FormCreate(Sender: TObject);
begin
  FTimeForm:=TForm_TimeStats.Create(self);
  FForm_TestSets:=TForm_TestSets.Create(self);
  FTimeForm.Position:=poMainFormCenter;
  SG_Result.OnDrawCell:= SG_ResultOnDrawCells;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.Show;
begin
  Form_Server.Enabled:=FALSE;
  IF CalcResult.Sets.Clients0[0].Name<>'' //Als Beispiel, dass CalcResult leer ist.
    THEN Load(CalcResult)
    ELSE begin
      Btn_SaveLoadClick(Btn_Load);
    end;
  IF FLoaded=TRUE
    THEN begin
      Form_Server.Enabled:=FALSE;
      inherited Show;
    end
    ELSE close;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.Load(CR: TCalcResult);
var c,r: Integer;
    CanSelect: Boolean; // f�r SG_ResultSelectCell
    l,t, w,h: Integer; //left und top und width und height des Formulars
begin
  CalcResult:=CR;
  //Horizontal: Spieler 1
  //Vertikal: Spieler 0

  //StringGrid
  with SG_Result do
    begin
      RowCount:=CR.Sets.High0+2;
      ColCount:=CR.Sets.High1+2;
      Height:=RowCount*(DefaultRowHeight+GridLineWidth+2)+5;
      Width:=ColCount*(DefaultColWidth+GridLineWidth+2)+5;
      //Spielernamen
      FOR r:=1 to RowCount-1 do
        SG_Result.Cells[0,r]:=CR.Sets.Clients0[r-1].Name;
      FOR c:=1 to ColCount-1 do
        SG_Result.Cells[c,0]:=CR.Sets.Clients1[c-1].Name;
    end;
  CB_ColorizeClick(self);
  RadioGroupSelection(RG_General);

  //Formgr��e anpassen
  w:=Width; h:=Height; l:=Left; t:=Top;
  w:=SG_Result.Left+SG_Result.Width;
  IF w < Btn_Load.Left+Btn_Load.Width
    THEN w:=Btn_Load.Left+Btn_Load.Width+RG_Player0.Left;
  h:=SG_Result.Top+SG_Result.Height + 38;
  IF w+Left > Screen.Width
    THEN Left:=Screen.Width-w;
  IF h+Top > Screen.Height
    THEN Top:=Screen.Height-h;
  IF w > Screen.Width
    THEN SG_Result.Width:=w - 2*SG_Result.Left - 6;
  IF h > Screen.Height
    THEN SG_Result.Height:=h - SG_Result.Top - SG_Result.Left - 38;
  Width:=w; Height:=h; Left:=l; Top:=t;


  //Forms
  FTimeForm.Stats:=self.CalcResult.TimeStats;
  FTimeForm.ShowOnly:=TRUE;
  FForm_TestSets.Sets:=CalcResult.Sets;  
  //Werte
  RG_Player0.ItemIndex:=0;
  RadioGroupSelection(RG_Player0);
  CanSelect:=TRUE;
  SG_ResultSelectCell(self, SG_Result.Col,SG_Result.Row, CanSelect);
  Refresh;

  FLoaded:=TRUE;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FTimeForm.Close;
  FForm_TestSets.close;
  Form_Server.Enabled:=TRUE;
end;






//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_CalcResult.RadioGroupSelection(Sender:TObject);
var ActivePlayer: 0..2;
    i: Integer;
begin
  IF Sender=RG_Player0
    THEN begin
      ActivePlayer:=0;
      RG_Player1.ItemIndex:=-1;
      RG_General.ItemIndex:=-1;
    end;
  IF Sender=RG_Player1
    THEN begin
      ActivePlayer:=1;
      RG_Player0.ItemIndex:=-1;
      RG_General.ItemIndex:=-1;
    end;
  IF Sender=RG_General
    THEN begin
      ActivePlayer:=2;
      RG_Player1.ItemIndex:=-1;
      RG_Player0.ItemIndex:=-1;
    end;
  //StringGridfarben
  FOR i:=1 to SG_Result.RowCount+1 //Spieler 0
    do FSGColor[0,i]:=clRedInactive;
  FOR i:=1 to SG_Result.ColCount+1 //Spieler 1
    do FSGColor[i,0]:=clBlueInactive;

  IF ActivePlayer=0
    THEN begin
      FOR i:=1 to SG_Result.RowCount+1 //Spieler 0
        do FSGColor[0,i]:=clRedActive;
    end;
  IF ActivePlayer=1
    THEN begin
      FOR i:=1 to SG_Result.ColCount+1 //Spieler 0
        do FSGColor[i,0]:=clBlueActive;
    end;

  Refresh;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.Refresh;
var i,j, PlayedGames: Integer;
    S: ShortString; //Eintrag in die Zelle
begin
  FOR i:=0 to CalcResult.Sets.High0 do  //Reihen
    FOR j:=0 to CalcResult.Sets.High1 do  //Spalten
      begin
        PlayedGames:= CalcResult.AGS[i,j].Wins[0] + CalcResult.AGS[i,j].Wins[1];
        CASE RG_Player0.ItemIndex of
          0: S:=IntToStr(CalcResult.AGS[i,j].Wins[0]);
          1: S:=IntToStr(round( CalcResult.AGS[i,j].Wins[0] / PlayedGames * 100))+'%';
          2: S:=IntToStr(CalcResult.AGS[i,j].Vorsprung[0]);
        end;
        CASE RG_Player1.ItemIndex of
          0: S:=IntToStr(CalcResult.AGS[i,j].Wins[1]);
          1: S:=IntToStr(round( CalcResult.AGS[i,j].Wins[1] / PlayedGames * 100))+'%';
          2: S:=IntToStr(CalcResult.AGS[i,j].Vorsprung[1]);
        end;
        CASE RG_General.ItemIndex of
          0: S:=IntToStr(CalcResult.AGS[i,j].SchollenLeft);
          1: S:=IntToStr(CalcResult.AGS[i,j].Unentschieden);
        end;

        SG_Result.Cells[j+1,i+1]:=S;
      end;

  //StringGrid kolorieren
  SG_Result.Repaint;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.SG_ResultSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  //Horizontal: Spieler 1
  La_Player0Name.Caption:=CalcResult.Sets.Clients0[ARow-1].Name;
  La_Player0Path.Caption:=GetLastPartOfFilePath(CalcResult.Sets.Clients0[ARow-1].Path);
  La_Player0Path.Hint:=CalcResult.Sets.Clients0[ARow-1].Path;
  //Vertikal: Spieler 0
  La_Player1Name.Caption:=CalcResult.Sets.Clients1[ACol-1].Name;
  La_Player1Path.Caption:=GetLastPartOfFilePath(CalcResult.Sets.Clients1[ACol-1].Path);
  La_Player1Path.Hint:=CalcResult.Sets.Clients1[ACol-1].Path;

end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.La_PathClick(Sender: TObject);
begin
  IF Sender=La_Player0Path
    THEN ShowMessage(La_Player0Path.Hint);
  IF Sender=La_Player1Path
    THEN ShowMessage(La_Player1Path.Hint);
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.Btn_TimeStatsClick(Sender: TObject);
begin
  IF FTimeForm.Visible=FALSE
    THEN FTimeForm.Show(CalcResult.TimeStats)
    ELSE FTimeForm.close;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.Btn_TestSettingsClick(Sender: TObject);
begin
  IF FForm_TestSets.Visible=FALSE
    THEN FForm_TestSets.Show
    ELSE FForm_TestSets.close;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_CalcResult.SG_ResultOnDrawCells(Sender: TObject; ACol,ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  IF (ARow=0) AND (ACol=0) THEN exit;
  With SG_Result do
    begin
      Canvas.Brush.Color:=FSGColor[ACol,ARow];
      Canvas.FillRect(Rect);
      IF (Canvas.Brush.Color=clBlueActive) OR (Canvas.Brush.Color=clRedActive)
        THEN Canvas.Font.Color:=clWhite
        ELSE Canvas.Font.Color:=clBlack;
      Canvas.TextOut(Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.CB_ColorizeClick(Sender: TObject);
var c,r: Integer;
begin
  IF CB_Colorize.Checked=TRUE
    THEN begin
    end
    ELSE begin //Alle Fl�chen auf clWhite setzen
      FOR c:=1 to SG_Result.ColCount do
        FOR r:=1 to SG_Result.RowCount do
          begin
            FSGColor[c,r]:=clWhite;
          end;
    end;
end;



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_CalcResult.Btn_SaveLoadClick(Sender: TObject);
begin
  IF Sender=Btn_Save
    THEN SaveDialog.Execute;
  IF Sender=Btn_Load
    THEN OpenDialog.Execute;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.DialogCanClose(Sender: TObject;
  var CanClose: Boolean);
var Path: String;
begin
  IF Sender=OpenDialog
    THEN begin
      IF FileExists(OpenDialog.FileName)=TRUE
        THEN Load(LoadFromFile(OpenDialog.FileName))
        ELSE CanClose:=FALSE;
    end;
  IF Sender=SaveDialog
    THEN begin
      Path:=SaveDialog.FileName;
      CheckFileEnding(Path,'.cst',TRUE);
      OpenDialog.FileName;
      IF FileExists(Path)=FALSE
        THEN SaveToFile(CalcResult,Path)
        ELSE begin
          IF MessageDlg(Path+' existiert bereits. �berschreiben?',mtConfirmation,[mbOK,mbCancel],0) = mrOK
            THEN SaveToFile(CalcResult,Path)
            ELSE CanClose:=FALSE;
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_CalcResult.SaveToFile(CR: TCalcResult; Path: String);
var Datei: File of TCalcResult;
    Data: TCalcResult;
begin
  try
    Assignfile(Datei,Path);
    rewrite(Datei);
    Data:=CR;
    write(Datei,Data);
  finally
    CloseFile(Datei);
  end;
end;
//------------------------------------------------------------------------------
function TForm_CalcResult.LoadFromFile(Path: String): TCalcResult;
var Datei: File of TCalcResult;
begin
  try
    Assignfile(Datei,Path);
    reset(Datei);
    read(Datei,result);
  finally
    CloseFile(Datei);
  end;
end;


end.
