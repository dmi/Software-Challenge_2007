unit Unit_Server_Finish;

interface                                                                    

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Unit_TGameBoard;

type
  TForm_Finish = class(TForm)
    La_Winner: TLabel;
    GB_Player0: TGroupBox;
    La_Fische0: TLabel;
    La_Schollen0: TLabel;
    Img_Pinguin0: TImage;
    La_Score0: TLabel;
    La_FieldsCollected0: TLabel;
    GB_Player1: TGroupBox;
    La_Fische1: TLabel;
    La_Schollen1: TLabel;
    La_Score1: TLabel;
    La_FieldsCollected1: TLabel;
    Img_Pinguin1: TImage;
    GB_General: TGroupBox;
    La_Runden: TLabel;
    La_UebrigeSchollen: TLabel;
    La_UebrigeFische: TLabel;
    La_Turns: TLabel;
    La_LostFields: TLabel;
    La_LostFish: TLabel;
    Btn_Cancel: TButton;
    Btn_SaveGame: TButton;
    Btn_NewGame: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_NewGameClick(Sender: TObject);
    procedure Btn_SaveGameClick(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }

    FGameStats: TGameStats;
    procedure SetGameStats(Value: TGameStats);
  public
    { Public-Deklarationen }
    FirstShown: Boolean; //Erstes Spielende schon gemacht? F�r die Sichtbarkeit im Mainmenu
    property GameStats: TGameStats read FGameStats write SetGameStats; //Anzuzeigende GameStats
  end;

var
  Form_Finish: TForm_Finish;    

implementation

uses Unit_Server_Main;

{$R *.dfm}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_Finish.FormCreate(Sender: TObject);
begin
  FirstShown:=FALSE;
end;

//------------------------------------------------------------------------------

procedure TForm_Finish.SetGameStats(Value: TGameStats);
begin
  //LABELS
  //Spieler 0
  GB_Player0.Caption:=Value.Player[0].Name;
  La_Score0.Caption:=IntToStr(Value.Player[0].Score);
  La_Score0.Font.Style:=[];
  La_FieldsCollected0.Caption:=IntToStr(Value.Player[0].FieldsCollected);
  La_FieldsCollected0.Font.Style:=[];
  //Spieler 1
  GB_Player1.Caption:=Value.Player[1].Name;
  La_Score1.Caption:=IntToStr(Value.Player[1].Score);
  La_Score1.Font.Style:=[];
  La_FieldsCollected1.Caption:=IntToStr(Value.Player[1].FieldsCollected);
  La_FieldsCollected1.Font.Style:=[];
  //Allgemein
  La_Turns.Caption:=IntToStr(Value.Turns);
  La_LostFields.Caption:=IntToStr(60 - Value.Player[0].FieldsCollected - Value.Player[1].FieldsCollected);
  La_LostFish.Caption:=IntToStr(100 - Value.Player[0].Score - Value.Player[1].Score);

  //Fische vergleichen
  IF Value.Vorsprung > 0
    THEN begin
      IF Value.Winner=0
        THEN La_Score0.Font.Style:=[fsBold,fsUnderline];
      IF Value.Winner=1
        THEN La_Score1.Font.Style:=[fsBold,fsUnderline];
    end;
  IF (Value.Vorsprung = 0) AND (Value.Winner<2)
    THEN begin
      IF Value.Winner=0
        THEN La_FieldsCollected0.Font.Style:=[fsBold,fsUnderline];
      IF Value.Winner=1
        THEN La_FieldsCollected1.Font.Style:=[fsBold,fsUnderline];
    end;

  //Gewinner anzeigen
  IF Value.Winner<>2
    THEN begin
      La_Winner.Font.Color:=Board.Player[Value.Winner].Color;
      La_Winner.Caption:=Value.Player[Value.Winner].Name;
    end
    ELSE begin
      La_Winner.Font.Color:=clBlack;
      La_Winner.Caption:='Unentschieden';
    end;
  La_Winner.Left:=(self.ClientWidth div 2) - (La_Winner.Width div 2);

  FGameStats:=Value;
end;

//------------------------------------------------------------------------------

procedure TForm_Finish.FormShow(Sender: TObject);
begin
  FirstShown:=TRUE;
end;

//------------------------------------------------------------------------------

procedure TForm_Finish.Btn_CancelClick(Sender: TObject);
begin
  close;
end;

//------------------------------------------------------------------------------

procedure TForm_Finish.Btn_SaveGameClick(Sender: TObject);
begin
  Form_Server.MM_Save.Click;
end;

//------------------------------------------------------------------------------

procedure TForm_Finish.Btn_NewGameClick(Sender: TObject);
begin
  Board.New;
  close;
end;

//------------------------------------------------------------------------------

procedure TForm_Finish.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  self.Btn_SaveGame.Enabled:=TRUE;
  self.Btn_NewGame.Enabled:=TRUE;
  Form_Server.Enabled:=TRUE;
end;


end.
