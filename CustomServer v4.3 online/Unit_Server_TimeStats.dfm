object Form_TimeStats: TForm_TimeStats
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Zeitstatistiken f'#252'r Clients'
  ClientHeight = 235
  ClientWidth = 413
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object La_Counter: TLabel
    Left = 126
    Top = 209
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Counter'
  end
  object La_CentiSec: TLabel
    Left = 192
    Top = 204
    Width = 81
    Height = 25
    AutoSize = False
    Caption = 'Alle Angaben in Centisekunden'
    WordWrap = True
  end
  object Btn_OK: TButton
    Left = 280
    Top = 205
    Width = 57
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = Btn_OKClick
  end
  object LB_Clients: TListBox
    Left = 8
    Top = 32
    Width = 401
    Height = 89
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = False
    TabOrder = 1
    OnClick = LB_ClientsClick
    OnDblClick = LB_ClientsDblClick
  end
  object GB_First: TGroupBox
    Left = 8
    Top = 128
    Width = 129
    Height = 70
    Caption = 'Erster Zug'
    TabOrder = 2
    object La_FirstMinimum0: TLabel
      Left = 8
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Minimum:'
    end
    object La_FirstMaximum0: TLabel
      Left = 8
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Maximum:'
    end
    object La_FirstAverage0: TLabel
      Left = 8
      Top = 16
      Width = 64
      Height = 13
      Caption = 'Durchschnitt:'
    end
    object La_FirstAverage: TLabel
      Left = 88
      Top = 16
      Width = 34
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '100 ms'
      ParentBiDiMode = False
    end
    object La_FirstMaximum: TLabel
      Left = 88
      Top = 32
      Width = 34
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '100 ms'
      ParentBiDiMode = False
    end
    object La_FirstMinimum: TLabel
      Left = 88
      Top = 48
      Width = 34
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '100 ms'
      ParentBiDiMode = False
    end
  end
  object Btn_Delete: TButton
    Left = 8
    Top = 205
    Width = 89
    Height = 25
    Caption = 'L'#246'schen'
    TabOrder = 3
    OnClick = Btn_DeleteClick
  end
  object GB_Other: TGroupBox
    Left = 144
    Top = 128
    Width = 129
    Height = 70
    Caption = 'Sonstige Z'#252'ge'
    TabOrder = 4
    object La_Average0: TLabel
      Left = 8
      Top = 16
      Width = 64
      Height = 13
      Caption = 'Durchschnitt:'
    end
    object La_Maximum0: TLabel
      Left = 8
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Maximum:'
    end
    object La_Minimum0: TLabel
      Left = 8
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Minimum:'
    end
    object La_Average: TLabel
      Left = 88
      Top = 16
      Width = 34
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '100 ms'
      ParentBiDiMode = False
    end
    object La_Maximum: TLabel
      Left = 88
      Top = 32
      Width = 34
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '100 ms'
      ParentBiDiMode = False
    end
    object La_Minimum: TLabel
      Left = 88
      Top = 48
      Width = 34
      Height = 13
      BiDiMode = bdRightToLeft
      Caption = '100 ms'
      ParentBiDiMode = False
    end
  end
  object GroupBox1: TGroupBox
    Left = 280
    Top = 128
    Width = 129
    Height = 70
    Caption = 'Gesamtes Spiel'
    TabOrder = 5
    object La_SumMinimum0: TLabel
      Left = 8
      Top = 48
      Width = 44
      Height = 13
      Caption = 'Minimum:'
    end
    object La_SumMaximum0: TLabel
      Left = 8
      Top = 32
      Width = 48
      Height = 13
      Caption = 'Maximum:'
    end
    object La_SumAverage0: TLabel
      Left = 8
      Top = 16
      Width = 64
      Height = 13
      Caption = 'Durchschnitt:'
    end
    object La_SumAverage: TLabel
      Left = 88
      Top = 16
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = '100 ms'
    end
    object La_SumMaximum: TLabel
      Left = 88
      Top = 32
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = '100 ms'
    end
    object La_SumMinimum: TLabel
      Left = 88
      Top = 48
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = '100 ms'
    end
    object La_PartSum: TLabel
      Left = 87
      Top = 32
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = '100 ms'
    end
    object La_PartSum0: TLabel
      Left = 8
      Top = 32
      Width = 36
      Height = 13
      Caption = 'Aktuell:'
    end
  end
  object RB_Board: TRadioButton
    Left = 16
    Top = 8
    Width = 113
    Height = 17
    Caption = 'Allgemeine Zeiten'
    TabOrder = 6
    OnClick = RefreshListBox
  end
  object RB_Players: TRadioButton
    Left = 152
    Top = 8
    Width = 153
    Height = 17
    Caption = 'Zeiten des aktuellen Spiels'
    Checked = True
    TabOrder = 7
    TabStop = True
    OnClick = RefreshListBox
  end
  object CB_Refresh: TCheckBox
    Left = 312
    Top = 8
    Width = 97
    Height = 17
    Caption = 'Aktualisieren'
    Checked = True
    State = cbChecked
    TabOrder = 8
    OnClick = CB_RefreshClick
  end
  object Btn_Refresh: TButton
    Left = 312
    Top = 8
    Width = 89
    Height = 17
    Caption = 'Aktualisieren'
    TabOrder = 9
    OnClick = Btn_RefreshClick
  end
  object Btn_Cancel: TButton
    Left = 344
    Top = 205
    Width = 65
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 10
    OnClick = Btn_CancelClick
  end
  object CB_Counter: TCheckBox
    Left = 168
    Top = 208
    Width = 17
    Height = 17
    Hint = 'Aktuelle Timer-Zeit anzeigen?'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
    OnClick = CB_CounterClick
  end
end
