unit DebugMemo;



interface

uses
  StdCtrls, classes,SysUtils,Dialogs;


  //----------------------------------------------------------------------------

  type
  TDebugEntry = record
    Text: String;
    Time: TDateTime; //Eintragsdatum
    ShowTime: Boolean; //Soll die Zeit f�r diesen Eintrag angezeigt werden?
  end;
  TDebugEntryArray = array of TDebugEntry;

  //----------------------------------------------------------------------------

  TDebugMemo = class (TMemo)
  private
    { Private-Deklarationen }
    FSendString: String;
    FEntries: TDebugEntryArray;
    FAddTime: Boolean;
    FAddDate: Boolean;
    FTimeSep: ShortString; //String, der die Zeit von dem Text trennt
    procedure SetAddTime(Value: Boolean);
    procedure SetAddDate(Value: Boolean);
    procedure SetTimeSeperator(Value: ShortString);
  public
    { Public-Deklarationen }
    property SendString: String read FSendString write FSendString;
    property Entries: TDebugEntryArray read FEntries;
    property AddTime: Boolean read FAddTime write SetAddTime; //Zeitanzeige
    property AddDate: Boolean read FAddDate write SetAddDate; //Datumsanzeige
    property TimeSeperator: ShortString read FTimeSep write SetTimeSeperator;

    constructor Create(AOwner: TComponent);
    procedure Send; overload;
    procedure Send(S:String); overload;//Sendet den String ins Memo, ShowTime=TRUE
    procedure Send(S: String; ShowTime: Boolean); overload;
    procedure Refresh; //L�dt alle Eintr�ge neu
    procedure Clear;
    procedure SaveToFile(Path: String);
    procedure LoadFromFile(Path: String);
  end;

implementation

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

constructor TDebugMemo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FAddTime:=TRUE;
  FTimeSep:=': ';
  self.ReadOnly:=TRUE;
  self.ScrollBars:=ssVertical;
end;

//------------------------------------------------------------------------------

procedure TDebugMemo.Send;
begin
  Send(SendString, TRUE);
end;

//------------------------------------------------------------------------------

procedure TDebugMemo.Send(S:String);
begin
  Send(S, TRUE);
end;
//------------------------------------------------------------------------------
procedure TDebugMemo.Send(S: String; ShowTime: Boolean);
begin
  setlength(FEntries,length(FEntries)+1);
  FEntries[High(FEntries)].Text:=S;
  FEntries[High(FEntries)].Time:=Now;
  FEntries[High(FEntries)].ShowTime:=ShowTime;
  IF (ShowTime=TRUE) AND (AddTime=TRUE) THEN S:=TimeToStr(Now)+FTimeSep+S;
  self.Lines.Add(S);
end;
//------------------------------------------------------------------------------

procedure TDebugMemo.SetAddTime(Value: Boolean);
var i: Integer;
begin
  IF Value<>FAddTime
    THEN begin
      FAddTime:=Value;
      Refresh;
    end;
end;

//------------------------------------------------------------------------------

procedure TDebugMemo.SetAddDate(Value: Boolean);
begin
  IF Value<>FAddDate
    THEN begin
      FAddDate:=Value;
      Refresh;
    end;
end;

//------------------------------------------------------------------------------

procedure TDebugMemo.SetTimeSeperator(Value: ShortString);
begin
  FTimeSep:=Value;
  IF FTimeSep<>Value THEN Refresh;
end;

//------------------------------------------------------------------------------

procedure TDebugMemo.Refresh;
var S: String;
    i: Integer;
begin
  //Alle Eintr�ge noch einmal neu hinzuf�gen
  inherited clear;
  IF length(FEntries)=0 THEN exit;
  FOR i:=0 to High(FEntries) do
    begin
      S:=FEntries[i].Text;
      IF (FEntries[i].ShowTime=TRUE)
        THEN begin
          IF (self.FAddDate=TRUE)
            THEN S:=DateToStr(FEntries[i].Time)+FTimeSep+FEntries[i].Text;
          IF  (self.FAddTime=TRUE)
            THEN S:=TimeToStr(FEntries[i].Time)+FTimeSep+FEntries[i].Text;
          IF (self.FAddDate=TRUE) AND (self.FAddTime=TRUE)
            THEN S:=DateTimeToStr(FEntries[i].Time)+FTimeSep+FEntries[i].Text;
        end;
      self.Lines.Add(S);
    end;
end;

//------------------------------------------------------------------------------

procedure TDebugMemo.Clear;
begin
  inherited clear;
  setlength(FEntries,0);
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



procedure TDebugMemo.SaveToFile(Path: String);
var Datei: textfile;
    i: Integer;
    L: String; //Eine Zeile
begin
  IF length(FEntries)=0 THEN exit;  
  try
    AssignFile(Datei,Path);
    IF FileExists(Path)=TRUE
      THEN begin
        Erase(Datei);
        Assignfile(Datei,Path);
      end;
    rewrite(Datei);

    L:='Ausgabedatei eines TDebugMemos. Erstellt am: '+DateTimeToStr(Now);
    writeln(Datei,L);

    //Aufbau:  '0'/'1' = ShowTime    Zeitangabe      Text
    FOR i:=0 to High(FEntries) do
      begin
        L:='';
        with FEntries[i] do
          begin
            IF ShowTime=TRUE
              THEN L:='1'
              ELSE L:='0';
            L:=L+' '+DateTimeToStr(Time)+'|'+Text;
          end;
        writeln(Datei,L);
      end;
    writeln(Datei,'end');
  finally
    closefile(Datei);
  end;
end;
//------------------------------------------------------------------------------
procedure TDebugMemo.LoadFromFile(Path: String);
var Datei: textfile;
    L: String; //Eine Zeile
    i: Integer;
begin
  IF FileExists(Path)=FALSE THEN exit;
  try
    Assignfile(Datei,Path);
    reset(Datei);
    Clear;
    readln(Datei,L); //Erste Zeile �berspringen
    readln(Datei,L); //Datensatz beginnt
    REPEAT
      setlength(FEntries,length(FEntries)+1);
      //ShowTime
      IF Copy(L,1,1)='0'
        THEN FEntries[High(FEntries)].ShowTime:=FALSE
        ELSE FEntries[High(FEntries)].ShowTime:=TRUE;
      //Time
      IF i<0
        THEN break
        ELSE FEntries[High(FEntries)].Time:=StrToDateTime(Copy(L,3,19));
      //Text
      FEntries[High(FEntries)].Text:=Copy(L,23,length(L)-22);
      readln(Datei,L);
    until L='end';
    Refresh;
  finally
    closefile(Datei);
  end;
end;

end.
