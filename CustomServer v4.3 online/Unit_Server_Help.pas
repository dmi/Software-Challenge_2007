unit Unit_Server_Help;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls;

type
  TForm_Help = class(TForm)
    Btn_OK: TButton;
    RG_Selection: TRadioGroup;
    La_Title: TLabel;
    La_Selection: TLabel;
    RichEdit_Spiel: TRichEdit;
    RichEdit_Entwickleransicht: TRichEdit;
    RichEdit_Clients: TRichEdit;
    procedure Btn_OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RG_SelectionClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_Help: TForm_Help;
  Lines_Spiel,Lines_Entwickleransicht,Lines_Clients: TStrings;

implementation

uses Unit_Server_Main;

{$R *.dfm}

procedure TForm_Help.RG_SelectionClick(Sender: TObject);
begin
  La_Selection.Caption:='- '+RG_Selection.Items[RG_Selection.Itemindex]+' -';
  La_Selection.Left:=(La_Title.Left + La_Title.Width div 2) - La_Selection.Width div 2;

  //Text anzeigen
  RichEdit_Spiel.Visible:=FALSE;
  RichEdit_Entwickleransicht.Visible:=FALSE;
  RichEdit_Clients.Visible:=FALSE;

  CASE RG_Selection.ItemIndex of
    0: RichEdit_Spiel.Visible:=TRUE;
    1: RichEdit_Entwickleransicht.Visible:=TRUE;
    2: RichEdit_Clients.Visible:=TRUE;
  end;
end;

procedure TForm_Help.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form_Server.Enabled:=TRUE;
end;

procedure TForm_Help.FormCreate(Sender: TObject);
begin
  Btn_OK.Left:=self.ClientWidth div 2 - Btn_OK.Width div 2;
  La_Title.Left:=self.ClientWidth div 2 - La_Title.Width div 2;
  RG_SelectionClick(Self);
end;

procedure TForm_Help.Btn_OKClick(Sender: TObject);
begin
  close;
end;

end.
