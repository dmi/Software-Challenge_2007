package SimpleClient;
import java.io.*;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
   Klasse, die den SimpleClient-Computerspieler darstellt
*/
public class SimpleClient 
{
    Pinguin pinguine[]; /*Array, in dem die Pinguinfiguren gespeichert werden*/
    int turn = 0;       /*Zaehler fuer die Nummer des aktuellen Zuges*/

    
    public SimpleClient()
    {
	pinguine = new Pinguin[4];
	for ( int i=0; i<4; i++ )
	    pinguine[i] = new Pinguin();
    }


    /*
      Sendet den Zug an den Server
    */
    void send(String s)
    {
		System.out.println(s);
    }

    /*
      Findet Position, an der ein Pinguin eingesetzt wird
    */
    String findStart(GameBoard board, int lturn)
    {
	int z,s;              /*z,s speichern Werte fuer Zeilen/Spaltenindizes*/
        boolean flag=false;   /*zeigt an, ob ein Einsetzfeld gefunden wurde*/
	String string = "";   /*speichert den Rueckgabewert*/
	
	z = lturn;
  
	/*laufe durch das Spielfeld bis eine Scholle mit einem Fisch gefunden wird*/

	while  ( (z <= 7) && (!flag) )
	{   /* Schleife laeuft durch die einzelnen Zeilen */
	    s = (int)(6 * Math.random()); /*starte in zufaelliger Spalte*/
	    while ( (s <= 10) && (!flag) ) 
	    { /*Schleife laeuft durch die einzelnen Spalten*/
		if ( (board.getScholle(z,s).fische == 1) && (board.getScholle(z,s).besetzt == false) ) 
		{
		    /*Position des eingesetzten Pinguins merken*/
		    pinguine[turn-1].x = s;
		    pinguine[turn-1].y = z;
		    pinguine[turn-1].bewegungsunfaehig = false;
		     
		    string = z +" "+s+" 0 0"; /*wird dann an den Server gesendet*/
		    flag = true;
		}
		s = s + 1;         /*Spaltenzaehler erhoehen*/
	    }
	    z = z + 1;		  /*Zeilenzaehler erhoehen*/
	}
	return string;
    }

    /*
      Methode zur Bestimmung des naechsten Zuges
    */
    String findMove(GameBoard board)
    {
	/* findet einen Zug nach folgendem Schema:
	   1. Starte mit zufaelligem Pinguin
	   2. Waehle zufaellige Startrichtung
	   3. Versuche per Tiefensuche und durchgehen aller Richtungen ein Feld mit 3 Fischen zu finden,
	   merke dabei das hoechstwertigste bisher besuchte Feld
	   4. Falls bisher kein 3er Feld gefunden so starte mit naechstem Pinguin bei 2.
	   5. Falls alle Pinguine gesucht haben so gebe das hoechstwertigste bisher besuchte Feld zurueck.
	*/


	int z, s, startpinguin, pinguinzaehler, pinguinnr, startrichtung, aktrichtung;
	/*z: zeile, s: spalte*/
	boolean found, blocked, movable; /*found: feld gefunden, blocked: weg blockiert, movable: kann Pinguin sich ueberhaupt noch bewegen*/
	int bestChoice[] = new int[5];  /*beste bisher gefundene Zugmoeglichkeit*/
	Scholle scholle; /*die Scholle, die jeweils begutachtet wird*/
	String result = new String(""); /*der Zug, der zurueck gegeben wird*/

	found = false;
	blocked = false;
	
	bestChoice[0] = -1;  /*Erstmal auf einen ungueltigen Wert initialisieren*/
	bestChoice[2] = 0;
	bestChoice[3] = 0;

	startpinguin = (int)(4 * Math.random()); /*zufaelliger Startpinguin*/
	pinguinnr = startpinguin;
	pinguinzaehler = startpinguin;

	while ( !found )
	{         /*aeussere Schleife iteriert ueber alle Pinguine*/

	    /*falls der Pinguin schon bewegungsunfaehig ist brauchen wir nicht suchen*/
	    if ( pinguine[pinguinnr].bewegungsunfaehig ) 
	    {
		pinguinzaehler = pinguinzaehler + 1;
		pinguinnr = pinguinzaehler % 4;
		if ( pinguinnr == startpinguin ) 
                { /*falls alle Pinguine durchgegangen sind senden wir die beste bisher gefundene Moeglichkeit*/
		    found = true;
		    result = bestChoice[0]+" "+bestChoice[1]+" "+bestChoice[2]+" "+bestChoice[3];
		    /*Position des bewegen Pinguins updaten*/
		    pinguine[bestChoice[4]].y = bestChoice[2];
		    pinguine[bestChoice[4]].x = bestChoice[3];
		}
	    }
            else 
	    {
		movable = false;

		startrichtung = (int)(4 * Math.random());  /*zufaellige Startrichtung*/
		aktrichtung = startrichtung;

		while ( ((aktrichtung == startrichtung) || ((aktrichtung % 6) != startrichtung)) 
			&& !found ) 
		{ /* Schleife iteriert ueber alle 6 moeglichen Richtungen*/
		    blocked = false;
		    z = pinguine[pinguinnr].y;  /*Startzeile setzen*/
		    s = pinguine[pinguinnr].x;  /*Startspalte setzen*/
		    
		    while ( (!found ) && (!blocked) ) 
		    { /*Schleife die in der aktuellen richtung bis zu einem blockierten Feld laeuft*/
			/*je nach Zugrichtung ein neues Zielfeld bestimmen*/
			switch (aktrichtung % 6) 
		        {
			case 0: 
			    /*oben links*/
			    z = z - 1;
			    break;
			case 1: 
			    /*oben rechts*/
			    z = z - 1;
			    s = s + 1;
			    break;
			case 2: 
			    /*rechts*/
			    s = s + 1;
			    break;
			case 3: 
			    /*unten rechts*/
			    z = z + 1;
			    break;
			case 4: 
			    /*unten links*/
			    z = z + 1;
			    s = s - 1;
			    break;
			case 5: 
			    /*links*/
			    s = s - 1;
			    break;
			}
		
			if ( (z >= 0) && (z <= 7) && (s >= 0) && (s <= 10) )
		        { 
			    /*zielfeld noch auf dem Spielbrett?*/
			    scholle = board.getScholle(z,s);
			    if ( !scholle.besetzt ) 
			    { 
				/* Zielfeld besetzt? */
				movable = true;
				if ( scholle.fische == 3 )
			        {
				    /* bei 3 Fischen sind wir fertig*/
				    result = pinguine[pinguinnr].y+" "+pinguine[pinguinnr].x+" "+z+" "+s;
				    /*wir haben einen Zug gefunden und aktualisieren die Position des gezogenen Pinguins*/
				    pinguine[pinguinnr].y = z;
				    pinguine[pinguinnr].x = s;
				    found = true;
				}
				else
				/*Das Feld enthaelt zwar weniger als 3 Fische, aber ist das Feld die aktuell beste Zugmoeglichkeit?*/
				if ( scholle.fische > board.getScholle(bestChoice[2], bestChoice[3]).fische )
				{  
				    bestChoice[0] = pinguine[pinguinnr].y;
				    bestChoice[1] = pinguine[pinguinnr].x;
				    bestChoice[2] = z;
				    bestChoice[3] = s;
				    bestChoice[4] = pinguinnr;
				}
			    } else 
			      {
				  blocked = true; /*Das aktuelle Feld ist besetzt, also suchen wir in der naechsten Richtung weiter*/
			      }
			} else 
			  {
			      blocked = true;   /*Das aktuelle Feld ist ausserhalb des Spielfeldes, also suchen wir in der naechsten Richtung weiter*/
			  }
		
		    } /*Schleife fuer Tiefensuche*/
		    aktrichtung = aktrichtung + 1; /*Pruefe naechste Richtung*/
		} /*Schleife ueber die Zugrichtungen*/
		/*hier ist ein Pinguin abgearbeitet*/
	    
		if ( !movable  && !found ) 
		{ /*gab es ueberhaupt ein valides Zielfeld?*/
		    pinguine[pinguinnr].bewegungsunfaehig = true;
		}
		
		pinguinzaehler = pinguinzaehler + 1; /*Pruefe naechsten Pinguin*/
		pinguinnr = pinguinzaehler % 4;
		
		if ( (pinguinnr == startpinguin) && !found ) 
		{  /*wurde bereits ueber alle Pinguine gesucht?*/
		    found = true;
		    /*dann waehle einfach beste gefundene Moeglichkeit*/
		    result = bestChoice[0]+" "+bestChoice[1]+" "+bestChoice[2]+" "+bestChoice[3];
		    /*Position des gesetzten Pinguins updaten*/
		    pinguine[bestChoice[4]].y = bestChoice[2];
		    pinguine[bestChoice[4]].x = bestChoice[3];
		}
	    }
	}
	return result;
    }

    /*
      Diese Methode ist die Hauptmethode, die beim Start
      ausgefuehrt wird.
    */
    public static void main(String[] args)
    {
    System.setErr(new PrintStream(new ErrorStream("logfile.txt")));
	SimpleClient spieler = new SimpleClient();
	GameBoard globalBoard = new GameBoard();
	int i = 1;
	String input = new String();
	String move = new String();  /*speichert den aktuellen Zug*/
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	while ( i == 1 )
	{
	    
	    try 
	    {
		input = reader.readLine();
	    }
	    catch (Exception e) { System.err.println(e); }
	    
	    if ( input.charAt(0) == '3' )
	    {
		spieler.turn = 0;
	    }	

	    if ( input.charAt(0) == '2' )
	    {
		i = 0;
	    }	
	    
	    if ( input.charAt(0) == '1' )
	    {
		spieler.turn = spieler.turn + 1;
		globalBoard.parseBoard(input);
		
		if ( spieler.turn <= 4 )
		{
		    move = spieler.findStart(globalBoard, spieler.turn);
		}
		else
		{
		    move = spieler.findMove(globalBoard);
		}
		spieler.send(move);
	    }

	    
	}
    }
} /* Ende Klasse SimpleClient */


/*
  Klasse, die einen Pinguin beschreibt
*/
class Pinguin
{
    public int x,y;
    public boolean bewegungsunfaehig;
}



/*
  Klasse, die eine Scholle beschreibt
*/
class Scholle
{
    public int fische;
    public boolean besetzt;

    public Scholle()
    {
	fische = 0;
	besetzt = false;
    }

    public Scholle(Scholle scholle)
    {
	fische = scholle.fische;
	besetzt = scholle.besetzt;
    }
}

/*
  Klasse, die ein Spielbrett repraesentiert
*/
class GameBoard 
{
    private Scholle board[][];

    public GameBoard()
    {
	board = new Scholle[8][11];
        for ( int z=0 ; z<8; z++ )
	    for ( int s=0; s<11; s++ )
		board[z][s] = new Scholle();
    }

    /*
      Aus einem Serverstring das Spielfeld auslesen
    */
    public void parseBoard(String string)
    {

	int z,s,fische;
	String tempstring = new String();
	int besetzt;

	z = 0;
	s = 0;
   
	for (int l = 2; l < string.length(); l++) 
	{  /* an der 3. Stelle beginnt die Beschreibung des Spielfelds */
   	    
	    if (string.charAt(l) == ' ')
	    {
		fische = (Integer.parseInt(tempstring) & 12) / 4; /* die beiden hoechstwertigsten bits geben die Anzahl der Fische an */
		besetzt = (Integer.parseInt(tempstring)  & 3); /* die beiden niederwertigsten Bits geben an ob das Feld besetzt ist */
		System.err.println("z:"+z+" s:"+s);
		board[z][s].fische = fische;
		if ((besetzt != 0) || (fische == 0))
          	{
		    board[z][s].besetzt = true;
		}
		else 
		{
		    board[z][s].besetzt = false;
		}
		s = s + 1;
		if ( s > 10 )
		{
                    z = z + 1;
		    s = 0;
		}
	        tempstring = "";
	    }
	    else 
            {
		tempstring = tempstring + string.charAt(l);
	    }
	}


    }

    /*
      Die Scholle mit den uebergebenen Koordinaten zurueckliefern
    */
    Scholle getScholle(int y, int x)
    {
	Scholle scholle = new Scholle();

	if ( (y<8) && (x<11) && (y>=0) && (x>=0) )
	{
	    scholle = new Scholle(board[y][x]);
	}
	else
	{
	    System.err.println("Request out of bounds" + y + " " + x);
	}

	return scholle;
    }
}

/**
Class for handling the Error Std Stream.
*/
class ErrorStream extends OutputStream {
	private OutputStream outstream;
	private JPanel panel = new JPanel(new BorderLayout());
	private JFrame frame;
	private JTextArea output;
	private JScrollPane jscp;
	private static SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
	public ErrorStream(String logfile) {
		try {
			outstream=new FileOutputStream(logfile, true);
		}
		catch (Exception e) {
			throw new RuntimeException(e.toString());
		}
		frame=new JFrame("Client Status (logging to: "+logfile+")");
		output = new JTextArea();
		output.setEditable(false);
		output.setLineWrap(true);
		output.setFont(new Font("SansSerif", Font.PLAIN, 12));
		frame.setSize(500, 300);
		frame.setLocation(10, 10);
		panel.add(output, BorderLayout.NORTH);
		frame.getContentPane().add(panel);
		jscp=new JScrollPane(panel);
 		frame.getContentPane().add(jscp); 
		frame.setVisible(true);
		addinfo("Client started up...\n");
		addinfo("ErrorStream logged to: "+logfile+"\n");
	}
	public void close() throws IOException{
		//Closes this output stream and releases any system resources associated with this stream. 
		outstream.close();
	}
	public void flush() throws IOException {
		//Flushes this output stream and forces any buffered output bytes to be written out. 
		JScrollBar bar = jscp.getVerticalScrollBar();
		bar.setValue(bar.getMaximum()-bar.getVisibleAmount());
		outstream.flush();
	}
	public void write(byte[] b) throws IOException {
		//Writes b.length bytes from the specified byte array to this output stream. 
		write(b, 0, b.length);
		addinfo(new String(b));	
	}

	public void write(byte[] b, int off, int len) throws IOException {
		//Writes len bytes from the specified byte array starting at offset off to this output stream. 
		outstream.write(b, off, len);
		addinfo(new String(b, off, len));
	}
	
	public void write(int b) throws IOException {
		//Writes the specified byte to this output stream.
		outstream.write(b);
		addinfo(String.valueOf((char)b));
	}
	
	private void addinfo(String addstatus) {
		output.append(addstatus);
	}
}
