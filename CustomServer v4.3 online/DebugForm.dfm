object Form_Debug: TForm_Debug
  Left = 0
  Top = 0
  Width = 434
  Height = 377
  BorderStyle = bsSizeToolWin
  Caption = 'Debug-Output'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu: TMainMenu
    Top = 8
    object MM_File: TMenuItem
      Caption = 'Datei'
      object MM_Save: TMenuItem
        Caption = 'Speichern'
        ShortCut = 16467
        OnClick = MM_SaveClick
      end
      object MM_Load: TMenuItem
        Caption = 'Laden'
        OnClick = MM_LoadClick
      end
      object MM_Close: TMenuItem
        Caption = 'Schlie'#223'en'
        OnClick = MM_CloseClick
      end
    end
    object MM_View: TMenuItem
      Caption = 'Ansicht'
      object MM_Draws: TMenuItem
        AutoCheck = True
        Caption = 'Z'#252'ge'
        OnClick = MM_DrawsClick
      end
      object MM_PlayerNames: TMenuItem
        AutoCheck = True
        Caption = 'Spielernamen'
        OnClick = MM_PlayerNamesClick
      end
      object MM_Time: TMenuItem
        AutoCheck = True
        Caption = 'Zeitanzeige'
        Checked = True
        ShortCut = 16468
        OnClick = MM_TimeClick
      end
      object MM_Date: TMenuItem
        AutoCheck = True
        Caption = 'Datumsanzeige'
        ShortCut = 16452
        OnClick = MM_DateClick
      end
    end
    object MM_Edit: TMenuItem
      Caption = 'Bearbeiten'
      object MM_Clear: TMenuItem
        Caption = 'Leeren'
        ShortCut = 46
        OnClick = MM_ClearClick
      end
      object MM_AddEntry: TMenuItem
        Caption = 'Hinzuf'#252'gen'
        ShortCut = 8237
        OnClick = MM_AddEntryClick
      end
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 'Textdatei (*.txt)|*.txt|Alle Dateien|*'
    OnCanClose = DialogCanClose
    Left = 32
    Top = 8
  end
  object SaveDialog: TSaveDialog
    Filter = 'Textdatei (*.txt)|*.txt|Alle Dateien|*'
    OnCanClose = DialogCanClose
    Left = 64
    Top = 8
  end
end
