unit Unit_Server_Options;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Spin, Unit_TGameBoard, Unit_CustomProc;

type
  TForm_Options = class(TForm)
    GB_Player0: TGroupBox;
    Ed_Name0: TEdit;
    La_Name0: TLabel;
    CB_CPUControlled0: TCheckBox;
    La_ClientPath0: TLabel;
    Ed_ClientPath0: TEdit;
    Btn_OpenDialog0: TButton;
    OpenDialog_ClientPath0: TOpenDialog;
    GB_Player1: TGroupBox;
    La_Name1: TLabel;
    La_ClientPath1: TLabel;
    Ed_Name1: TEdit;
    CB_CPUControlled1: TCheckBox;
    Ed_ClientPath1: TEdit;
    Btn_OpenDialog1: TButton;
    OpenDialog_ClientPath1: TOpenDialog;
    Btn_OK: TButton;
    Btn_Abort: TButton;
    GB_SaveLoad: TGroupBox;
    CB_StatSave: TCheckBox;
    Ed_StatPath: TEdit;
    Btn_StatPath: TButton;
    CB_SaveSteps: TCheckBox;
    Img_Pinguin1: TImage;
    GB_View: TGroupBox;
    CB_HexTranspareny: TCheckBox;
    GB_Clients: TGroupBox;
    CB_Wait: TCheckBox;
    SE_Wait: TSpinEdit;
    La_Wait: TLabel;
    La_ms1: TLabel;
    Pa_ColorMarkPlayer: TPanel;
    Pa_ColorMarkClient: TPanel;
    ColorDialog_Player: TColorDialog;
    ColorDialog_Client: TColorDialog;
    La_clMarkPlayer: TLabel;
    La_clMarkClient: TLabel;
    CB_AutoMove: TCheckBox;
    La_Warning: TLabel;
    La_GameShowInterval: TLabel;
    SE_GameShowInterval: TSpinEdit;
    La_ms2: TLabel;
    Btn_AutoSave: TButton;
    Ed_AutoSave: TEdit;
    CB_AutoSave: TCheckBox;
    CB_QuadVisible: TCheckBox;
    CB_HexVisible: TCheckBox;
    SaveDialog_AutoSave: TSaveDialog;
    SaveDialog_StatPath: TSaveDialog;
    Btn_Standart: TButton;
    Btn_ClientRestart0: TButton;
    Btn_ClientRestart1: TButton;
    CB_ShowPinguinHints: TCheckBox;
    RG_StartPlayer: TRadioGroup;
    GB_Other: TGroupBox;
    CB_Debug: TCheckBox;
    CB_Java0: TCheckBox;
    CB_Java1: TCheckBox;
    La_TimeOut: TLabel;
    SE_TimeOut: TSpinEdit;
    La_sec: TLabel;
    CB_AutoContinue: TCheckBox;
    procedure CB_JavaClick(Sender: TObject);
    procedure Btn_StandartClick(Sender: TObject);
    procedure OpenDialog_ClientPathCanClose(Sender: TObject;
      var CanClose: Boolean);
    procedure Btn_AbortClick(Sender: TObject);
    procedure Btn_OKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_OpenDialogClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox(Sender:TObject);
    procedure PanelColor(Sender: TObject);
    procedure RestartClient(Sender: TObject);
  private
    { Private-Deklarationen }
    procedure SaveChanges;
    procedure GameShowFields(Enabled: Boolean);
  public
    { Public-Deklarationen }
  end;

var
  Form_Options: TForm_Options;
  Settings: TGameSettings;
  GameShowMessageShown: Boolean; //Wurde der GameShow-Hinweis schon angezeigt?

implementation

{$R *.dfm}                

uses Unit_Server_Main, Unit_Server_TimeStats;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

procedure TForm_Options.FormShow(Sender: TObject);
begin
  IF Form_Server.CB_ClientAuto.Visible=TRUE
    THEN Form_Server.CB_ClientAuto.Checked:=FALSE;

  Settings:=Unit_Server_Main.Board.Settings;
  //Spieler 0
  Ed_Name0.Text:= Settings.Name0;
  CB_CPUControlled0.Checked:= Settings.CPUControlled0;
  Ed_ClientPath0.Text:= Settings.ClientPath0;
  OpenDialog_ClientPath0.FileName:= Settings.ClientPath0;
  CB_Java0.Checked:=Settings.JavaClient0;

  //Spieler 1
  Ed_Name1.Text:= Settings.Name1;
  CB_CPUControlled1.Checked:= Settings.CPUControlled1;
  Ed_ClientPath1.Text:= Settings.ClientPath1;
  CB_Java1.Checked:=Settings.JavaClient1;
  OpenDialog_ClientPath1.FileName:= Settings.ClientPath1;

  //Clients
  CB_AutoMove.Checked:= Settings.AutoMove0;
  CB_Wait.Checked:= Settings.ClientWait0;
  SE_Wait.Value:= Settings.WaitSleep0;

  //Ansicht
  CB_HexTranspareny.Checked:= Settings.HexTransparency;
  CB_ShowPinguinHints.Checked:=Settings.ShowPinguinHints;
  Pa_ColorMarkPlayer.Color:= Settings.clMarkPlayer;
  Pa_ColorMarkClient.Color:= Settings.clMarkClient;
  CB_QuadVisible.Checked:= Settings.QuadVisible;
  CB_HexVisible.Checked:= Settings.HexVisible;
  CB_Debug.Checked:=Settings.ShowDebugForm;

  //Speichern Laden Abspielen
  CB_SaveSteps.Checked:= Settings.SaveTurnOption;
  SE_GameShowInterval.Value:= Settings.GameShowInterval;
  CB_AutoContinue.Checked:=Settings.AutoContinue;
  //AutoSpeichern Spiel
  CB_AutoSave.Checked:= Settings.AutoSave;
  Ed_AutoSave.Text:= Settings.AutoSavePath;
  SaveDialog_AutoSave.FileName:= Settings.AutoSavePath;
  //AutoSpeichern Statistik
  CB_StatSave.Checked:= Settings.StatSave;
  Ed_StatPath.Text:= Settings.StatPath;
  SaveDialog_StatPath.FileName:= Settings.StatPath;

  //Sonstiges
  IF Settings.StartPlayer=2
    THEN RG_StartPlayer.ItemIndex:=0
    ELSE RG_StartPlayer.ItemIndex:=Settings.StartPlayer+1;
  SE_TimeOut.Value:=Settings.ClientTimeOut div 100;


  //GameShow
  IF Board.GameShow=TRUE
    THEN begin
      GameShowFields(FALSE);
      IF GameShowMessageShown=FALSE
        THEN begin
          ShowMessage('W�hrend des Abspielens eines gespeicherten Spiels sind einige Optionen nicht verf�gbar.');
          GameShowMessageShown:=TRUE;
        end;
    end
    ELSE GameShowFields(TRUE);

  CheckBox(self);
  
  //Noch nicht eingebaute Features     test
  CB_StatSave.Enabled:=FALSE;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.SaveChanges;
begin
  //Players
  Settings.Name0:=Ed_Name0.Text;
  Settings.Name1:=Ed_Name1.Text;
  Settings.CPUControlled0:=CB_CPUControlled0.Checked;
  Settings.CPUControlled1:=CB_CPUControlled1.Checked;
  IF Settings.CPUControlled0=TRUE
    THEN begin
      Settings.ClientPath0:=Ed_ClientPath0.Text;
      Settings.JavaClient0:=CB_Java0.Checked;
    end;
  IF Settings.CPUControlled1=TRUE
    THEN begin
      Settings.ClientPath1:=Ed_ClientPath1.Text;
      Settings.JavaClient1:=CB_Java1.Checked;
    end;

  //Clients
  Settings.AutoMove0:=CB_AutoMove.Checked;
  Settings.AutoMove1:=CB_AutoMove.Checked;
  Settings.ClientWait0:=CB_Wait.Checked;
  Settings.ClientWait1:=CB_Wait.Checked;
  Settings.WaitSleep0:=SE_Wait.Value;
  Settings.WaitSleep1:=SE_Wait.Value;

  //Ansicht
  Settings.HexVisible:=CB_HexVisible.Checked;
  Settings.HexTransparency:=CB_HexTranspareny.Checked;
  Settings.ShowPinguinHints:=CB_ShowPinguinHints.Checked;
  Settings.clMarkPlayer:=Pa_ColorMarkPlayer.Color;
  Settings.clMarkClient:=Pa_ColorMarkClient.Color;
  Settings.QuadVisible:=CB_QuadVisible.Checked;
  Settings.ShowDebugForm:=CB_Debug.Checked;

  //Speichern Laden Abspielen
  Settings.SaveTurnOption:=CB_SaveSteps.Checked;
  Settings.AutoSave:=CB_AutoSave.Checked;
  Settings.AutoSavePath:=Ed_AutoSave.Text;
  Settings.StatSave:=CB_StatSave.Checked;
  Settings.StatPath:=Ed_StatPath.Text;
  Settings.GameShowInterval:=SE_GameShowInterval.Value;
  Settings.AutoContinue:=CB_AutoContinue.Checked;

  //Sonstiges
  IF RG_StartPlayer.ItemIndex=0
    THEN Settings.StartPlayer:=2
    ELSE Settings.StartPlayer:=RG_StartPlayer.ItemIndex-1;
  Settings.ClientTimeOut:=SE_TimeOut.Value*100;

  //Settings �bergeben
  Unit_Server_Main.Board.Settings:=Settings;

  //Main-Formular
  Form_Server.MM_DebugClick(self);
  Form_Server.MM_EntwicklungClick(self);
  Form_Server.MM_HexClick(self);
  Form_Server.MM_TransparencyClick(self);
end;

//------------------------------------------------------------------------------

procedure TForm_Options.CheckBox(Sender:TObject);
begin
  //Client 0
  Ed_ClientPath0.Enabled:=CB_CPUControlled0.Checked;
  Btn_OpenDialog0.Enabled:=CB_CPUControlled0.Checked;
  La_ClientPath0.Enabled:=CB_CPUControlled0.Checked;
  CB_Java0.Enabled:=CB_CPUControlled0.Checked;
  Btn_ClientRestart0.Enabled:=CB_CPUControlled0.Checked;
  IF CB_CPUControlled0.Checked=TRUE
    THEN Ed_ClientPath0.Color:=clWindow
    ELSE Ed_ClientPath0.Color:=clInactiveCaptionText;
  IF CB_CPUControlled0.Enabled=FALSE
    THEN begin
      Ed_ClientPath0.Color:=clInactiveCaptionText;
      Ed_ClientPath0.Enabled:=FALSE;
      Btn_OpenDialog0.Enabled:=FALSE;
      La_ClientPath0.Enabled:=FALSE;
      CB_Java0.Enabled:=FALSE;
    end;
  IF (CB_Java0.Checked=TRUE)
    THEN Btn_OpenDialog0.Enabled:=FALSE
    ELSE Btn_OpenDialog0.Enabled:=TRUE;

  //Client 1
  Ed_ClientPath1.Enabled:=CB_CPUControlled1.Checked;
  Btn_OpenDialog1.Enabled:=CB_CPUControlled1.Checked;
  La_ClientPath1.Enabled:=CB_CPUControlled1.Checked;
  CB_Java1.Enabled:=CB_CPUControlled1.Checked;
  Btn_ClientRestart1.Enabled:=CB_CPUControlled1.Checked;
  IF CB_CPUControlled1.Checked=TRUE
    THEN Ed_ClientPath1.Color:=clWindow
    ELSE Ed_ClientPath1.Color:=clInactiveCaptionText;
  IF CB_CPUControlled1.Enabled=FALSE
    THEN begin
      Ed_ClientPath1.Color:=clInactiveCaptionText;
      Ed_ClientPath1.Enabled:=FALSE;
      Btn_OpenDialog1.Enabled:=FALSE;
      La_ClientPath1.Enabled:=FALSE;
      CB_Java1.Enabled:=FALSE;
    end;
  IF (CB_Java1.Checked=TRUE)
    THEN Btn_OpenDialog1.Enabled:=FALSE
    ELSE Btn_OpenDialog1.Enabled:=TRUE;

  //Statistik
  Ed_StatPath.Enabled:=CB_StatSave.Checked;
  Btn_StatPath.Enabled:=CB_StatSave.Checked;
  IF CB_StatSave.Checked=TRUE
    THEN Ed_StatPath.Color:=clWindow
    ELSE Ed_StatPath.Color:=clInactiveCaptionText;
  IF CB_StatSave.Enabled=FALSE
    THEN begin
      Ed_StatPath.Color:=clInactiveCaptionText;
      Ed_StatPath.Enabled:=FALSE;
      Btn_StatPath.Enabled:=FALSE;
    end;

  //Behalte Runden im Zwischenspeicher
  CB_AutoSave.Enabled:=CB_SaveSteps.Checked;
  IF CB_SaveSteps.Checked=FALSE
    THEN CB_AutoSave.Checked:=FALSE;
  //AutoSpeichern
  Ed_AutoSave.Enabled:=CB_AutoSave.Checked;
  Btn_AutoSave.Enabled:=CB_AutoSave.Checked;
  IF CB_AutoSave.Checked=TRUE
    THEN Ed_AutoSave.Color:=clWindow
    ELSE Ed_AutoSave.Color:=clInactiveCaptionText;
  IF CB_AutoSave.Enabled=FALSE
    THEN Ed_AutoSave.Color:=clInactiveCaptionText;

  //Clients
  SE_Wait.Enabled:=CB_Wait.Checked;
  La_Wait.Enabled:=CB_Wait.Checked;
  La_ms1.Enabled:= CB_Wait.Checked;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.CB_JavaClick(Sender: TObject);
begin
  IF (Settings.JavaWarningShown=FALSE) AND ( ((Sender=CB_Java0) AND (CB_Java0.Checked=TRUE)) OR ((Sender=CB_Java1) AND (CB_Java1.Checked=TRUE)) )
    THEN begin
      ShowMessage('F�r Javaclients geben Sie bitte direkt den Befehl zum Aufruf des Clients in das Editfeld des Pfades ein. Eine �berpr�fung auf Richtigkeit findet nicht statt!');
      Settings.JavaWarningShown:=TRUE;
    end;

  CheckBox(Sender);
end;

//------------------------------------------------------------------------------

procedure TForm_Options.RestartClient(Sender: TObject);
var Client: TBit;
begin
  IF Sender=Btn_ClientRestart1 THEN Client:=1;
  IF Sender=Btn_ClientRestart0 THEN Client:=0;
  //Beenden
  Board.Player[Client].SendToClient(2);
  //Starten
  Board.Player[Client].StartClient;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.Btn_OKClick(Sender: TObject);
var Path: String;
begin
  //Einstellungen �berpr�fen
  IF (CB_CPUControlled0.Checked=TRUE)
    THEN begin
      Path:=Ed_ClientPath0.Text;
      IF (CB_Java0.Checked=FALSE)
        THEN begin
          IF CheckClientPath(Path,true)=FALSE THEN exit;
        end
        ELSE begin
          IF (Path='') OR (Path=' ')
            THEN begin
              ShowMessage('Das Feld der Kommandozeile zum Start des Clients 0 ist leer!');
              exit;
            end;
        end;
    end;
  IF (CB_CPUControlled1.Checked=TRUE)
    THEN begin
      Path:=Ed_ClientPath1.Text;
      IF (CB_Java1.Checked=FALSE)
        THEN begin
          IF CheckClientPath(Path,true)=FALSE THEN exit;
        end
        ELSE begin
          IF (Path='') OR (Path=' ')
            THEN begin
              ShowMessage('Das Feld der Kommandozeile zum Start des Clients 1 ist leer!');
              exit;
            end;
        end;
    end;

  SaveChanges;
  Close;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.Btn_AbortClick(Sender: TObject);
begin
  Close;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Einstellungen speichern
  Board.SaveGameSettingsInFile(Board.Settings,Board.GameSettingsPath);

  Form_Server.LabelRefreshTurn;
  Form_TimeStats.RefreshListBox(self);
  Form_Server.Btn_ClientTurn.Enabled:=Board.Player[Board.CurrentPlayer].CPUControlled;
  Form_Server.Enabled:=TRUE;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//################################
//#######  OPEN-DIALOGS  #########
//################################


procedure TForm_Options.Btn_OpenDialogClick(Sender: TObject);
begin
  IF Sender=Btn_OpenDialog1 THEN OpenDialog_ClientPath1.Execute;
  IF Sender=Btn_OpenDialog0 THEN OpenDialog_ClientPath0.Execute;
  IF Sender=Btn_StatPath    THEN SaveDialog_StatPath.Execute;
  IF Sender=Btn_AutoSave    THEN SaveDialog_AutoSave.Execute;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.OpenDialog_ClientPathCanClose(Sender: TObject;
  var CanClose: Boolean);
var Path: String;
    buttoncode: word;
begin
  IF Sender=OpenDialog_ClientPath0 THEN Ed_ClientPath0.Text:=OpenDialog_ClientPath0.FileName;
  IF Sender=OpenDialog_ClientPath1 THEN Ed_ClientPath1.Text:=OpenDialog_ClientPath1.FileName;
  IF Sender=SaveDialog_AutoSave
    THEN begin
      Path:=SaveDialog_AutoSave.FileName;
      CheckFileEnding(Path,'.pap',true);
      IF FileExists(Path)=TRUE
        THEN begin
          buttoncode:=MessageDlg('Datei '+Path+' �berschreiben?',mtConfirmation,[mbYES,mbNO],0);
          IF Buttoncode=mrNo THEN exit;
        end;
      Ed_AutoSave.Text:=Path;
      SaveDialog_AutoSave.FileName:=Path;
    end;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


procedure TForm_Options.PanelColor(Sender: TObject);
begin
  IF Sender=Pa_ColorMarkPlayer
    THEN begin
      ColorDialog_Player.Color:=Pa_ColorMarkPlayer.Color;
      ColorDialog_Client.Color:=Pa_ColorMarkClient.Color;
      ColorDialog_Player.Execute;
    end;
  IF Sender=Pa_ColorMarkClient
    THEN begin
      ColorDialog_Client.Color:=Pa_ColorMarkClient.Color;
      ColorDialog_Player.Color:=Pa_ColorMarkPlayer.Color;
      ColorDialog_Client.Execute;
    end;
  Pa_ColorMarkPlayer.Color:=ColorDialog_Player.Color;
  Pa_ColorMarkClient.Color:=ColorDialog_Client.Color;
end;

//------------------------------------------------------------------------------

procedure TForm_Options.GameShowFields(Enabled: Boolean);
begin
    GB_Player0.Enabled:=Enabled;
    GB_Player1.Enabled:=Enabled;
    GB_Clients.Enabled:=Enabled;

    //Spieler
    Ed_Name0.Enabled:=Enabled;
    La_Name0.Enabled:=Enabled;
    CB_CPUControlled0.Enabled:=Enabled;
    CB_Java0.Enabled:=Enabled;

    Ed_Name1.Enabled:=Enabled;
    La_Name1.Enabled:=Enabled;
    CB_CPUControlled1.Enabled:=Enabled;
    CB_Java1.Enabled:=Enabled;

    La_ClientPath0.Enabled:=Enabled;
    Ed_ClientPath0.Enabled:=Enabled;
    Btn_OpenDialog0.Enabled:=Enabled;
    Btn_ClientRestart0.Enabled:=Enabled;

    La_ClientPath1.Enabled:=Enabled;
    Ed_ClientPath1.Enabled:=Enabled;
    Btn_OpenDialog1.Enabled:=Enabled;
    Btn_ClientRestart1.Enabled:=Enabled;

    //Speichern Laden Abspielen
    CB_SaveSteps.Enabled:=Enabled;
    CB_StatSave.Enabled:=Enabled;

    Ed_StatPath.Enabled:=Enabled;
    Btn_StatPath.Enabled:=Enabled;

    CB_AutoMove.Enabled:=Enabled;

    //Clients
    CB_Wait.Enabled:=Enabled;

    SE_Wait.Enabled:=Enabled;
    La_Wait.Enabled:=Enabled;
    La_ms1.Enabled:=Enabled;
    La_Warning.Enabled:=Enabled;

    CB_AutoSave.Enabled:=Enabled;

    Btn_AutoSave.Enabled:=Enabled;
    Ed_AutoSave.Enabled:=Enabled;

    //Sonstiges
    RG_StartPlayer.Enabled:=Enabled;

    CheckBox(self);

end;

//------------------------------------------------------------------------------

procedure TForm_Options.Btn_StandartClick(Sender: TObject);
var buttoncode: word;
begin
  buttoncode:=MessageDlg('Standarteinstellungen wiederherstellen?',mtConfirmation,[mbOk,mbAbort],0);
  IF buttoncode=mrok
    THEN begin
      unit_Server_Main.Board.Settings:= unit_Server_Main.Board.StandartSettings;
      FormShow(self);
    end;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------





//################################
//############  TEST  ############
//################################


end.
