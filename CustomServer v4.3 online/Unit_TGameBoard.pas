unit Unit_TGameBoard;

interface

uses                                                            
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, math, ExtCtrls, Menus, StdCtrls, Spin, Grids, DosCommand, Unit_CustomProc,
  Unit_Timer;

type
  TGameBoard = class; //Diese Form der Deklaration (Vorw�rtsdeklaration) wird ben�tigt,
  TPinguin = class; //um innerhalb von TPinguin eine Instanz von TGameBoard und in TGameBoard
  TPlayer = class;  //eine Instanz von TPinguin haben zu k�nnen.

  TBit = 0..1;
  TDirection = 0..5; //Richtungen in der Matrix: 0: oben; 1: rechts oben;
                    //2: rechts; 3: unten; 4: links unten; 5: links
  TStartPlayer = 0..2;  //0: Spieler0 startet; 1: Spieler1 startet; 2: Zufall

  TScholle = record
    fish: byte;
    valid: Boolean; //Existiert die Scholle? Wenn nicht auf dem Spielfeld oder untergegangen: valid=FALSE
    OnGameBoard: Boolean; //Liegt die Scholle auf dem Spielfeld?
    case full: Boolean of  //TScholle.Player kann nur aufgerufen werden, wenn Full=TRUE ist
      TRUE: (Player: TBit;         //Spieler, der einen Pinguin auf dem Feld stehen hat
             PinguinNummer: byte;); //Nummer des Pinguins (0..3) des Spielers
  end;

  //########## STATISTIK ##############

  TClientTimeStats = record //Statistik f�r die Zugzeiten eines Clients
    Path: String[255];
    FirstAverage,       //in Centisekunden
    FirstMaximum,       //in Centisekunden
    FirstMinimum,       //in Centisekunden
    Average,            //in Centisekunden
    Minimum,            //in Centisekunden
    Maximum,            //in Centisekunden
    SumAverage,         //in Centisekunden
    SumMinimum,         //in Centisekunden
    SumMaximum,         //in Centisekunden
    PartSum,            //in Centisekunden, zum Zusammenz�hlen der Z�ge in einem Spiel
    NumberOfGames       //Anzahl der Spiele, die in den Sums berechnet sind.
                : Integer;
  end;
  TClientTimeStatsArray = record
    Arr: Array[0..64] of TClientTimeStats;
    length: Integer; //Anzahl der besetzten Stellen im Array
  end;


  TStatsPlayer = record //Statistik f�r einen Spieler in einem Spiel
    Name: String[128];
    Score,FieldsCollected: Integer;
    CASE CPUControlled: Boolean of
      TRUE: (ClientPath: String[255];
              Time: TClientTimeStats;);
  end;
  TGameStats = record //Statistik f�r ein Spiel
    Player: array[0..1] of TStatsPlayer;
    Turns: Integer;
    Winner: 0..2;  //Wenn Winner=2, dann ist unentschieden
    Vorsprung: byte; //Punktevorsprung des Gewinnders. Wenn Score gleich, dann ist es NUll
  end;

  //############# SPEICHERN ###############

  TSavedTurn = record
    Fields: String;
    CurrentPlayer: TBit;
    Score: array[0..1] of integer;
    FieldsCollected: array[0..1] of integer;
  end;
  TSavedTurnArr = array of TSavedTurn;

  //############ EINSTELLUNGEN ################

  TListClient = record //F�r die Testreihen
    Name: String[255];
    Path: String[255];
    Java: Boolean; //Soll auf *.exe �berpr�ft werden?
  end;
  TListClientArray = array[0..32] of TListClient;

  TField = array[0..10,0..7] of TScholle;
  TIntArray = array[0..1] of Integer;
  TStringArray = array of String;

  TGameSettings = record //Alle Spiel-Einstellungen, die auch gespeichert werden k�nnen
    HexVisible: Boolean;
    HexTransparency: Boolean;
    ShowPinguinHints: Boolean;

    ShowDebugForm: Boolean;
    DebugFormPos: TPoint; //Left und Top des Formulars
    DebugFormSize: TPoint; //X: Width,  Y: Height
    DebugAddTime: Boolean;
    DebugPlayerName: Boolean;
    DebugSendDraw: Boolean;

    ShowTimeForm: Boolean;
    TimeFormPos: TPoint;  //Left und Top des Formulars
    ShowCounter: Boolean; //Der Echtzeitcounter in Form_TimeStats


    clMarkPlayer: TColor;
    clMarkClient: TColor;
    QuadVisible: Boolean;

    SaveTurnOption: Boolean;
    AutoSave: Boolean;
    AutoSavePath: String[255];
    AutoContinue: Boolean; //Soll das letzte Spiel automatisch fortgesetzt werden?
    StatSave: Boolean;
    StatPath: String[255];
    GameShowInterval: integer;
    StartPlayer: TStartPlayer;
    ClientTimeOut: Integer; //Zeit�berschreitung, in Centisekunden
    JavaWarningShown: Boolean;

    //Player 0
    Name0: String[128];
    CPUControlled0: Boolean;
    ClientPath0: String[255];
    AutoMove0: Boolean;
    ClientWait0: Boolean;
    WaitSleep0: Integer;
    JavaClient0: Boolean;

    //Player 0
    Name1: String[128];
    CPUControlled1: Boolean;
    ClientPath1: String[255];
    AutoMove1: Boolean;
    ClientWait1: Boolean;
    WaitSleep1: Integer;
    JavaClient1: Boolean;
  end;

  TTestSettings = record //Alle Einstellungen f�r die Testreihen
    Clients0: TListClientArray;
    Clients1: TListClientArray;
    AmountOfGames: Integer; //Spiele pro Begegnung
    WaitEachTurn: Integer; //Verz�gerung, wenn 0, dann keine Verz�gerung
    AfterEachGame: 0..2; //0: Clients neustarten, 1: Clients zur�cksetzen, 2: gar nichts
    StartPlayer: TStartPlayer;
    SaveGames: Boolean; //Sollen alle Aufzeichnungen gespeichert werden?
    SavePath: String[255];
    Index0,Index1,High0,High1: Integer; //Z�hlvariablen, 0 und 1 f�r Spieler, Index f�r aktuelles Spiel, High f�r Anzahl der Clients
    Counter: Integer; //Z�hlvariable f�r die Nummer des Spiels der aktuellen Begegnung - beginnt mit 0
  end;

//------------------------------------------------------------------------------
  TPinguin = class
    private
      FPosed: Boolean;
      FPosition: TPoint;
      FGameBoard: TGameBoard;
      function GetMovable: Boolean;
      function GetFish: byte;
      procedure SetPosition(Value: TPoint);
      procedure SetGameBoard(Value: TGameBoard);
    public
      property movable: Boolean read GetMovable; //Ist der Pinguin noch zu bewegen?
      property GameBoard: TGameBoard read FGameBoard write SetGameBoard;
      property Posed: Boolean read FPosed; //Ist der Pinguin schon platziert und im Spiel?
      property Fish: byte read GetFish; //Fisch, auf dem der Pinguin steht
      property Position: TPoint read FPosition write SetPosition; //Position in der Matrix
            //Die Instanz von TGameBoard, in denen sich die Pinguine befinden
      function GetAllFish: integer; //Die Summe des zu erreichenden Fischs, Arbeitet mit TGameBoard.GetAllFish
      function GetAllTargets: TPointArray; //Alle zu erreichenden Felder; arbeitet mit TGameBoard.GetAllTargets
      function GetTargetsInDirection(Direction: TDirection): TPointArray;
                  //Alle zu erreichenden Felder in Richtung; arbeitet mit TGameBoard.GetTargetsInDirection
  end;

  TPinguinArray = array[0..3] of TPinguin;

//------------------------------------------------------------------------------
  TPlayer = class
  private
    FGameBoard: TGameBoard;
    FColor: TColor;
    SaveName: String; //Zwischengespeicherter Name (wird f�r die GameShow gebraucht)
    FName: String;
    FNumber: TBit;
    FPinguin: TPinguinArray;
    FScore: integer;
    FFieldsCollected: byte;
    FAmZug: Boolean;

    FDosCommand: TDosCommand;
    FCPUControlled: Boolean;
    FJavaClient: Boolean;
    FClientPath: String;
    FAutoMove: Boolean;
    FClientWait: Boolean;
    FWaitSleep: integer;
    FClientIsDrawing: Boolean;
    FTimeStats: TClientTimeStats;
    property DosCommand: TDosCommand read FDosCommand write FDosCommand;

    constructor Create(GameBoard:TGameBoard);

    procedure SetName(Value:String);
    function GetMovable: Boolean;
    function GetAllPinguinsPosed: Boolean;
    procedure SetCPUControlled(Value: Boolean);
    procedure SetClientPath(Value: String);
    procedure SetGameBoard(Value: TGameBoard);
    property GameBoard: TGameBoard read FGameBoard write SetGameBoard;

    function GetTimeStats: TClientTimeStats;
  public
    property Color: TColor read FColor;
    property Name: String read FName write SetName;
    property Number: TBit read FNumber;
    property movable: Boolean read GetMovable; //Sind Pinguine des Spielers noch zu bewegen?
    property AllPinguinsPosed: Boolean read GetAllPinguinsPosed;
    property Pinguin: TPinguinArray read FPinguin;
    property Score: integer read FScore; //Anzahl der bisher gesammelten Fische
    property FieldsCollected: byte read FFieldsCollected;
    property AmZug: Boolean read FAmZug; // Da ist mir kein englischer Ausdruck eingefallen ^^

    //Client
    property CPUControlled: Boolean read FCPUControlled write SetCPUControlled; //KI-kontrolliert?
    property JavaClient: Boolean read FJavaClient write FJavaClient; //Ist der Spieler ein JavaClient?
    property ClientPath: String read FClientPath write SetClientPath; //Dateipfad f�r Client-ExeDatei
    property AutoMove: Boolean read FAutoMove write FAutoMove; //Automatisch Aufforderung an Client senden?
    property ClientWait: Boolean read FClientWait write FClientWait; //Gibt es Verz�gerung?
    property WaitSleep: integer read FWaitSleep write FWaitSleep; //Verz�gerung des Clientzuges in ms
    property ClientIsDrawing: Boolean read FClientIsDrawing;
    property TimeStats: TClientTimeStats read GetTimeStats; //Wird in der neuen Runde zur�ckgesetzt und am Ende des Spiels gespeichert.
    procedure StartClient; //Startet den Client, so dass er im Hintergrund l�uft.
    procedure SendToClient(command: byte); overload;
        //Sendet Spielfeldcode und Zuganweisungen (command) zum Client
    procedure SendToClient(S: String); overload; //Sendet String an Client
  end;

//------------------------------------------------------------------------------

  TPapImage = class (TImage)
  private
    Position: TPoint;
//    procedure SetPosition (Value:TPoint);
//  public
//    property Position: TPoint read FPosition write SetPosition; //Position des Images in der Matrix
  end;

//------------------------------------------------------------------------------


  //   EREIGNISSE
  TNewTurnEvent = procedure of object; //Bei neuer Runde
  TMouseMoveEvent = procedure of object; //Bei Mausbewegung
  TGameFinishEvent = procedure (GameStats: TGameStats; Index0,Index1,Counter, Time: Integer) of object; //Bei Ende eines Spiels, Indizes und Counter f�r die Position in der Testreihe
  TTimeOutEvent = procedure (Sender: TObject; Client: TBit; Time: Integer) of object; //Bei Zeit�berschreitung eines Clients
  TErrorReportEvent = procedure(Msg: String) of object;

  TGameBoard = class
  private
    //#### Allgemein ####
    FField : TField;
    FTurn: Integer; //Nummer der aktuellen Spielrunde, 0 hei�t kein Spiel laufend; eine Runde
                  //ist ein Spielzug eines Spielers, also beginnt auch beim Spielerwechsel eine neue Runde
    FStartPlayer: TStartPlayer;
    FCalculating: Boolean;
    Timer_Finish: TTimer; //L�sst die Form_Finish kurz nach dem letzten Zug erscheinen.
    FTimer_ClientDraw: TTimer;
    FMyTimer: TMyTimer;
    FClientTimeStats: TClientTimeStatsArray;
    FClientTimeStatsPath: String;
    FClientTimeOut: Integer;
    FJavaWarningShown: Boolean;

    //#### Visualisierung ####
    Form: TForm; //Das Formular, auf das die Bilder gemalt werden k�nnen
    imgPos: array[0..10,0..7] of TPoint; //Die Positionen der Images f�r Left und Top
    Img_Fish: array[1..3,1..30] of TPapImage;
    Img_Pinguin: array[0..1,0..3] of TPapImage; //Spieler 0 hat Rot, Spieler 1 hat Blau
    Bmp_Fish1, Bmp_Fish2, Bmp_Fish3, Bmp_PinguinRed, Bmp_PinguinBlue: TBitmap;
    Paintbox: TPaintbox; //Paintbox, die zwischen Pinguinen und Fischen liegt und auf die die Markierungen gemacht werden.
    PB_Cursor: TPaintbox; //Liegt genau �ber dem gesamten Spielfeld, um Mausbewegungen und -klicks zu empfangen
    ImgLoaded: Boolean; //Bilder und Paintbox schon geladen?
    FHexVisible: Boolean;
    FQuadVisible: Boolean;
        //Gibt an, ob automatisch visualisiert wird, manuell sind Draw-Prozeduren immer aufrufbar
    FHextransparency: Boolean; //Ecken der Fischbilder transparent?
    FShowPinguinHints: Boolean;
    FclMarkPlayer: TColor;
    FclMarkClient: TColor;
    FDebugSendDraw: Boolean;
    //#### Eingabe ####
    FAusgang,FZiel:TPoint; //Ausgang: ausgew�hltes Feld, Ziel:= Feld unter Cursor
    //#### Laden und Speichern ####
    FLastGameStats: TGameStats;
    FGameSettingsPath: String;
    FTestSettingsPath: String;
    FTestSettings: TTestSettings;
    FAutoSave: Boolean;
    FAutoSavePath: String;
    FAutoContinue: Boolean;
    FStatSave: Boolean;
    FStatPath: String;
    FSavedTurn: TSavedTurnArr;
    FSaveTurnOption: Boolean;
    FGameShow: Boolean;
    FGameShowInterval: Integer;
    //#### Ereignisse ####
    FOnNewTurn: TNewTurnEvent;
    FOnMouseMove: TMouseMoveEvent;
    FOnGameFinish: TGameFinishEvent;
    FOnClientTimeOut: TTimeOutEvent;
    FOnErrorReport: TErrorReportEvent;

    function GetFieldsInRadiusPrivate(x,y: byte; Radius: byte): TPointArray;
        //Alle Felder um ein Feld herum im angegbenen Radius, wobei PointArray die Felder darstellt, die schon gefunden sind.
    procedure OnNewLine(Sender: TObject; NewLine: string; OutputType: TOutputType);
       //Wird aufgerufen, wenn der Client etwas in sein Konsolenfenster geschrieben hat.


    procedure PosePinguin(const x,y: Byte; Player: TBit);
        //Setzt Pinguin f�r Spieler, aufgerufen durch Move
    procedure NextTurn;
    procedure FinishGame(Sender:TObject); //Wird Timer_Finish.OnTime zugewiesen
    procedure OnTimer_ClientDraw(Sender:TObject); //Wird Timer_ClientDraw.OnTime zugewiesen

    //########## EIGENSCHAFTEN-METHODEN ##########
    function GetSettings: TGameSettings;
    function GetStandartSettings: TGameSettings;
    procedure SetSettings(Value: TGameSettings);
    function GetTestSettings: TTestSettings;
    procedure SetStatPath(Value: String);
    procedure SetAutoSavePath(Value: String);
    procedure SetHexVisible(Value: Boolean);
    procedure SetQuadVisible(Value: Boolean);
    procedure SetHexTransparency(Value: Boolean);
    procedure SetShowPinguinHints(Value: Boolean);
    function GetclPlayer: TColor;
    function GetWidth: integer;
    function GetHeight: integer;
    function GetCurrentPlayer: TBit;
    procedure SetCurrentPlayer (const Value: TBit);
    function GetGameFinished: Boolean;

  public
    StringGrid: TStringGrid; //StringGrid, in das das Feld geschrieben wird
    Left,Top: Integer;//Linke obere  Ecke des Hex-Spielfeldes

    DataPath: String; // Pfad, in dem die Daten (Bilder) gespeichert sind
    Player: array[0..1] of TPlayer;

    property Settings: TGameSettings read GetSettings write SetSettings;
    property TestSettings: TTestSettings read GetTestSettings write FTestSettings;
    property StandartSettings: TGameSettings read GetStandartSettings;
    property Timer_ClientDraw: TTimer read FTimer_ClientDraw; //Misst die Dauer des CLientzuges
    property ClientTimeStats: TClientTimeStatsArray read FClientTimeStats write FClientTimeStats;
    property ClientTimeStatsPath: String read FClientTimeStatsPath;
    property ClientTimeOut: Integer read FClientTimeOut write FClientTimeOut;
                        //Die maximale Zeit f�r die Zeit�berschreitung in Centisekunden
    property JavaWarningShown: Boolean read FJavaWarningShown write FJavaWarningShown;
    //### Allgemeines zum Spielverlauf ###
    property Ausgang: TPoint read FAusgang;
    property Ziel: TPoint read FZiel;
    property Turn: Integer read FTurn; //Nummer der aktuellen Spielrunde, 0 hei�t kein Spiel laufend; eine Runde
                  //ist ein Spielzug eines Spielers, also beginnt auch beim Spielerwechsel eine neue Runde
    property CurrentPlayer: TBit read GetCurrentPlayer write SetCurrentplayer; //Aktueller Spieler
    property StartPlayer: TStartPlayer read FStartPlayer write FStartPlayer;
    property Calculating: Boolean read FCalculating write FCalculating; //Wird gerade ein Spiel berechnet?
    property GameFinished: Boolean read GetGameFinished;

    property Field : TField read FField;
    //### Speichern und Laden ###
    property LastGameStats: TGameStats read FLastGameStats; //Die Statistik des letzten Spiel
    property GameSettingsPath: String read FGameSettingsPath write FGameSettingsPath;
    property TestSettingsPath: String read FTestSettingsPath write FTestSettingsPath;
    property AutoSave: Boolean read FAutoSave write FAutoSave; //Spiel automatisch jede Runde speichern?
    property AutoSavePath: String read FAutoSavePath write SetAutoSavePath; //Pfad zum AutoSpeichern
    property AutoContinue: Boolean read FAutoContinue write FAutoContinue; //Soll das Spiel automatisch beim Start fortgesetzt werden?
    property StatSave: Boolean read FStatSave write FStatSave; //Statistik automatisch speichern?
    property StatPath: String read FStatPath write SetStatPath;
    property SavedTurn: TSavedTurnArr read FSavedTurn;
    property SaveTurnOption: Boolean read FSaveTurnOption write FSaveTurnOption;
                  //Sollen die einzelnen Schritte gespeichert werden?
    property GameShow: Boolean read FGameShow; //Wird gerade ein geladenes Spiel angezeigt?
    property GameShowInterval: integer read FGameShowInterval write FGameShowInterval;
          //Das Intervall beim automatischen Durchlauf eines Spiels
    //### Visualisierung ###
    property HexVisible: Boolean read FHexVisible write SetHexVisible;
    property QuadVisible: Boolean read FQuadVisible write SetQuadVisible;
    property Hextransparency: Boolean read FHextransparency write SetHexTransparency;
                          //Ecken der Fischbilder transparent?
    property ShowPinguinHints: Boolean read FShowPinguinHints write SetShowPinguinHints; //Fischanzahl im Tooltip zeigen
    property clPlayer: TColor read GetclPlayer; //Farbe des aktuellen Spielers
    property clMarkPlayer: TColor read FclMarkPlayer write FclMarkPlayer;//Farbe der Pfadmarkierung
    property clMarkClient: TColor read FclMarkClient write FclMarkClient;
    property Width: integer read GetWidth;
    property Height: integer read GetHeight;
    property DebugSendDraw: Boolean read FDebugSendDraw write FDebugSendDraw; //Sollen die Spielz�ge �ber Debug ausgegeben werden?

    //### Ereignisse ###
    property  OnNewTurn: TNewTurnEvent read FOnNewTurn write FOnNewTurn; //Bei neuer Runde
    property  OnMouseMove: TMouseMoveEvent read FOnMouseMove write FOnMouseMove; //Bei Mausbewegung
    property  OnGameFinish: TGameFinishEvent read FOnGameFinish write FOnGameFinish; //Bei Ende eines Spiels
    property  OnClientTimeOut: TTimeOutEvent read FOnClientTimeOut write FOnClientTimeOut;
    property  OnErrorReport: TErrorReportEvent read FOnErrorReport write FOnErrorReport;


    constructor Create(Formular: TForm; StringGrid: TStringGrid);
    procedure Free; //Beendet alle Instanzen von Playern, Pinguinen und schlie�t die Clients.

    procedure New; //Setzt das gesamte Spiel zur�ck und verteilt Schollen neu
    //########## INFOS ZU FELDERN ##########
    procedure renumberPinguins;
    function GetNextFieldInDirection(x,y: byte;Direction: TDirection): TPoint;
                                //Koordinaten des angrenzenden Feldes in angegebener Richtung
    function GetAllTargets(const x,y:byte): TPointArray; //Alle zu erreichenden Felder
    function GetFieldsInRadius(x,y: byte; Radius: byte): TPointArray; //Alle Felder um ein Feld herum im angegbenen Radius
    function GetTargetsInDirection(const x,y: byte; Direction: TDirection): TPointArray;
    function GetAllNeighborFields(x,y:byte): TPointArray; //Array der Nachbarfelder
    function GetAllFish(const x,y: byte): integer; //Liefert die Summe des von (x|y) aus zu erreichenden Fischs
    //########## BEWEGUNG ##########
    function CheckMove(const X1,Y1,X2,Y2: Byte): Boolean;
        //Gibt TRUE zur�ck, wenn Bewegung funktionieren kann, sonst FALSE; X2=Y2=0, dann Setzen von Pinguin gepr�ft
        //Pose gibt an, ob der Pinguin neu gesetzt werden soll oder bewegt.
    procedure Move(const X1,Y1,X2,Y2: Byte);
        //Bewegt den Pinguin auf dem Spielfeld und aktualisiert Visualisierung
    function CheckPath(X1,Y1,X2,Y2: Byte): Boolean;
    function GetPath(X1,Y1,X2,Y2: Byte): TPointArray; //Gibt den Pfad von Punkt1 zu Punkt2
              //(ohne Punkt 1 selbst) in dynamischem Punktarray wieder
    //########## KOMMUNIKATION ##########
    function EncodeField(x,y:byte; Player0: TBit): byte; //Kodiert ein einzelnes Feld (ohne Anweisung f�r Client)
    function EncodeBoard(Player0: TBit): AnsiString;
        //Kodiert das gesamte Spielfeld (ohne Anweisung f�r Client)
        //Player0 ist der Spieler in TGameBoard, der im Code als Player 0 angegben wird -> siehe Funktionsblock
    procedure DecodeBoard(s: AnsiString); //Dekodiert das gesamte Spielfeld aus s in die TGameBoard-Instanz (ohne Anweisung f�r Client)
    //########## VISUALISIERUNG ##########
    procedure LoadImages;
    procedure DrawHex; //Zeichnet Sechsecke von HexAnchor links oben ausgehend
    procedure DrawQuad;//Zeichnet Rechtecke (Feldmatrix) in StringGrid
    procedure OnDrawCells(Sender: TObject; ACol,ARow: Integer; Rect: TRect; State: TGridDrawState); //Zum Einf�rben der Zellen
    procedure DrawAllPinguins; //Zeichnet die Pinguine auf das Hexagonalfeld
    procedure RefreshPaintBox;
    procedure MarkField(const x,y: byte; Color: TColor);
        //Umrahmt ein Sechseck des Feldes in angegebener Farbe
    procedure MarkTPointArray(PointArray: TPointArray; Color: TColor); //Markiert alle Felder im Array
    //########## EINGABE ##########
    function MouseToPos(const x,y:integer): TPoint; //Berechnet aus den Mauskoordinaten
                                  //vom Formular das darunter liegende Feld in Matrixkoordinaten
    procedure PB_CursorMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
    procedure PB_CursorMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PB_CursorMouseUp(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: Integer);
    procedure PB_CursorDblClick(Sender: TObject); //Platziert einen Pinguin zu Beginn
    //########## SPEICHERN & LADEN ##########
    procedure SaveTurn;
    procedure SaveGameToFile(Path: String);
    procedure LoadGameFromFile(Path: String);
    procedure ContinueGame(Turns: TSavedTurnArr);//Setzt ein Spiel in der letzten gespeicherten Runde fort.
    procedure LoadStep(TargetPos: integer); //Zielposition: 1 ist Anfang; wenn TargetPos>Maximal, dann ans Ende
    procedure GetIntoGame; //Aus der Gameshow in das Spiel einsteigen.
    procedure SaveGameSettingsInFile(Settings: TGameSettings; Path: String);
    function LoadGameSettingsFromFile(Path: String): TGameSettings;
    procedure SaveTestSettingsInFile(Settings: TTestSettings; Path: String);
    function LoadTestSettingsFromFile(Path: String): TTestSettings;
    procedure SaveTimeStatsInFile(ClientTimeStats: TClientTimeStatsArray; Path: String);
    function LoadTimeStatsFromFile(Path: String): TClientTimeStatsArray;
  end;

  function CheckClientPath(var Path: String; ShowMessages: Boolean): Boolean;
  function AddTwoTimeStats(const TimeStats0, TimeStats1: TClientTimeStats): TClientTimeStats;


var MouseDown: Boolean; //Ist die Maus gedr�ckt?
    PB_Refresh: byte; //Dazu da, damit bei Mausbewegung weniger oft aktualisiert wird
    RootDir: String; //Pfad des Ordners, in dem die Exedatei ist.
    StringGridColors: Array[0..10,0..7] of TColor; //Farben der StringGridzellen


const PB_RefreshMAX: byte = 4;
      FinishWait: Integer = 150; //Zeit, bis Form_Finish nach dem letzten Zug angezeigt wird.
      //Bilddateien
      PathFish1: ShortString    = 'Fisch1.bmp';
      PathFish2: ShortString    = 'Fisch2.bmp';
      PathFish3: ShortString    = 'Fisch3.bmp';
      PathPingBlue: ShortString = 'PinguinBlue.bmp';
      PathPingRed: ShortString  = 'PinguinRed.bmp';
      //StringGridFarben
      clValidFalse: TColor = clGray;
      clValidTrue: TColor = clWhite ;
      clOnGameBoardFalse: TColor = $00505050; //DunkelGrau


implementation

uses Unit_Server_Finish, Unit_Server_Progress, DebugForm, Unit_Faktoren,
    Unit_Server_Main, Unit_Server_TimeStats, Unit_CalcThd;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


constructor TGameBoard.Create(Formular: TForm; StringGrid: TStringGrid);
var i,j:byte;
begin
  inherited Create; //Die Standart-Create-Prozedur wird eingeschoben

  self.Form:=Formular;
  self.StringGrid:=StringGrid;
  IF StringGrid<>nil THEN StringGrid.OnDrawCell:=self.OnDrawCells;
  IF Form=nil
    THEN FHexVisible:=FALSE;
  IF StringGrid=nil
    THEN FQuadVisible:=FALSE;

  //Timer
  Timer_Finish:=TTimer.Create(Form);
  with Timer_Finish do
    begin
      OnTimer:=self.FinishGame;
      Enabled:=FALSE;
      Interval:=FinishWait;
    end;
  FTimer_ClientDraw:=TTimer.Create(Form); //Dieser Timer dient eigentlich nur noch zur Anzeige der aktuellen Zeit und zum �berpr�fen auf TimeOut
  with FTimer_ClientDraw do
    begin
      OnTimer:=self.OnTimer_ClientDraw;
      Tag:=0;
      Enabled:=FALSE;
      Interval:=1;
    end;
  FMyTimer:= TMyTimer.Create;
  FMyTimer.ResetTime;

  //Player
  Player[0]:=TPlayer.Create(self);
  Player[1]:=TPlayer.Create(self);
  Player[0].DosCommand:=TDosCommand.Create(Form);
  Player[1].DosCommand:=TDosCommand.Create(Form);
  Player[0].DosCommand.OnNewLine:=self.OnNewLine;
  Player[1].DosCommand.OnNewLine:=self.OnNewLine;
  Player[0].FNumber:=0;
  Player[1].FNumber:=1;
  Player[0].FColor:=clRed;
  Player[1].FColor:=clBlue;

  //Position des Spielfeldes auf der Form
  IF Form<>nil
    THEN begin
      Left:=10;
      Top:=10;
    end;

  FAusgang.X:=0; FAusgang.Y:=0;
  FZiel.X:=0; FZiel.Y:=0;

  //valid und OnGameBoard festlegen, zeilenweise (mehr Schritte, aber bessere �bersicht)
  FOR i:=0 to 10 do //Spalten
    FOR j:=0 to 7 do //Zeilen
      begin
        IF   ((j=0) AND (i>=3)           )
          OR ((j=1) AND (i>=3) AND (i<=9))
          OR ((j=2) AND (i>=2) AND (i<=9))
          OR ((j=3) AND (i>=2) AND (i<=8))
          OR ((j=4) AND (i>=1) AND (i<=8))
          OR ((j=5) AND (i>=1) AND (i<=7))
          OR ((j=6)            AND (i<=7))
          OR ((j=7)            AND (i<=6))
            THEN begin
              FField[i,j].valid:=TRUE;
              FField[i,j].OnGameBoard:=TRUE;
            end
            ELSE begin
              FField[i,j].valid:=FALSE;
              FField[i,j].OnGameBoard:=FALSE;
            end;
      end;

  //Grundlegende Dateipfade festlegen    
  RootDir:=Application.exename;
  Delete(RootDir,length(RootDir)-15,16);
  DataPath:=RootDir+'Data\';
  IF Form<>nil THEN LoadImages;

IF Form<>nil
  THEN begin
    //Sicherungen
    FGameSettingsPath:=DataPath+'GameSettings';
    IF FileExists(GameSettingsPath)=TRUE
      THEN Settings:=LoadGameSettingsFromFile(GameSettingsPath)
      ELSE self.Settings:=StandartSettings;

    FTestSettingsPath:=DataPath+'TestSettings';
    IF FileExists(TestSettingsPath)=TRUE
      THEN TestSettings:=LoadTestSettingsFromFile(TestSettingsPath);

    FClientTimeStatsPath:=DataPath+'ClientTimeStats';
    IF FileExists(ClientTimeStatsPath)=TRUE
      THEN FClientTimeStats:=LoadTimeStatsFromFile(ClientTimeStatsPath);
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.Free;
var i,j: byte;
begin
  FOR i:=0 to 1 do
    begin
      FOR j:=0 to 3 do
        begin
          Img_Pinguin[i,j].Free;
          Player[i].Pinguin[j].Free;
        end;
      Player[i].SendToClient(2);
      Player[i].DosCommand.Free;
      Player[i].Free;
    end;

  Timer_Finish.Free; Timer_ClientDraw.Free;

  Bmp_Fish1.Free; Bmp_Fish2.Free; Bmp_Fish3.Free; Bmp_PinguinRed.Free; Bmp_PinguinBlue.Free;
  Paintbox.Free;
  PB_Cursor.Free;
  FOR i:=1 to 3 do
    FOR j:=1 to 30 do
      begin
      Img_Fish[i,j].Free;
      end;

  inherited Free;
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------



//#######################################
//#############  ALLGEMEIN  #############
//#######################################


function TGameBoard.GetStandartSettings: TGameSettings;
begin
  with result do
    begin
      HexVisible:=TRUE;
      HexTransparency:=TRUE;
      ShowPinguinHints:=TRUE;

      ShowDebugForm:=FALSE;
      DebugFormPos.X:=Screen.Width-Form_Debug.Width;
      DebugFormPos.Y:=0;
      DebugFormSize.X:=441;
      DebugFormSize.Y:=167;
      DebugAddTime:=TRUE;
      DebugPlayerName:=FALSE;
      DebugSendDraw:=FALSE;

      ShowTimeForm:=FALSE;
      TimeFormPos.X:=0;
      TimeFormPos.Y:=0;
      ShowCounter:=TRUE;

      clMarkPlayer:=clBlack;
      clMarkClient:=clPurple;
      QuadVisible:=FALSE;
      SaveTurnOption:=TRUE;
      AutoSave:=TRUE;
      AutoSavePath:= RootDir+'Saves\AutoSave.pap';
      AutoContinue:=FALSE;
      StatSave:=FALSE;
      StatPath:=RootDir+'Saves\AutoStatistik.csv';
      GameShowInterval:=1000;
      StartPlayer:=2;
      ClientTimeOut:=1000; //10 Sekunden
      JavaWarningShown:=FALSE;
      

      //Player 0
      Name0:='Spieler 0';
      CPUControlled0:=FALSE;
      AutoMove0:=FALSE;
      ClientWait0:=TRUE;
      WaitSleep0:=800;
      JavaClient0:=FALSE;

      //Player 1
      Name1:='Spieler 1';
      CPUControlled1:=FALSE;
      AutoMove1:=FALSE;
      ClientWait1:=TRUE;        
      WaitSleep1:=800;
      JavaClient1:=FALSE;
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.New;
var
  i,j, FishBuffer: byte;
  FishCD: array[1..3] of byte; //=FishCountDown, wie viele 1er, 2er, 3er noch zu verteilen sind
  RepeatAbort: Boolean; //Bricht Repeatschleife ab, keine weitere Funktion
begin
  FishCD[1]:=30;
  FishCD[2]:=20;
  FishCD[3]:=10;
  FOR i:=0 to 10 do //Spalten
    FOR j:=0 to 7 do //Zeilen
      begin //valid und OnGameBoard festlegen, zeilenweise (mehr Schritte, aber bessere �bersicht)
        IF   ((j=0) AND (i>=3)           )  
          OR ((j=1) AND (i>=3) AND (i<=9))
          OR ((j=2) AND (i>=2) AND (i<=9))
          OR ((j=3) AND (i>=2) AND (i<=8))
          OR ((j=4) AND (i>=1) AND (i<=8))
          OR ((j=5) AND (i>=1) AND (i<=7))
          OR ((j=6)            AND (i<=7))
          OR ((j=7)            AND (i<=6))
            THEN begin                  
              FField[i,j].valid:=TRUE;
              FField[i,j].OnGameBoard:=TRUE;
            end
            ELSE begin
              self.FField[i,j].valid:=FALSE;
              self.FField[i,j].OnGameBoard:=FALSE;
            end;
        //Fische werden verteilt
        RepeatAbort:=FALSE;
        IF FField[i,j].valid=TRUE
          THEN begin
            //Pinguine entfernen
            FField[i,j].Full:=FALSE;
            //Fische verteilen
            REPEAT
              FishBuffer:=random(60)+1;
              CASE FishBuffer of //Die Wahrscheinlichkeiten werden verteilt
                1..30:  FishBuffer:=1;
                31..50: FishBuffer:=2;
                51..60: FishBuffer:=3;
              end;
              IF FishCD[FishBuffer]>0 //Wenn noch Fische aus der Kategorie da sind
                THEN begin
                  FField[i,j].fish:=FishBuffer;
                  IF Form<>nil
                    THEN begin
                      //Position des Images festlegen
                      with Img_Fish[FishBuffer,FishCD[FishBuffer]] do
                        begin
                          Position.X:=i;
                          Position.Y:=j;
                          Left:=ImgPos[i,j].X;
                          Top :=ImgPos[i,j].Y;
                        end;
                    end;

                  dec(FishCD[FishBuffer]);
                  RepeatAbort:=TRUE;
                end;
            UNTIL RepeatAbort=TRUE;
          end;
      end;
  //Spielstand zur�cksetzen
  FTurn:=1;
  SetLength(FSavedTurn,1);
  //Startspieler
  CASE StartPlayer of
    0: CurrentPlayer:=0;
    1: CurrentPlayer:=1;
    2: Currentplayer:=Random(2); //Startspieler zuf�llig ermitteln
  end;
  FOR i:=0 to 1 do
    begin
      Player[i].FScore:=0;
      Player[i].FFieldsCollected:=0;
      //Pinguine
      FOR j:=0 to 3 do
        begin
          Player[i].Pinguin[j].FPosed:=FALSE;
          Player[i].FPinguin[j].FPosition.X:=0;
          Player[i].FPinguin[j].FPosition.Y:=0;
        end;
    end;
  //Clients reinitialisieren
  IF Calculating=FALSE
    THEN begin
      IF Player[0].CPUControlled=TRUE
        THEN Player[0].SendToClient(3);
      IF Player[1].CPUControlled=TRUE
        THEN Player[1].SendToClient(3);
    end
    ELSE begin
      FOR i:=0 to 1 do
        CASE FTestSettings.AfterEachGame of
          0:
            begin
              Player[i].SendToClient(2);
              Player[i].StartClient;
            end;
          1: Player[i].SendToClient(3);
        end;
    end;
  //Client-Zeitstatistik zur�cksetzen
  FOR i:=0 to 1 do
    with Player[i].FTimeStats do
      begin
        FirstAverage:=0;
        FirstMaximum:=0;
        FirstMinimum:=0;
        Average:=0;
        Maximum:=0;
        Minimum:=0;
        SumAverage:=0;
        SumMaximum:=0;
        SumMinimum:=0;
        PartSum:=0;
      end;
  //Optionale Visualisierung
  IF HexVisible=TRUE
    THEN begin
      DrawHex;
      FAusgang.X:=0; FAusgang.Y:=0;
    end;
  IF QuadVisible=TRUE THEN DrawQuad;
  //Falls vorher ein Spiel angezeigt wurde, werden nun die originalen Spielernamen wieder geladen.
  IF GameShow=TRUE
    THEN begin
      Player[0].Name:=Player[0].SaveName;
      Player[1].Name:=Player[1].SaveName;
    end;
  FGameShow:=FALSE;
  //Ereignisse
  IF Assigned(FOnNewTurn) THEN FOnNewTurn;
  // Erste Runde Speichern
  IF SaveTurnOption=TRUE THEN SaveTurn;     
  //Debug
  IF (Form_Debug.Memo<>nil) AND (Calculating=FALSE) THEN Form_Debug.Memo.Send('########## Neues Spiel ##########',false);
end;

//------------------------------------------------------------------------------

procedure TGameBoard.NextTurn;
var i: integer;
    GameFinished: Boolean;
begin
  FOR i:=0 to 1 do Player[i].FClientIsDrawing:=FALSE;
  GameFinished:=FALSE;
  FTurn:=FTurn+1;
  IF Currentplayer=1 THEN Currentplayer:=0 ELSE Currentplayer:=1;
  IF Turn>8 //Die �berpr�fung f�rs Spielende wird nur gemacht, wenn Pinguine schon gesetzt sind.
    THEN begin
      IF Player[CurrentPlayer].movable=FALSE //Wenn neuer Spieler unbeweglich ist, wird wieder auf
        THEN begin
          IF Currentplayer=1 THEN Currentplayer:=0 ELSE Currentplayer:=1;//vorherigen zur�ckgesetzt
          //Falls auch dieser unbeweglich ist, wird Spiel beendet
          IF Player[CurrentPlayer].movable=FALSE
            THEN begin
              IF Calculating=TRUE
                THEN begin
                  FinishGame(self);
                end
                ELSE Timer_Finish.Enabled:=TRUE;
              GameFinished:=TRUE;
            end;
        end;
    end;
  //Speichern
  IF SaveTurnOption=TRUE THEN SaveTurn;
  IF (AutoSave=TRUE) AND (SaveTurnOption=TRUE) THEN SaveGameToFile(AutoSavePath);
  //Visualisierung
  IF HexVisible=TRUE
    THEN begin
      FAusgang.X:=0;
      FAusgang.Y:=0;
      DrawHex;
      RefreshPaintbox;
    end;
  IF QuadVisible=TRUE THEN DrawQuad;
  //Ereignisse
  IF Assigned(FOnNewTurn) THEN FOnNewTurn;

  //Anweisung zu Clients schicken
  IF GameFinished=FALSE
    THEN begin
      FOR i:=0 to 1 do
        begin
          IF (Player[i].CPUControlled=TRUE) AND (Player[i].AutoMove=TRUE)
            THEN begin
              IF i=Currentplayer
                THEN Player[i].SendToClient(1)
                ELSE Player[i].SendToClient(0);
            end;
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.FinishGame(Sender: TObject);
var i,j,t: byte;
    NewTimeStat: array[0..1] of Boolean; //Eintrag f�r Spieler0/1 noch nicht vorhanden?
begin
  FOR i:=0 to 1 do
    FOR j:=0 to 3 do
      begin
        Player[i].FScore:=Player[i].Score+Player[i].Pinguin[j].Fish;
        inc(Player[i].FFieldsCollected);
      end;

  //######## SpielStatistik ########
  FOR i:=0 to 1 do
    begin
      With FLastGameStats.Player[i] do
        begin
          Name:=Player[i].Name;
          Score:=Player[i].Score;
          FieldsCollected:=Player[i].FieldsCollected;
          CPUControlled:=Player[i].CPUControlled;
          IF CPUControlled=TRUE
            THEN begin
              ClientPath:=Player[i].ClientPath;
            end;
          end;
    end;
  FLastGameStats.Turns:=Turn;

  FLastGameStats.Winner:=2;
  //Fische vergleichen
  IF FLastGameStats.Player[0].Score > FLastGameStats.Player[1].Score
    THEN FLastGameStats.Winner:=0;
  IF FLastGameStats.Player[1].Score > FLastGameStats.Player[0].Score
    THEN FLastGameStats.Winner:=1;
  //Schollen vergleichen
  IF FLastGameStats.Player[0].Score = FLastGameStats.Player[1].Score
    THEN begin
      IF FLastGameStats.Player[0].FieldsCollected > FLastGameStats.Player[1].FieldsCollected
        THEN FLastGameStats.Winner:=0;
      IF FLastGameStats.Player[1].FieldsCollected > FLastGameStats.Player[0].FieldsCollected
        THEN FLastGameStats.Winner:=1;
    end;
  //Vorsprung  
  IF FLastGameStats.Winner=0
    THEN FLastGameStats.Vorsprung := FLastGameStats.Player[FLastGameStats.Winner].Score-FLastGameStats.Player[1].Score;
  IF FLastGameStats.Winner=1
    THEN FLastGameStats.Vorsprung := FLastGameStats.Player[FLastGameStats.Winner].Score-FLastGameStats.Player[0].Score;
  IF FLastGameStats.Winner=2
    THEN FLastGameStats.Vorsprung := 0;



  //########  Zeitstatistik #########
  //Gesamtes Spiel
  FOR j:=0 to 1 do
    begin
      with Player[j].FTimeStats do
        begin
          SumMaximum:=PartSum;
          SumMinimum:=PartSum;
          SumAverage:=PartSum;
          PartSum:=PartSum;
          NumberOfGames:=1;
        end;
    end;
  //neuer Eintrag in allgemeine Statistik
  NewTimeStat[0]:=TRUE;
  NewTimeStat[1]:=TRUE;
  IF ClientTimeStats.length>=1
    THEN begin
      FOR i:=0 to ClientTimeStats.length-1 do
        begin
          FOR j:=0 to 1 do
            begin
              //Eintrag schon vorhanden
              IF Player[j].TimeStats.Path=ClientTimeStats.Arr[i].Path
                THEN begin
                  NewTimeStat[j]:=FALSE;
                  FClientTimeStats.Arr[i]:=AddTwoTimeStats(ClientTimeStats.Arr[i],Player[j].TimeStats);
                end;
            end;
        end;
    end;
  //beide neue Eintr�ge gleich
  IF (NewTimeStat[0]=TRUE) AND (NewTimeStat[1]=TRUE) AND (Player[0].TimeStats.Path=Player[1].TimeStats.Path)
    THEN begin
      inc(FClientTimeStats.length);
      FClientTimeStats.Arr[ClientTimeStats.length-1]:=AddTwoTimeStats(Player[0].TimeStats,Player[1].TimeStats);
    end
    //zwei neue Eintr�ge erstellen
    ELSE begin
      FOR j:=0 to 1 do
        begin
          IF NewTimeStat[j]=TRUE
            THEN begin
              inc(FClientTimeStats.length);
              FClientTimeStats.Arr[ClientTimeStats.length-1]:=Player[j].TimeStats;
            end;
        end;
    end;

  //####### Ausgabe #######
  IF Calculating=FALSE
    THEN begin
      //Timer
      Timer_Finish.Enabled:=FALSE;
      //Visualisierung
      IF HexVisible=TRUE
      THEN begin
        FAusgang.X:=0;
        FAusgang.Y:=0;
        DrawHex;
      end;
      IF QuadVisible=TRUE THEN DrawQuad;

      //Ereignisse
//      IF Assigned(OnGameFinish) THEN OnGameFinish(LastGameStats,0,0,0,0);
      IF Assigned(FOnNewTurn) THEN FOnNewTurn;
      //Fenster anzeigen
      Form.Enabled:=FALSE;
      Form_Finish.GameStats:=self.LastGameStats;
      Form_Finish.Show;
    end
    ELSE begin
      //Finish-Ereignis
      IF Assigned(OnGameFinish) THEN OnGameFinish(LastGameStats, TestSettings.Index0,TestSettings.Index1, TestSettings.Counter, Player[0].TimeStats.partSum + Player[1].TimeStats.partSum);


      FTestSettings:=GetNextCalcClients(TestSettings);
//      IF TestSettings.Counter = TestSettings.AmountOfGames-1
//        THEN begin
//          //Indexwerte erh�hen
//          IF TestSettings.Index1 < TestSettings.High1 //F�r Spieler 1 einen weitergehen
//            THEN inc(FTestSettings.Index1)
//            ELSE begin //F�r Spieler 0 weitergehen Spieler 1 neu anfangen
//              IF (TestSettings.Index0 <= TestSettings.High0)
//                THEN begin
//                  inc(FTestSettings.Index0);
//                  FTestSettings.Index1:=0;
//                end;
//            end;
//          FTestSettings.Counter:=0;
//        end
//        ELSE inc(FTestSettings.Counter);
      IF (TestSettings.Index0 < TestSettings.High0+1) and (TestSettings.Index1 <= TestSettings.High1)
        THEN begin
          Player[0].CPUControlled:=TRUE;
          Player[1].CPUControlled:=TRUE;
          Player[0].JavaClient:=TestSettings.Clients0[TestSettings.Index0].Java;
          Player[1].JavaClient:=TestSettings.Clients0[TestSettings.Index0].Java;
          Player[0].Name         :=TestSettings.Clients0[TestSettings.Index0].Name;
          Player[1].Name         :=TestSettings.Clients1[TestSettings.Index1].Name;
          Player[0].ClientPath   :=TestSettings.Clients0[TestSettings.Index0].Path;
          Player[1].ClientPath   :=TestSettings.Clients1[TestSettings.Index1].Path;
          //Neues Spiel
          New;
          Player[CurrentPlayer].SendToClient(1);
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.OnTimer_ClientDraw(Sender:TObject);
begin
  Timer_ClientDraw.Tag:=round(FMyTimer.CheckTime/10); //Aus Tag liest man sp�ter die gestoppte Zeit
  IF Calculating=FALSE
    THEN begin
      IF (Form_TimeStats.Visible=TRUE) AND (Form_TimeStats.CB_Counter.Checked=TRUE)
        THEN begin
          Form_TimeStats.La_Counter.Caption:=IntToStr(round(FMyTimer.CheckTime/10));
          IF round(FMyTimer.CheckTime/10) > 500
            THEN Form_TimeStats.La_Counter.Font.Color:=clRed
            ELSE Form_TimeStats.La_Counter.Font.Color:=clBlack;
        end;
    end
    ELSE begin
      IF (Form_Progress.Visible=TRUE) AND (Form_Progress.CB_Counter.Checked=TRUE)
        THEN begin
          Form_Progress.La_Counter.Caption:=IntToStr(round(FMyTimer.CheckTime/10));
          IF round(FMyTimer.CheckTime/10) > 500
            THEN Form_Progress.La_Counter.Font.Color:=clRed
            ELSE Form_Progress.La_Counter.Font.Color:=clBlack;
        end;
    end;
  //TimeOut
  IF (round(FMyTimer.CheckTime/10) >= ClientTimeOut) AND (round(FMyTimer.CheckTime/10) > 10)
    THEN begin
      Timer_ClientDraw.Enabled:=FALSE;
      IF assigned(OnClientTimeOut) THEN OnClientTimeOut(self,self.CurrentPlayer,round(FMyTimer.CheckTime/10));
      IF Calculating=TRUE
        THEN begin
          //Spiel und Client neustarten
          IF (self.Player[CurrentPlayer].DosCommand<>nil) and (self.Player[CurrentPlayer].DosCommand.IsRunning=TRUE)
            THEN self.Player[CurrentPlayer].SendToClient(2);
          self.Player[CurrentPlayer].StartClient;
          self.New;
          self.Player[CurrentPlayer].SendToClient(1);
        end;
    end;
end;



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------







//###########################################
//#############  VISUALISIERUNG  ############
//###########################################


procedure TGameBoard.DrawQuad;
var x,y:byte;
begin
  With StringGrid do begin
    //Layout anpassen
    ColCount:=12;
    RowCount:=9;
    Height:=RowCount*(DefaultRowHeight+GridLineWidth+1)+10;
    Width:=ColCount*(DefaultColWidth+GridLineWidth+1)+7;
    //Werte eintragen
    FOR x:=0 to 11 do
      FOR y:=0 to 8 do
        begin
          IF (y=0) AND (x<>0) THEN Cells[x,y]:=IntToStr(x-1); //In die Zeilen�berschrift Spaltennummer-1 schreiben
          IF (x=0) AND (y<>0) THEN Cells[x,y]:=IntToStr(y-1); //In die Spalten�berschrift Zeilennummer-1 schreiben
          //Wenn mit Feldarrays gearbeitet wird, muss immer -1 gerechnet werden, da die �berschriften
          //die nullte Stelle des Grids besetzen.
          IF (x>0) AND (y>0)
            THEN begin
              IF Field[x-1,y-1].valid=FALSE //Wenn Feld nicht existiert (nicht auf Spielfeld oder Scholle weg)
                THEN begin
                  IF Field[x-1,y-1].OnGameBoard=FALSE
                    THEN begin //Wenn nicht auf Spielfeld
                      Cells[x,y]:='  ';
                      StringGridColors[x-1,y-1]:=clOnGameBoardFalse; //Dunkelgrau
                    end
                    ELSE begin //Wenn Scholle versunken
                      Cells[x,y]:='  ~';
                      StringGridColors[x-1,y-1]:=clValidFalse;
                    end;
                end
                ELSE begin
                  IF FField[x-1,y-1].full=FALSE //wenn kein Pinguin auf dem Feld ist
                    THEN begin
                      Cells[x,y]:='  '+IntToStr(FField[x-1,y-1].Fish); //Dann Fische als Zahl anzeigen
                      StringGridColors[x-1,y-1]:=clValidTrue;
                    end
                    ELSE begin                                              
                      //Sonst Spieler x als Px anzeigen
                      Cells[x,y]:='P'+IntToStr(FField[x-1,y-1].Player) + ','+IntToStr(FField[x-1,y-1].PinguinNummer);
                      IF Field[x-1,y-1].Player=0
                        THEN StringGridColors[x-1,y-1]:=clRed
                        ELSE StringGridColors[x-1,y-1]:=clBlue;
                    end;
                end;
            end;
        end;
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.OnDrawCells(Sender: TObject; ACol,ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  IF (ARow=0) OR (ACol=0) THEN exit;
  With StringGrid do
    begin
      Canvas.Brush.Color:=StringGridColors[ACol-1,ARow-1];
      Canvas.FillRect(Rect);
      IF (Canvas.Brush.Color=clRed) OR (Canvas.Brush.Color=clBlue)
        THEN Canvas.Font.Color:=clWhite
        ELSE Canvas.Font.Color:=clBlack;
      Canvas.TextOut(Rect.Left+2, Rect.Top+2, Cells[ACol, ARow]);
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.LoadImages;
var i,j,VertEinschub: byte; //VertEinschub ist Vertikaler Einschub: H�he der Spitze eines Secksecks
begin
      //Bitmaps erstellen
      Bmp_Fish1:=TBitmap.Create;
      Bmp_Fish2:=TBitmap.Create;
      Bmp_Fish3:=TBitmap.Create;
      Bmp_PinguinBlue:=TBitmap.Create;
      Bmp_PinguinRed:=TBitmap.Create;
      //Bitmaps laden
      try Bmp_Fish1.LoadfromFile(DataPath+PathFish1);
        except on EFOpenError do
          Messagedlg('"'+DataPath+PathFish1+'" konnte nicht geladen werden. �berpr�fen Sie die Existenz der Datei!',mtError,[mbOK],0);
      end;
      try Bmp_Fish2.LoadfromFile(DataPath+PathFish2);
        except on EFOpenError do
          Messagedlg('"'+DataPath+PathFish2+'" konnte nicht geladen werden. �berpr�fen Sie die Existenz der Datei!',mtError,[mbOK],0);
      end;
      try Bmp_Fish3.LoadfromFile(DataPath+PathFish3);
        except on EFOpenError do
          Messagedlg('"'+DataPath+PathFish3+'" konnte nicht geladen werden. �berpr�fen Sie die Existenz der Datei!',mtError,[mbOK],0);
      end;
      try Bmp_PinguinBlue.LoadfromFile(DataPath+PathPingBlue);
        except on EFOpenError do
          Messagedlg('"'+DataPath+PathPingBlue+'" konnte nicht geladen werden. �berpr�fen Sie die Existenz der Datei!',mtError,[mbOK],0);
      end;
      try Bmp_PinguinRed.LoadfromFile(DataPath+PathPingRed);
        except on EFOpenError do
          Messagedlg('"'+DataPath+PathPingRed+'" konnte nicht geladen werden. �berpr�fen Sie die Existenz der Datei!',mtError,[mbOK],0);
      end;

      //Fische
      FOR i:=1 to 30 do
        begin
          Img_Fish[1,i]:=TPapImage.Create(Form);
          with Img_Fish[1,i] do begin
            Picture.Bitmap:=Bmp_Fish1;
            Parent:=Form;
            AutoSize:=TRUE;
            Transparent:=Hextransparency;
            Visible:=FALSE;
          end;
        end;
      FOR i:=1 to 20 do
        begin
          Img_Fish[2,i]:=TPapImage.Create(Form);
          with Img_Fish[2,i] do begin
            Picture.Bitmap:=Bmp_Fish2;
            Parent:=Form;
            AutoSize:=TRUE;
            Transparent:=Hextransparency;
            Visible:=FALSE;
          end;
        end;
      FOR i:=1 to 10 do
      begin
          Img_Fish[3,i]:=TPapImage.Create(Form);
          with Img_Fish[3,i] do begin
            Picture.Bitmap:=Bmp_Fish3;
            Parent:=Form;
            AutoSize:=TRUE;
            Transparent:=Hextransparency;
            Visible:=FALSE;
          end;
        end;

      //Pinguine
      FOR i:=0 to 1 do
        FOR j:=0 to 3 do
          begin
          Img_Pinguin[i,j]:=TPapImage.Create(Form);
          Img_Pinguin[i,j].Parent:=Form;
          Img_Pinguin[i,j].AutoSize:=TRUE;
          Img_Pinguin[i,j].Transparent:=TRUE;
          Img_Pinguin[i,j].Visible:=FALSE;
          IF i=0
            THEN Img_Pinguin[i,j].Picture.Bitmap:=Bmp_PinguinRed
            ELSE Img_Pinguin[i,j].Picture.Bitmap:=Bmp_PinguinBlue;
          end;

      //ImgPos setzen, also die Positionen f�r die Images setzen
      VertEinschub:=Bmp_Fish1.Height div 4;
      FOR i:=0 to 10 do
        FOR j:=0 to 7 do
          begin
            ImgPos[i,j].X:=Left + i*Bmp_Fish1.Width;
            ImgPos[i,j].Y:=Top + j*Bmp_Fish1.Height;
            CASE j of     //Richtig horizontal einschieben
              0: ImgPos[i,j].X:=ImgPos[i,j].X - 3*Bmp_Fish1.Width;
              1: ImgPos[i,j].X:=ImgPos[i,j].X - 5*(Bmp_Fish1.Width div 2);
              2: ImgPos[i,j].X:=ImgPos[i,j].X - 2*Bmp_Fish1.Width;
              3: ImgPos[i,j].X:=ImgPos[i,j].X - 3*(Bmp_Fish1.Width div 2);
              4: ImgPos[i,j].X:=ImgPos[i,j].X - Bmp_Fish1.Width;
              5: ImgPos[i,j].X:=ImgPos[i,j].X - (Bmp_Fish1.Width div 2); //einen halben nach links
              //6: Hier muss nichts ver�ndert werden
              7: ImgPos[i,j].X:=ImgPos[i,j].X + (Bmp_Fish1.Width div 2); //einen halben nach rechts
            end;
            //Vertikaler Einschub
            ImgPos[i,j].Y:=ImgPos[i,j].Y - j*VertEinSchub;
          end;

      //Paintbox
      Paintbox:=TPaintbox.Create(Form);
      with Paintbox do
        begin
          Parent:=Form;
          Left:=self.Left;
          Top:=self.Top;
          Width:=ImgPos[10,7].X+Bmp_Fish1.Width - Paintbox.Left;
          Height:=ImgPos[10,7].Y+Bmp_Fish1.Height - Paintbox.Top;
        end;

      //PB_MouseMove, steht am Ende, damit es oben auf ist -> funktioniert nicht ganz richtig
      PB_Cursor:=TPaintbox.Create(Form);
      with PB_Cursor do begin
        Parent:=Form;
        Left:=self.Left;
        //vergr��ert, damit MouseToPos auf -1|-1 gesetzt werden kann
        Top:=self.Top -5;
        Width:=ImgPos[10,7].X+Bmp_Fish1.Width - Paintbox.Left;
        Height:=ImgPos[10,7].Y+Bmp_Fish1.Height - Paintbox.Top+10;
        OnMouseMove:= PB_CursorMouseMove;
        OnMouseDown:= PB_CursorMouseDown;
        OnMouseUp:=   PB_CursorMouseUp;
        OnDblClick:=  PB_CursorDblClick;
        BringToFront; //sicherstellen, dass es ganz oben ist
      end;
    ImgLoaded:=TRUE;
end;

procedure TGameBoard.DrawHex;
var
  FishCounter: array[1..3] of byte; //wie viele 1er, 2er, 3er Images schon verteilt sind
  i: byte;
begin
  //******** Images laden, EINMALIG''''''''''''''''''
  IF ImgLoaded=FALSE THEN LoadImages;
  //''''''''' Images anzeigen '''''''''''''
  FOR i:=1 to 30 do
    begin
      Img_Fish[1,i].Visible:=Field[Img_Fish[1,i].Position.X, Img_Fish[1,i].Position.Y].valid;
    end;
  FOR i:=1 to 20 do
    begin
      Img_Fish[2,i].Visible:=Field[Img_Fish[2,i].Position.X, Img_Fish[2,i].Position.Y].valid;
    end;
  FOR i:=1 to 10 do
    begin
      Img_Fish[3,i].Visible:=Field[Img_Fish[3,i].Position.X, Img_Fish[3,i].Position.Y].valid;
    end;   
  DrawAllPinguins;
  PB_Cursor.BringToFront; //sicherstellen, dass es ganz oben ist
end;

//------------------------------------------------------------------------------

procedure TGameBoard.RefreshPaintBox;
begin
  PaintBox.Refresh;
end;
//------------------------------------------------------------------------------

procedure TGameBoard.DrawAllPinguins;
var i,j: byte;
  Punkt:  TPoint; //�bersichtlichkeit
begin
  FOR i:=0 to 1 do
    FOR j:=0 to 3 do
      begin
        IF Player[i].Pinguin[j].Posed=TRUE
          THEN begin
            with Img_Pinguin[i,j] do
              begin
                Punkt:=Player[i].Pinguin[j].Position;
                Left:=ImgPos[Punkt.X,Punkt.Y].X;
                Top :=ImgPos[Punkt.X,Punkt.Y].Y;
                //Hints
                Hint:='Fisch: '+IntToStr(Player[i].Pinguin[j].Fish);
              end;
          end;
        Img_Pinguin[i,j].Visible:=Player[i].Pinguin[j].Posed;
      end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.MarkField(const x,y: byte; Color: TColor);
    //Umrahmt ein Sechseck des Spielfelds (an Position x,y in der Matrix) in angegebener Farbe
var Punkt: array[0..6] of TPoint; //Alle Eckpunkte des Sechsecks in der Paintbox, im Uhrzeigersinn von oben
    i: byte;
begin
  Punkt[0].X:=ImgPos[x,y].X-Paintbox.Left + Bmp_Fish1.Width div 2;
  Punkt[0].Y:=ImgPos[x,y].Y-Paintbox.Top;

  Punkt[1].X:=ImgPos[x,y].X-Paintbox.Left + Bmp_Fish1.Width;
  Punkt[1].Y:=Punkt[0].Y + Bmp_Fish1.Height div 4;

  Punkt[2].X:=Punkt[1].X;
  Punkt[2].Y:=Punkt[1].Y + Bmp_Fish1.Height div 2;

  Punkt[3].X:=Punkt[0].X;
  Punkt[3].Y:=ImgPos[x,y].Y-Paintbox.Top + Bmp_Fish1.Height;

  Punkt[4].X:=ImgPos[x,y].X-Left;
  Punkt[4].Y:=Punkt[2].Y;

  Punkt[5].X:=Punkt[4].X;
  Punkt[5].Y:=Punkt[1].Y;

  Punkt[6]:=Punkt[0];

  with Paintbox.Canvas do
    begin
      Pen.Color:=Color;
      Pen.Width:=3;
      Polyline(Punkt); //Verbindet alle Punkte des Sechsecks im Kreis
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.MarkTPointArray(PointArray: TPointArray; Color: TColor);
var i: integer;
begin
  FOR i:=0 to length(PointArray)-1 do
    begin
      MarkField(PointArray[i].X,PointArray[i].Y,Color);
    end;
end;



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------





//######################################
//#############  BEWEGUNG  #############
//######################################


function TGameBoard.CheckMove(const X1,Y1,X2,Y2: Byte): Boolean;
begin
  //Pinguin bewegen
  IF    (turn>8)
    AND (Field[X1,Y1].full=TRUE)
    AND (Field[X1,Y1].Player=Currentplayer)
    AND (CheckPath(X1,Y1,X2,Y2)=TRUE)
    AND (GameShow=FALSE)
      THEN result:=TRUE
      ELSE result:=FALSE;
  //Pinguin setzen
  IF (X2=0) AND (Y2=0) AND (turn<=8) AND (GameShow=FALSE)
    THEN begin
      IF    (FField[X1,Y1].full=FALSE)
        AND (FField[X1,Y1].valid=TRUE)
        AND (FField[X1,Y1].Fish=1)
          THEN result:=TRUE
          ELSE result:=FALSE;
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.Move(const X1,Y1,X2,Y2: Byte);
begin
  IF    (CheckMove(X1,Y1,X2,Y2)=TRUE)
    AND (turn>8)
    THEN begin
      //Pinguin bewegen
      FField[X2,Y2].Full:=TRUE;
      FField[X2,Y2].Player:=Field[X1,Y1].Player;
      FField[X2,Y2].PinguinNummer:=FField[X1,Y1].PinguinNummer;
      Player[CurrentPlayer].FPinguin[Field[X2,Y2].PinguinNummer].FPosition.X:=X2;
      Player[CurrentPlayer].FPinguin[Field[X2,Y2].PinguinNummer].FPosition.Y:=Y2;
      FField[X1,Y1].Full:=FALSE;
      //Fische verteilen
      Player[FField[X2,Y2].Player].FScore:=Player[FField[X2,Y2].Player].Score + Field[X1,Y1].Fish;
      inc(Player[FField[X2,Y2].Player].FFieldsCollected);
      //Scholle entfernen
      FField[X1,Y1].valid:=FALSE;
      //N�chste Runde
      NextTurn;
    end;
  //Falls der Pinguin neu gesetzt werden soll
  IF (X2=0) AND (Y2=0) AND (turn<=8)
    THEN PosePinguin(X1,Y1,Currentplayer);
end;

//------------------------------------------------------------------------------

procedure TGameBoard.PosePinguin(const x,y: Byte; Player: TBit);
var i:byte;
    p: TPoint;
begin
  IF    (CheckMove(X,Y,0,0)=TRUE)
    AND (Player=Currentplayer)
      THEN begin
        FField[x,y].full:=TRUE;
        FField[x,y].Player:=Player;
        //Nummer von noch nicht gesetztem Pinguin ermitteln
        i:=0;
        While self.Player[Player].Pinguin[i].Posed = TRUE do inc(i);
        //Diesen Pinguin setzen
        IF i<=3 //reine Vorsichtsma�nahme, dass keine Zugriffsverletzung auftreten kann.
          THEN begin
            self.Player[Player].Pinguin[i].FPosed:=TRUE;
            p.x:=x; p.y:=y;
            self.Player[Player].Pinguin[i].FPosition:=p;
            FField[x,y].PinguinNummer:=i;
            //N�chste Runde
            NextTurn;
          end;
      end;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetNextFieldInDirection(x,y: byte;Direction: TDirection): TPoint;
begin
  CASE Direction of
    0{oben}:
      begin
        result.X:=X;
        result.Y:=Y-1;
      end;
    1{rechts oben}:
      begin
        result.X:=X+1;
        result.Y:=Y-1;
      end;
    2{rechts}:
      begin
        result.X:=X+1;
        result.Y:=Y;
      end;
    3{unten}:
      begin
        result.X:=X;
        result.Y:=Y+1;
      end;
    4{links unten}:
      begin
        result.X:=X-1;
        result.Y:=Y+1;
      end;
    5{links}:
      begin
        result.X:=X-1;
        result.Y:=Y;
      end;
  end;
  //Falls result au�erhalb des ArrayBereichs oder Zielfeld ung�ltig, soll result auf (0|0) gesetzt werden
  IF (result.X<0) OR (result.X>10)
    OR (result.Y<0) OR (result.Y>7)
    OR (Field[result.X,result.Y].valid=FALSE)
      THEN begin
        result.X:=0; result.Y:=0;
        exit;
      end;

end;

//------------------------------------------------------------------------------

function TGameBoard.CheckPath(X1,Y1,X2,Y2: Byte): Boolean;
var x,y, i,j, k,l: byte;
begin
  //�berpr�fen, ob keine der unteren Bedingungen zutrifft
  IF (((Y2-Y1)<>(X2-X1)*(-1)) AND (X1<>X2) AND (Y1<>Y2))
    OR ((X1=X2) AND (Y1=Y2)) //�berpr�fen, ob Ausgang=Ziel
    THEN begin
      CheckPath:=FALSE;
      Exit;
    end;
  CheckPath:=FALSE;
      //Horizontal
      IF Y1=Y2
        THEN begin
          CheckPath:=TRUE;
          IF X1<X2
            THEN begin //nach rechts
              i:=X1+1; j:=X2;
            end
            ELSE begin //nach links
              i:=X2; j:=X1-1;
            end;
          FOR x:=i to j do
            IF (FField[x,Y1].Full=TRUE) OR (FField[x,Y1].valid=FALSE)//Wenn besetzt oder unm�glich ist,
              THEN CheckPath:=FALSE; //ist kein Pfad m�glich
          Exit;
        end;
      //Vertikal
      IF X1=X2

        THEN begin
          CheckPath:=TRUE;
          IF Y1<Y2
            THEN begin //nach oben
              k:=Y1+1; l:=Y2;
            end
            ELSE begin //nach unten
              k:=Y2; l:=Y1-1;
            end;
          FOR y:=k to l do
            IF (FField[X1,y].Full=TRUE) OR (FField[X1,y].valid=FALSE)//Wenn besetzt oder unm�glich ist,
              THEN CheckPath:=FALSE; //ist kein Pfad m�glich
          Exit;
        end;
      //Diagonal
      IF (Y2-Y1)=(X2-X1)*(-1)
        THEN begin
          CheckPath:=TRUE;
          IF (X1<X2) //nach rechts oben
            THEN begin
              i:=X1; j:=X2;//+1
              k:=Y1; l:=Y2;//-1
            end
            ELSE begin //nach links unten
              i:=X1; j:=X2;//-1
              k:=Y1; l:=Y2;//+1
            end;
          x:=i; y:=k;
          REPEAT
            IF (X1<X2) AND (x<>j) AND (y<>l)
              THEN begin //nach rechts oben
                inc(x); dec(y);
              end;
            IF (X1>X2) AND (x<>j) AND (y<>l)
              THEN begin //nach links unten
                dec(x); inc(y);
              end;
            IF (FField[x,y].Full=TRUE) OR (FField[x,y].valid=FALSE)
              THEN CheckPath:=FALSE;
          until (x=j) AND (y=l);
      end;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetPath(X1,Y1,X2,Y2: Byte): TPointArray;
var x,y, i,j, k,l: byte;
    A,B,P: TPoint;
    ArrayLength:Integer;
    Richtung: TDirection;
begin
  ArrayLength:=0;
  IF CheckPath(X1,Y1,X2,Y2)=TRUE
    THEN begin
      //Horizontal
      IF Y1=Y2
        THEN begin
          IF X1<X2
            THEN begin
              i:=X1+1; j:=X2; //nach rechts: Von X1 zu X2 z�hlen
            end
            ELSE begin
              i:=X2; j:=X1-1; //nach links: Von X2 zu X1 z�hlen
            end;
          FOR x:=i to j do
            IF (FField[x,Y1].Full=FALSE) AND (FField[x,Y1].valid=TRUE)
              THEN begin
                inc(ArrayLength);
                setlength(result,ArrayLength);
                result[ArrayLength-1].X:=x;
                result[ArrayLength-1].Y:=Y1;
              end;
        end;
      //Vertikal
      IF X1=X2
        THEN begin
          IF Y1<Y2
            THEN begin
              k:=Y1+1; l:=Y2;
            end
            ELSE begin
              k:=Y2; l:=Y1-1;
            end;
          FOR y:=k to l do
            IF (FField[X1,y].Full=FALSE) AND (FField[X1,y].valid=TRUE)
              THEN begin
                inc(ArrayLength);
                setlength(result,ArrayLength);
                result[ArrayLength-1].X:=X1;
                result[ArrayLength-1].Y:=y;
              end;
        end;
      //Diagonal
      IF (Y2-Y1)=(X2-X1)*(-1)
        THEN begin
          IF (X1<X2) //nach rechts oben
            THEN begin
              i:=X1; j:=X2;//+1
              k:=Y1; l:=Y2;//-1
            end
            ELSE begin //nach links unten
              i:=X1; j:=X2;//-1
              k:=Y1; l:=Y2;//+1
            end;
          x:=i; y:=k;
          REPEAT
            IF (X1<X2) AND (x<>j) AND (y<>l)
              THEN begin //nach rechts oben
                inc(x); dec(y);
              end;
            IF (X1>X2) AND (x<>j) AND (y<>l)
              THEN begin //nach links unten
                dec(x); inc(y);
              end;
            IF (FField[x,y].Full=FALSE) AND (FField[x,y].valid=TRUE)
              THEN begin
                inc(ArrayLength);
                setlength(result,ArrayLength);
                result[ArrayLength-1].X:=x;
                result[ArrayLength-1].Y:=y;
              end;
          until (x=j) AND (y=l);
      end;
    end
    ELSE begin
      setlength(result,1);
      result[0].X:=0; result[0].Y:=0;
    end;
//  IF (X1=X2) AND (Y1=Y2)
//    THEN begin
//      setlength(result,1);
//      result[0].X:=X1; result[0].Y:=Y1;
//      exit;
//    end;
//  IF CheckPath(X1,Y1,X2,Y2)=FALSE
//    THEN begin
//      setlength(result,1);
//      result[0].X:=0; result[0].Y:=0;
//      exit;
//    end;
//  //Richtung ermitteln
//  {oben}        IF (X1=X2) AND (Y1>Y2) THEN Richtung:=0;
//  {RechtsOben}  IF ((Y2-Y1)=(X2-X1)*(-1)) AND (X1<X2) THEN Richtung:=1;
//  {rechts}      IF (Y1=Y2) AND (X1<X2) THEN Richtung:=2;
//  {unten}       IF (X1=X2) AND (Y1>Y2) THEN Richtung:=3;
//  {LinksUnten}  IF ((Y2-Y1)=(X2-X1)*(-1)) AND (X1>X2) THEN Richtung:=4;
//  {links}       IF (Y1=Y2) AND (X1>X2) THEN Richtung:=5;
//  ArrayLength:=0;
//  P.X:=X1; P.Y:=Y1;
//  WHILE (P.X<>X2) AND (P.Y<>Y2) do
//    begin
//      inc(ArrayLength);
//      setlength(result,ArrayLength);
//      result[ArrayLength-1]:=NextFieldInDirection(P,Richtung);
//      P:=NextFieldInDirection(P,Richtung);
//    end;
end;





//------------------------------------------------------------------------------
//------------------------------------------------------------------------------





//###########################################
//#############  KOMMUNIKATION  #############
//###########################################


function TGameBoard.EncodeField(x,y:byte; Player0: TBit): byte;
//EncodeField ist die Zahl (dezimal), die im Code das Feld beschreibt
//Player0 ist der Player in TGameBoard, der im Code als Spieler 0 angegeben wird.
//Das ist daf�r n�tzlich, dass f�r den Client Player 0 immer er selbst ist.
var PlayerBit: TBit;
begin
  result:=0;
  IF FField[x,y].valid=TRUE
    THEN begin
      CASE FField[x,y].Fish of
        1: result:=4;  //die ersten beiden Bitstellen (8,4) ergeben: 01
        2: result:=8;  //die ersten beiden Bitstellen (8,4) ergeben: 10
        3: result:=12; //die ersten beiden Bitstellen (8,4) ergeben: 11
    end;
    IF FField[x,y].Full=TRUE
      THEN begin
        //Die Pinguine des Spielers, der als Player0 angegben ist, werden im Code als Pinguine
        //von dem Spieler der Zahl 0 gekennzeichnet.
        //Beispiel: Player0=1 -> Alle Pinguine von Spieler 1 werden im Code zu denen von Spieler 0
        //                       Umgekehrt werden alle von Spieler 0 zu denen von Spieler 1
        //          Player0=0 -> Spieler im Code und in TGameBoard stimmem �berein.
        IF (Player0=0) //An letzter Stelle steht Spieler (1 oder 0)
          THEN PlayerBit:= Field[x,y].Player
          ELSE
            IF Field[x,y].Player = Player0
              THEN PlayerBit:= 0
              ELSE PlayerBit:= 1;
        IF PlayerBit=0
          THEN result:=result + 2  //letzen beiden Bits:  10
          ELSE result:=result + 1; //letzen beiden Bits:  01
      end;
//      ELSE result:= result + 2; //An vorletzter Stelle steht 1, wenn nicht besetzt
  end;
end;

//------------------------------------------------------------------------------

function TGameBoard.EncodeBoard(Player0: TBit): AnsiString;
//Player0 ist der Player in TGameBoard, der im Code als Spieler 0 angegeben wird.
//Das ist daf�r n�tzlich, dass f�r den Client Player 0 immer er selbst ist.
var x,y: integer;
begin
  result:='';
  FOR y:=0 to 7 do  //er schreibt zeilenweise auf
    FOR x:=0 to 10 do
      begin
        //Feld aufschreiben
        result:=result + IntToStr(EncodeField(x,y,Player0)) + ' '; //Zahl f�r das Feld + Leerstelle anf�gen
      end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.DecodeBoard(s: AnsiString);
//Dekodiert das gesamte Spielfeld aus s in die TGameBoard-Instanz (ohne Anweisung f�r Client)
var y, x, tempInt: byte;
    l: integer;
    tempstring: AnsiString;
    FishCounter: array[1..3] of byte;
begin
  try
  x:=0; y:=0;
  FOR l:=1 to length(s)-1
    do begin
      IF s[l] = ' '
        THEN begin
          tempInt:=StrToInt(tempstring);
          IF tempInt=0
            THEN FField[x,y].valid:=FALSE
            ELSE begin
              FField[x,y].valid:=TRUE;
              //Fische
              FField[x,y].Fish:=0;
              IF tempInt >= 8 //Wenn 4. Bit 1 ist
                THEN begin
                  FField[x,y].Fish:=2; //hat Feld schonmal 2 Fische
                  IF (tempInt-8) >= 4 THEN FField[x,y].Fish:=3; //Wenn auch 3. Bit 1 ist, hat Feld 3 Fische
                end
                ELSE IF tempInt >= 4 THEN FField[x,y].Fish:=1; //Wenn 4. Bit 0 ist, gibt es nur einen Fisch
              //Spieler
              FField[x,y].Full:=FALSE;
              IF (tempInt - FField[x,y].Fish*4 = 1) //Wenn die beiden kleinsten Bits 01 sind, ist Spieler 1 auf dem Feld
                THEN begin
                FField[x,y].Full:=TRUE;
                FField[x,y].Player:=1;
                end;
              IF (tempInt - FField[x,y].Fish*4 > 1) //Wenn die beiden kleinsten Bits 10 sind, ist Spieler 0 auf dem Feld
                THEN begin
                FField[x,y].Full:=TRUE;
                FField[x,y].Player:=0;
                end;
            end;
          //N�chstes Feld
          x := x + 1;
          IF x > 10 //N�chste Zeile
            THEN begin
              y := y + 1;
              x := 0;
            end;
          tempstring := '';
        end
        ELSE tempstring := tempstring + s[l];  //N�chster Char im String
    end;
  //Pinguine neu nummerieren
  renumberPinguins;
  //Spielfeld f�r die Pinguine aktualisieren
  FOR x:=0 to 3 do
    begin
      FOR y:=0 to 1 do
        begin
          Player[y].FPinguin[x].GameBoard:=self;
        end;
    end;
  //Images neu anordnen
  IF Hexvisible=TRUE
    THEN begin
      FishCounter[1]:=1;
      FishCounter[2]:=1;
      FishCounter[3]:=1;
      FOR x:=0 to 10 do
        FOR y:=0 to 7 do
          begin
            IF (Field[x,y].OnGameBoard=TRUE) AND (Field[x,y].valid=TRUE)
              THEN begin
                Img_Fish[Field[x,y].Fish,FishCounter[Field[x,y].Fish]].Position.X:=x;
                Img_Fish[Field[x,y].Fish,FishCounter[Field[x,y].Fish]].Position.y:=y;
                Img_Fish[Field[x,y].Fish,FishCounter[Field[x,y].Fish]].Left:=ImgPos[x,y].X;
                Img_Fish[Field[x,y].Fish,FishCounter[Field[x,y].Fish]].Top:=ImgPos[x,y].Y;
                inc(FishCounter[Field[x,y].Fish]);
              end;
          end;
    end;
  except
    on exception do ShowMessage('Fehler beim Dekodieren des Spielfeldes!');
  end;
end;

//------------------------------------------------------------------------------    


procedure TGameBoard.OnNewLine(Sender: TObject; NewLine: string;
    OutputType: TOutputType);
//wird aufgerufen, wenn der Client etwas in sein Konsolenfenster geschrieben hat.
var Client: TBit;
    X1,Y1,X2,Y2, StringPosX2: byte;
    IntString: array[2..4] of ShortString;
    NewLineCopy: String;
    s: String;
    t: Integer;
    Stats: TClientTimeStats;
    LogDebug: Boolean; //Soll Debug ausgegben werden?
begin
  LogDebug := true;
  IF Sender=Player[0].DosCommand THEN Client:=0;
  IF Sender=Player[1].DosCommand THEN Client:=1;

  try
  //Faktoren einlesen
  if (FaktCounter > -1) and
     (FaktCounter < High(FaktRawStr)) then
  begin
    LogDebug := false;
    FaktRawStr[FaktCounter] := NewLine;
    inc(FaktCounter);
    if FaktCounter >= High(FaktRawStr) then
      FaktCounter := -1;             
    exit;
  end;

   //Punkte auslesen
   NewLineCopy:=NewLine+' ';
   s := copy(NewLineCopy, 1, pos(' ', NewLineCopy)-1);
   delete(NewLineCopy, 1, pos(' ', NewLineCopy));
   Y1:=StrToIntDef(s, 0);
   s := copy(NewLineCopy, 1, pos(' ', NewLineCopy)-1);
   delete(NewLineCopy, 1, pos(' ', NewLineCopy));
   X1:=StrToIntDef(s, 0);
   s := copy(NewLineCopy, 1, pos(' ', NewLineCopy)-1);
   delete(NewLineCopy, 1, pos(' ', NewLineCopy));
   Y2:=StrToIntDef(s, 0);
   s := copy(NewLineCopy, 1, pos(' ', NewLineCopy)-1);
   delete(NewLineCopy, 1, pos(' ', NewLineCopy));
   X2:=StrToIntDef(s, 0);
  except
    on EAccessViolation do
        begin                                        
          ShowMessage('Fehler beim Auslesen des Clientoutputs (Zugriffsfehler)');
          exit;
        end;
    on EConvertError do
        begin
          ShowMessage('Fehler beim Auslesen des Clientoutputs (Konvertierungsfehler)');
          exit;
        end;
  end;
  
  //Debug
  IF ((Copy(NewLine,1,5)='debug') OR ((X1=0) AND (X2=0) AND (Y1=0) AND (Y2=0))) and (LogDebug = true)
    THEN begin
      IF DebugPlayername=FALSE
        THEN SendDebug('Client'+IntToStr(Client)+':  '+NewLine)
        ELSE SendDebug(Player[Client].Name+':  '+NewLine);
      exit;
    end
    ELSE

    //TimeStats
    IF (Timer_ClientDraw.Enabled=TRUE) AND (Copy(NewLine,1,5)<>'debug')
      THEN begin
        Player[Client].FClientIsDrawing:=FALSE;
        Timer_ClientDraw.Enabled:=FALSE;
        t:=round(FMyTimer.CheckTime/10);
        with Player[Client].FTimeStats do
          begin
            IF (Turn=1) OR (Turn=2)
              //Erster Zug
              THEN begin 
                FirstAverage:=t;
                FirstMaximum:=t;
                FirstMinimum:=t;
              end
              //Sonstige Z�ge
              ELSE begin 
                IF Average>0
                  THEN Average:=round((Average*(Turn div 2)+t) / (1+(Turn div 2)))
                  ELSE Average:=t;
                IF (t>Maximum) THEN Maximum:=t;
                IF (Minimum>0)
                  THEN begin
                    IF (t<Minimum) THEN Minimum:=t;
                  end
                  ELSE Minimum:=t;
              end;
            //Gesamtes Spiel
            PartSum:=PartSum+t; //Erstmal nur z�hlen, am Ende des Spiels zusammenaddieren
          end;
    end;

  //Zug ins Debugfenster
  IF (DebugSendDraw=TRUE) and (LogDebug = true)
    THEN begin
      IF DebugPlayername=FALSE
        THEN SendDebug('Client'+IntToStr(Client)+':  '+NewLine)
        ELSE SendDebug(Player[Client].Name+':  '+NewLine);
    end;

  //Zug
  IF Client=CurrentPlayer
    THEN
      IF CheckMove(X1,Y1,X2,Y2)=TRUE
        THEN begin
          IF HexVisible=TRUE
            THEN begin
              IF (Player[CurrentPlayer].ClientWait=TRUE)
                THEN begin
                  MarkField(X1,Y1,clPlayer);
                  IF (turn>8) THEN MarkTPointArray(GetPath(X1,Y1,X2,Y2),clMarkClient);
                  sleep(Player[CurrentPlayer].WaitSleep);
                end;
            end;
          Move(X1,Y1,X2,Y2);
        end
        ELSE begin
          IF Calculating=FALSE
            THEN begin
              ShowMessage('Ung�ltigen Zug vom Client erhalten, fordern Sie ggf. manuell einen weiteren! ("'+Newline+'")');
              Form_Server.Btn_ClientTurn.Enabled:=TRUE;
              IF (Form_Server.Focused=TRUE) THEN Form_Server.Btn_ClientTurn.SetFocus;
            end
            ELSE begin
              IF Client=0 THEN S:=Player[1].Name ELSE S:=Player[0].Name;
              IF Assigned(FOnErrorReport)
                THEN FOnErrorReport('Ung�ltiger Zug von '+Player[Client].Name+'; Gegner: '+S+'; Spiel: '+IntToStr(TestSettings.Counter)+'; Runde: '+IntToStr(Turn));
              //Neues Spiel
              New;
              Player[CurrentPlayer].SendToClient(1);
            end;

        end;
end;






//------------------------------------------------------------------------------
//------------------------------------------------------------------------------






//###########################################
//################  EINGABE  ################
//###########################################

function TGameBoard.MouseToPos(const x,y:integer): TPoint;
var i,j: integer; //i: Spalten, j: Zeilen
begin
  result.X:=0; result.Y:=0;
  //Zeile ermitteln
  FOR j:=0 to 7 do
    begin
      IF y>=ImgPos[0,j].Y THEN result.Y:=j;
    end;
  //Spalte ermitteln
  FOR i:=0 to 10 do
    begin
      IF (x>=ImgPos[i,result.Y].X) THEN result.X:=i
    end;
  //Falls au�erhalb des Spielfeldes oder unm�gliches Feld auf -1|-1 setzen
  IF   (x>ImgPos[7,6].X+Bmp_Fish1.Width)
    OR (y>ImgPos[6,7].Y+Bmp_Fish1.Height)
    OR (x<ImgPos[3,0].X)
    OR (y<ImgPos[3,0].Y)
    OR (FField[result.X,result.Y].valid=FALSE)
    THEN
      begin
        result.X:=0;
        result.Y:=0;
      end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.PB_CursorMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
var Path: TPointArray;
begin
  IF PB_Refresh=PB_RefreshMAX
    THEN begin
      PB_Refresh:=0;
      FZiel:=MouseToPos(X+Left,Y);
      //Hint f�r Pinguin zeigen
      PB_Cursor.ShowHint:=FALSE;
      IF Field[Ziel.X,Ziel.Y].Full=TRUE
        THEN begin
          PB_Cursor.Hint:=Img_Pinguin[Field[Ziel.X,Ziel.Y].Player, Field[Ziel.X,Ziel.Y].PinguinNummer].Hint;
          PB_Cursor.ShowHint:=Img_Pinguin[Field[Ziel.X,Ziel.Y].Player, Field[Ziel.X,Ziel.Y].PinguinNummer].ShowHint;
        end;
      //Pfad markieren
      IF (Player[CurrentPlayer].CPUControlled=FALSE) OR (MouseDown=TRUE)
        THEN Paintbox.Refresh;
      MarkField(Ausgang.X,Ausgang.Y,clPlayer);
      Path:= GetPath(Ausgang.X,Ausgang.Y,Ziel.X,Ziel.Y);
      IF    (MouseDown=TRUE)
        AND (Ziel.X<>-1)    AND (Ziel.Y<>-1)
        AND (Ausgang.X<>-1) AND (Ausgang.Y<>-1)
        AND (CheckPath(Ausgang.X,Ausgang.Y,Ziel.X,Ziel.Y)=TRUE)
          THEN MarkTPointArray(GetPath(Ausgang.X,Ausgang.Y,Ziel.X,Ziel.Y),clMarkPlayer);
    end
    ELSE inc(PB_Refresh);
  //Ereignisse
  IF Assigned(FOnMouseMove) THEN FOnMouseMove;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.PB_CursorMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var FieldPos: TPoint; //Position in der Matrix
begin
  IF HexVisible=TRUE
    THEN begin
      FieldPos:=MouseToPos(x+Left,y);
      IF    (Field[FieldPos.X,FieldPos.Y].Full=TRUE) //Nur wenn tats�chlich ein eigener Pinguin auf dem                                            
        AND (Field[FieldPos.X,FieldPos.Y].Player=Currentplayer)//ausgew�hlten Feld steht, wird markiert
        AND (Turn>8)//Alle Pinguine gesetzt
        THEN begin
          FAusgang:=FieldPos;
          MouseDown:=TRUE;
          IF (FieldPos.X<>0) AND (FieldPos.Y<>0)
            THEN begin
              IF QuadVisible=TRUE
                THEN begin
                  StringGrid.Col:=FieldPos.X+1;
                  StringGrid.Row:=FieldPos.Y+1;
                end;
              MarkField(Ausgang.X,Ausgang.Y,clPlayer);
            end;
        end;
    end;
  //Ereignisse
  IF Assigned(FOnMouseMove) THEN FOnMouseMove;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.PB_CursorMouseUp(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: Integer);
begin       
//  PB_Cursor.Refresh;
  IF (Turn>=8) AND (MouseDown=TRUE)
    THEN
      IF CheckMove(Ausgang.X,Ausgang.Y,Ziel.X,Ziel.Y)=TRUE
        THEN begin
          Move(Ausgang.X,Ausgang.Y,Ziel.X,Ziel.Y);
        end;
  MouseDown:=FALSE;
  //Ereignisse
  IF Assigned(FOnMouseMove) THEN FOnMouseMove;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.PB_CursorDblClick(Sender: TObject);
begin
  IF (Player[CurrentPlayer].AllPinguinsPosed=FALSE)
    AND (Field[Ziel.X,Ziel.Y].Full=FALSE)
    AND (Field[Ziel.X,Ziel.Y].valid=TRUE)
    AND (Field[Ziel.X,Ziel.Y].Fish=1)
    THEN
      IF CheckMove(Ziel.X,Ziel.Y,0,0)=TRUE
        THEN begin
          Move(Ziel.X,Ziel.Y,0,0);
        end;
  //Ereignisse
  IF Assigned(FOnMouseMove) THEN FOnMouseMove;
end;







//------------------------------------------------------------------------------
//------------------------------------------------------------------------------




//###########################################
//##########  FELDINFORMATIONEN  ############
//###########################################

procedure TGameBoard.renumberPinguins;
var x,y: integer;
    Number: array[0..1] of byte;
begin
  Number[0]:=0; Number[1]:=0;
  FOR x:=0 to 1 do
    FOR y:=0 to 3 do
      begin
        Player[x].FPinguin[y].FPosed:=FALSE;
        Player[x].FPinguin[y].FPosition.X:=0;
        Player[x].FPinguin[y].FPosition.Y:=0;
      end;
  FOR y:=0 to 7 do
    FOR x:=0 to 10 do
      begin
        IF (Field[x,y].Full=TRUE) AND (Field[x,y].Valid=TRUE)
          THEN begin
            Player[Field[x,y].Player].FPinguin[Number[Field[x,y].Player]].FPosition.X:=x;
            Player[Field[x,y].Player].FPinguin[Number[Field[x,y].Player]].FPosition.Y:=y;
            FField[x,y].PinguinNummer:=Number[Field[x,y].Player];
            Player[Field[x,y].Player].FPinguin[Number[Field[x,y].Player]].FPosed:=TRUE;
            inc(Number[Field[x,y].Player]);
          end;
      end;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetTargetsInDirection(const x,y: byte; Direction: TDirection): TPointArray;
var P: TPoint;
begin
  P:=GetNextFieldInDirection(x,y,Direction);
  While (Field[P.X,P.Y].Valid=TRUE) AND (Field[P.X,P.Y].Full=FALSE) do
    begin
      setlength(result,length(result)+1);
      result[length(result)-1]:=P;
      P:=GetNextFieldInDirection(P.X,P.Y,Direction);
    end;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetAllTargets(const x,y:byte): TPointArray;
var i: TDirection;
    j,k: integer;
    TargetsInDir: TPointArray;
begin
  FOR i:=0 to 5 do
    begin
      //Arraylength um L�nge von GetTargetsInDirection verl�ngern
      k:=length(result);
      TargetsInDir:=GetTargetsInDirection(X,Y,i);
      setlength(result, k + length(TargetsInDir));
      //GetTargetsInDirection(i) an result anh�ngen
      FOR j:=k  to length(result)-1 do
        begin
          result[j]:=TargetsInDir[j - k];
        end;
    end;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetAllNeighborFields(x,y:byte): TPointArray;
var r:TDirection;
begin
  FOR r:=0 to 5 do
    begin
      setlength(result,length(result)+1);
      result[length(result)-1]:=GetNextFieldInDirection(x,y,r);
    end;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetFieldsInRadiusPrivate(x,y: byte; Radius: byte): TPointArray;
var r, i:Integer;
    d: TDirection;
    InArray: Boolean; //Feld schon im resultArray vorhanden
    FeldAusgang,FeldNachbar: TPoint;
    PointArray: TPointArray;
    Originalfelder: TField;
begin
  result:=GetAllNeighborFields(x,y);
  //Immer um Ausgang herum abtasten
  FOR r:=1 to Radius do
    begin
      //Funktion ruft sich selbst wieder auf mit Radius-1
      IF Radius-1 > 0
        THEN begin
          FOR d:=0 to 5 do
            begin
              //weitermachen
              FeldNachbar:=GetNextFieldInDirection(X,Y,d);
              PointArray:=GetFieldsInRadius(FeldNachbar.X,FeldNachbar.Y,Radius-1);
              //und h�ngt das Ergebnis an sich selbst dran
              result:=AddPointArrays(result,PointArray,FALSE);
            end;
        end;
    end;
    //Schon abgearbeitete Felder bei OnGameBoard auf FALSE setzen, damit sie nicht noch einmal durchlaufen werden.
    FOR i:=0 to High(result) do
    FField[result[i].X,result[i].Y].OnGameBoard:=FALSE;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetFieldsInRadius(x,y: byte; Radius: byte): TPointArray;
  //Alle Felder um ein Feld herum im angegbenen Radius, Felder au�erhalb des Spielfelds werden gefiltert
  //Alle angrenzdenen Felder zum result addieren und diese Vorgang f�r alle diese Felder wiederholen
var r, i:Integer;
    d: TDirection;
    InArray: Boolean; //Feld schon im resultArray vorhanden
    FeldAusgang,FeldNachbar: TPoint;
    PointArray: TPointArray;
    Originalfelder: TField;
begin
  OriginalFelder:=self.Field; //Originalfelder sichern
  result:=GetAllNeighborFields(x,y);
  //Immer um Ausgang herum abtasten
  FOR r:=1 to Radius do
    begin
      //Funktion ruft sich selbst wieder auf mit Radius-1
      IF Radius-1 > 0
        THEN begin
          FOR d:=0 to 5 do
            begin
              //Schon abgearbeitete Felder bei OnGameBoard auf FALSE setzen, damit sie nicht noch einmal durchlaufen werden.
              FOR i:=0 to High(result) do
                FField[result[i].X,result[i].Y].OnGameBoard:=FALSE;
              //weitermachen
              FeldNachbar:=GetNextFieldInDirection(X,Y,d);
              PointArray:=GetFieldsInRadiusPrivate(FeldNachbar.X,FeldNachbar.Y,Radius-1);
              //und h�ngt das Ergebnis an sich selbst dran
              result:=AddPointArrays(result,PointArray,FALSE);
            end;
        end;
    end;
  self.FField:=OriginalFelder;

end;

//------------------------------------------------------------------------------

function TGameBoard.GetAllFish(const x,y: byte): integer;
var i: integer;
    Fields: TPointArray;
begin
  result:=0;
  Fields:=GetAllTargets(x,y);
  IF length(Fields)<>0
    THEN
      FOR i:=0 to length(Fields)-1 do
        result:=result+Field[Fields[i].X,Fields[i].Y].Fish;
end;



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------




//###########################################
//###########  SPEICHERN & LADEN  ###########
//###########################################

procedure TGameBoard.SaveTurn;
begin
  setLength(FSavedTurn,Turn+1);
  with FSavedTurn[length(SavedTurn)-1] do
    begin
      Fields:=EncodeBoard(0);
      CurrentPlayer:=self.CurrentPlayer;
      Score[0]:=self.Player[0].Score;
      Score[1]:=self.Player[1].Score;
      FieldsCollected[0]:=self.Player[0].FieldsCollected;
      FieldsCollected[1]:=self.Player[1].FieldsCollected;
    end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SaveGameToFile(Path: String);
var Datei:textfile;
    i:byte;
    buttoncode: word;
begin
  IF length(SavedTurn)-1 < Turn
    THEN begin
      MessageDlg('Fehler beim Speichern der Datei, die Aufzeichnung ist nicht vollst�ndig.', mtError, [mbOK], 0);
      exit;
    end;
  try
    Assignfile(Datei,Path);
    IF FileExists(Path)=TRUE
      THEN begin
        Erase(Datei);
        Assignfile(Datei,Path);
      end;
    rewrite(Datei);
    //Spielernamen
    writeln(Datei,Player[0].Name);
    writeln(Datei,Player[1].Name);
    //Anzahl der Runden
    writeln(Datei,length(SavedTurn)-1);
    for i:=1 to length(SavedTurn)-1 do
      begin
        writeln(Datei,SavedTurn[i].Fields); //Spielfeld
        writeln(Datei, IntToStr(SavedTurn[i].CurrentPlayer));
        writeln(Datei, IntToStr(SavedTurn[i].Score[0]));
        writeln(Datei, IntToStr(SavedTurn[i].Score[1]));
        writeln(Datei, IntToStr(SavedTurn[i].FieldsCollected[0]));
        writeln(Datei, IntToStr(SavedTurn[i].FieldsCollected[1]));
        writeln(Datei, ' '); //Leerzeile zur Trennung der Runden
      end;
    Closefile(Datei);
  except on Exception do MessageDlg('Fehler beim Speichern der Datei', mtError, [mbOK], 0);
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.LoadGameFromFile(Path: String);
var Datei:textfile;
    StringBuffer: String;
    buttoncode: word; //Nur daf�r da, MessageDialog ausf�hren zu k�nnen
    Steps,i: integer;
begin
  try
    Assignfile(Datei,Path);
    reset(Datei);
    //Spielernamen
    //Aktuelle Spielernamen zwischenspeichern
    Player[0].SaveName:=Player[0].Name;
    Player[1].SaveName:=Player[1].Name;
    readln(Datei, self.Player[0].FName);
    readln(Datei, self.Player[1].FName);
    //Schritte
    readln(Datei, StringBuffer); //Zeile wird in den StringBuffer gelesen
    Steps:=StrToInt(StringBuffer);
    setlength(FSavedTurn,Steps+1);
    FOR i:=1 to Steps do
      begin
        readln(Datei, StringBuffer);
          FSavedTurn[i].Fields:=StringBuffer; //Spielfeld
        readln(Datei, StringBuffer);
          FSavedTurn[i].CurrentPlayer:=StrToInt(StringBuffer);
        readln(Datei, StringBuffer);
          FSavedTurn[i].Score[0]:=StrToInt(StringBuffer);
        readln(Datei, StringBuffer);
          FSavedTurn[i].Score[1]:=StrToInt(StringBuffer);
        readln(Datei, StringBuffer);
          FSavedTurn[i].FieldsCollected[0]:=StrToInt(StringBuffer);
        readln(Datei, StringBuffer);
          FSavedTurn[i].FieldsCollected[1]:=StrToInt(StringBuffer);
        readln(Datei); //Leerzeile zur Trennung der Runden
      end;
    CloseFile(Datei);
    FGameShow:=TRUE;
    LoadStep(1);
  except on Exception do MessageDlg('Fehler beim Laden des Spiels: '+Path, mtError, [mbOK], 0);
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.ContinueGame(Turns: TSavedTurnArr);
begin
  FSavedTurn:=Turns;
  LoadStep(110);  //letzte Runde laden
  GetIntoGame;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.LoadStep(TargetPos: integer);
begin
  try
      IF TargetPos<1 THEN TargetPos:=1;
      IF TargetPos>length(SavedTurn)-1 THEN TargetPos:=length(SavedTurn)-1;
      //Inhalte laden
      DecodeBoard(SavedTurn[TargetPos].Fields);
      FTurn:=TargetPos;
      self.Currentplayer:=SavedTurn[TargetPos].CurrentPlayer;
      self.Player[0].FFieldsCollected:=SavedTurn[TargetPos].FieldsCollected[0];
      self.Player[1].FFieldsCollected:=SavedTurn[TargetPos].FieldsCollected[1];
      self.Player[0].FScore:=SavedTurn[TargetPos].Score[0];
      self.Player[1].FScore:=SavedTurn[TargetPos].Score[1];

      //Visualisierung
      IF HexVisible=TRUE THEN DrawHex;
      IF QuadVisible=TRUE THEN DrawQuad;
      //Ereignisse
      IF Assigned(FOnNewTurn) THEN FOnNewTurn;
  except
    on Exception do ShowMessage('Fehler beim Laden von Runde '+IntToStr(TargetPos));
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.GetIntoGame;
begin
  FGameShow:=FALSE;
  //Ereignisse
  IF Assigned(FOnNewTurn) THEN FOnNewTurn;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SaveGameSettingsInFile(Settings: TGameSettings; Path: String);
var Datei: File of TGameSettings;
begin
  try
    Assignfile(Datei,Path);
    rewrite(Datei);
    write(Datei,Settings);
  finally
    CloseFile(Datei);
  end;
end;
//------------------------------------------------------------------------------
function TGameBoard.LoadGameSettingsFromFile(Path: String): TGameSettings;
var Datei: File of TGameSettings;
begin
  try
    Assignfile(Datei,Path);
    reset(Datei);
    read(Datei,result);
  finally
    CloseFile(Datei);
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SaveTestSettingsInFile(Settings: TTestSettings; Path: String);
var Datei: File of TTestSettings;
    Data: TTestSettings;
begin
  try
    Assignfile(Datei,Path);
    rewrite(Datei);
    Data:=Settings;
    write(Datei,Data);
  finally
    CloseFile(Datei);
  end;
end;
//------------------------------------------------------------------------------
function TGameBoard.LoadTestSettingsFromFile(Path: String): TTestSettings;
var Datei: File of TTestSettings;
begin
  try
    Assignfile(Datei,Path);
    reset(Datei);
    read(Datei,result);
  finally
    CloseFile(Datei);
  end;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SaveTimeStatsInFile(ClientTimeStats: TClientTimeStatsArray; Path: String);
var Datei: File of TClientTimeStatsArray;
begin
  try
    Assignfile(Datei,Path);
    rewrite(Datei);
    write(Datei,ClientTimeStats);
  finally
    CloseFile(Datei);
  end;
end;

//------------------------------------------------------------------------------

function TGameBoard.LoadTimeStatsFromFile(Path: String): TClientTimeStatsArray;
var Datei: File of TClientTimeStatsArray;
begin
  try
    Assignfile(Datei,Path);
    reset(Datei);
    read(Datei,result)
  finally
    CloseFile(Datei);
  end;
end;






//------------------------------------------------------------------------------
//------------------------------------------------------------------------------







//###########################################
//#############  EIGENSCHAFTEN  #############
//###########################################

//------------------------------------------------------------------------------
function TGameBoard.GetSettings: TGameSettings;
//Alle Einstellungen vom GameBoard laden
begin
  result.HexVisible:=HexVisible;
  result.HexTransparency:=HexTransparency;
  result.ShowPinguinHints:=ShowPinguinHints;
  result.ShowDebugForm:=Form_Debug.Visible;

  result.DebugFormPos.X:=Form_Debug.Left;
  result.DebugFormPos.Y:=Form_Debug.Top;
  result.DebugFormSize.X:=Form_Debug.Width;
  result.DebugFormSize.Y:=Form_Debug.Height;
  result.DebugAddTime:=Form_Debug.Memo.AddTime;
  result.DebugPlayerName:=DebugPlayerName;
  result.DebugSendDraw:=DebugSendDraw;

  result.ShowTimeForm:=Form_TimeStats.Visible;
  result.TimeFormPos.X:=Form_TimeStats.Left;
  result.TimeFormPos.Y:=Form_TimeStats.Top;
  result.ShowCounter:=Form_TimeStats.CB_Counter.Checked;

  result.clMarkPlayer:=clMarkPlayer;
  result.clMarkClient:=clMarkClient;
  result.QuadVisible:=QuadVisible;
  result.SaveTurnOption:=SaveTurnOption;
  result.AutoSave:=AutoSave;
  result.AutoSavePath:=AutoSavePath;
  result.AutoContinue:=AutoContinue;
  result.StatSave:=StatSave;
  result.StatPath:=StatPath;
  result.GameShowInterval:=GameShowInterval;
  result.StartPlayer:=StartPlayer;
  result.ClientTimeOut:=ClientTimeOut;
  result.JavaWarningShown:=JavaWarningShown;

  //Player 0
  result.Name0:=Player[0].Name;
  result.CPUControlled0:=Player[0].CPUControlled;
  result.ClientPath0:=Player[0].ClientPath;
  result.AutoMove0:=Player[0].AutoMove;
  result.AutoMove0:=Player[0].AutoMove;
  result.ClientWait0:=Player[0].ClientWait;
  result.WaitSleep0:=Player[0].WaitSleep;
  result.JavaClient0:=Player[0].JavaClient;

  //Player 1
  result.Name1:=Player[1].Name;
  result.CPUControlled1:=Player[1].CPUControlled;
  result.ClientPath1:=Player[1].ClientPath;
  result.AutoMove1:=Player[1].AutoMove;
  result.AutoMove1:=Player[1].AutoMove;
  result.ClientWait1:=Player[1].ClientWait;
  result.WaitSleep1:=Player[1].WaitSleep;
  result.JavaClient1:=Player[1].JavaClient;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SetSettings(Value: TGameSettings);
begin
  //Alle Einstellungen auf das GameBoard �bertragen
  HexVisible:=Value.HexVisible;
  HexTransparency:=Value.HexTransparency;
  ShowPinguinHints:=Value.ShowPinguinHints;
  clMarkPlayer:=Value.clMarkPlayer;
  clMarkClient:=Value.clMarkClient;
  QuadVisible:=Value.QuadVisible;
  SaveTurnOption:=Value.SaveTurnOption;
  AutoSave:=Value.AutoSave;
  AutoSavePath:=Value.AutoSavePath;
  AutoContinue:=Value.AutoContinue;
  StatSave:=Value.StatSave;
  IF StatSave=TRUE
    THEN StatPath:=Value.StatPath;
  GameShowInterval:=Value.GameShowInterval;
  StartPlayer:=Value.StartPlayer;
  ClientTimeOut:=Value.ClientTimeOut;
  JavaWarningShown:=Value.JavaWarningShown;

  //Player 0
  Player[0].Name:=Value.Name0;
  Player[0].FCPUControlled:=Value.CPUControlled0;
  Player[0].JavaClient:=Value.JavaClient0; //Muss vor dem ClientPath stehen!
  IF Player[0].CPUControlled=TRUE
    THEN Player[0].ClientPath:=Value.ClientPath0;
  Player[0].AutoMove:=Value.AutoMove0;
  Player[0].ClientWait:=Value.ClientWait0;
  Player[0].WaitSleep:=Value.WaitSleep0;

  //Player 1
  Player[1].Name:=Value.Name1;
  Player[1].FCPUControlled:=Value.CPUControlled1;
  Player[1].JavaClient:=Value.JavaClient1;  //Muss vor dem ClientPath stehen!//Muss vor dem ClientPath stehen!
  IF Player[1].CPUControlled=TRUE
    THEN Player[1].ClientPath:=Value.ClientPath1;
  Player[1].AutoMove:=Value.AutoMove1;
  Player[1].ClientWait:=Value.ClientWait1;
  Player[1].WaitSleep:=Value.WaitSleep1;

  DebugSendDraw:=Value.DebugSendDraw;
  IF Form_Debug<>nil
    THEN begin
      with Form_Debug do
        begin
          IF Value.ShowDebugForm<>Visible
            THEN Visible:=Value.ShowDebugForm;
          Left:=Value.DebugFormPos.X;
          Top:=Value.DebugFormPos.Y;
          Width:=Value.DebugFormSize.X;
          Height:=Value.DebugFormSize.Y;
        end;
      IF (Form_Debug.Memo<>nil)
        THEN Form_Debug.Memo.AddTime:=Value.DebugAddTime;
    end;
  DebugPlayerName:=Value.DebugPlayerName;

  IF Form_TimeStats<>nil
    THEN begin
      with Form_TimeStats do
        begin
          IF Value.ShowTimeForm=TRUE
            THEN Show(ClientTimeStats)
            ELSE Hide;
          Board:=self;
          Left:=Value.TimeFormPos.X;
          Top:=Value.TimeFormPos.Y;
          CB_Counter.Checked:=Value.ShowCounter;
        end;
    end;

end;

//------------------------------------------------------------------------------

function TGameBoard.GetTestSettings: TTestSettings;
begin
  result:=FTestSettings;
  //High0 und High1 bestimmen
  result.High0:=0;
  While (result.High0<High(result.Clients0)) AND (result.Clients0[result.High0].Name<>'')
    do inc(result.High0);
  IF result.Clients0[result.High0].Name=''
    THEN result.High0:=result.High0-1;
  result.High1:=0;
  While (result.High1<High(result.Clients1)) AND (result.Clients1[result.High1].Name<>'')
    do inc(result.High1);
  IF result.Clients1[result.High1].Name=''
    THEN result.High1:=result.High1-1;

end;

//------------------------------------------------------------------------------

procedure TGameBoard.SetStatPath(Value: String);
begin
  IF Value<>''
    THEN begin
      CheckFileEnding(Value,'.csv',true);
      FStatPath:=Value;
    end
    ELSE ShowMessage('Angegebener Speicherpfad f�r die Statistik ist ung�ltig!');
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SetAutoSavePath(Value: String);
begin
  IF Value<>''
    THEN begin
      CheckFileEnding(Value,'.pap',true);
      FAutoSavePath:=Value;
    end
    ELSE ShowMessage('Angegebener Speicherpfad f�r die automatische Spielspeicherung ist ung�ltig!');
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SetHexTransparency(Value: Boolean);
var i: byte;
begin
  FHexTransparency:=Value;
  FOR i:=1 to 30 do
    Img_Fish[1,i].Transparent:=Hextransparency;
  FOR i:=1 to 20 do
    Img_Fish[2,i].Transparent:=Hextransparency;
  FOR i:=1 to 10 do
    Img_Fish[3,i].Transparent:=Hextransparency;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SetShowPinguinHints(Value: Boolean);
var i,j:byte;
begin
  FOR i:=0 to 1 do
    FOR j:=0 to 3 do
      Img_Pinguin[i,j].ShowHint:=Value;
  FShowPinguinHints:=Value;
end;


//------------------------------------------------------------------------------

procedure TGameBoard.SetHexVisible(Value: Boolean);
var i: byte;
begin
  IF (Value=FALSE) AND (Calculating=FALSE)
    THEN begin
    //Alle Images unsichtbar machen
      FOR i:=1 to 30 do
        Img_Fish[1,i].visible:=FALSE;
      FOR i:=1 to 20 do
        Img_Fish[2,i].visible:=FALSE;
      FOR i:=1 to 10 do
        Img_Fish[3,i].visible:=FALSE;
      FOR i:=0 to 3 do
        begin
          Img_Pinguin[0,i].visible:=FALSE;
          Img_Pinguin[1,i].visible:=FALSE;
        end;
    end
    ELSE DrawHex;
  FHexVisible:=Value;
  IF Paintbox<>nil THEN Paintbox.enabled:=Value;
  IF PB_Cursor<>nil THEN PB_Cursor.enabled:=Value;
end;

//------------------------------------------------------------------------------

procedure TGameBoard.SetQuadVisible(Value: Boolean);
begin
  IF (Value=FALSE)
    THEN begin
      IF StringGrid<>nil THEN StringGrid.Visible:=FALSE;
    end
    ELSE begin
      IF StringGrid<>nil
        THEN begin
          FQuadVisible:=TRUE;
          DrawQuad;
          StringGrid.Visible:=TRUE;
        end
        ELSE begin
          FQuadVisible:=FALSE;
          ShowMessage('Keine Ausgabe in StringGrid m�glich, definieren Sie zuerst ein StringGrid im Formular f�r Ihre Instanz von TGameBoard.StringGrid.');
        end;
    end;
  IF (StringGrid<>nil) OR (Calculating=FALSE)
    THEN FQuadVisible:=Value;
end;

//------------------------------------------------------------------------------

function TGameBoard.GetclPlayer: TColor;
begin
  result:=Player[CurrentPlayer].Color;
end;
//------------------------------------------------------------------------------
function TGameBoard.GetWidth: integer;
begin
  result:=ImgPos[7,6].X + Bmp_Fish1.Width - Left;
end;
//------------------------------------------------------------------------------
function TGameBoard.GetHeight: integer;
begin
  result:=ImgPos[6,7].Y + Bmp_Fish1.Height - Top;
end;
//------------------------------------------------------------------------------
function TGameBoard.GetCurrentPlayer: TBit;
begin
  IF Player[0].AmZug=TRUE
    THEN result:=0
    ELSE result:=1;
end;
//------------------------------------------------------------------------------
procedure TGameBoard.SetCurrentPlayer (const Value: TBit);
begin
  IF Value=0
    THEN begin
      Player[0].FAmZug:=TRUE;
      Player[1].FAmZug:=FALSE;
    end
    ELSE begin
      Player[0].FAmZug:=FALSE;
      Player[1].FAmZug:=TRUE;
    end;
end;
//------------------------------------------------------------------------------
function TGameBoard.GetGameFinished: Boolean;
begin
  IF (Turn<=8)
    THEN begin
      result:=FALSE;
      exit;
    end;
  IF (Player[0].movable=FALSE) AND (Player[1].movable=FALSE)
    THEN result:=TRUE
    ELSE result:=FALSE; 
end;





//------------------------------------------------------------------------------
//------------------------------------------------------------------------------









//###########################################
//################  TPLAYER  ################
//###########################################


constructor TPlayer.Create(GameBoard:TGameBoard);
var i: 0..3;
begin
  inherited Create;
  self.GameBoard:=GameBoard;
  FOR i:=0 to 3 do
    begin
      self.FPinguin[i]:=TPinguin.Create;
      self.FPinguin[i].GameBoard:=GameBoard;
    end;
end;

//------------------------------------------------------------------------------

procedure TPlayer.SetName(Value:String);
var other: TBit;
begin
  IF self.Number=0 THEN other:=1 ELSE other:=0;
  IF    (Value<>'')
    AND (Value<>GameBoard.Player[other].Name)
      THEN FName:=Value
      ELSE ShowMessage('Angegebener Spielername ist ung�ltig.');
end;
//------------------------------------------------------------------------------
procedure TPlayer.SetCPUControlled(Value: Boolean);
begin
  IF Value=FALSE
    THEN IF DosCommand<>nil THEN SendToClient(2);
  FCPUControlled:=Value;
  //Ereignisse
//  IF Assigned(GameBoard.FOnNewTurn) THEN GameBoard.FOnNewTurn;
end;
//------------------------------------------------------------------------------
procedure TPlayer.SetClientPath(Value: String);
begin
  //Javaclient
  IF JavaClient=TRUE
    THEN begin
      IF DosCommand<>nil THEN SendToClient(2);
      FClientPath:=Value;
      //Clientanwendung im Hintergrund starten
      StartClient;
    end;

  //Delphiclient
  IF (JavaClient=FALSE)
    THEN begin
      IF (CheckClientPath(Value,TRUE)=TRUE)
        THEN begin
          IF DosCommand<>nil THEN SendToClient(2);
          FClientPath:=Value;
          //Clientanwendung im Hintergrund starten
          StartClient;
        end
        ELSE self.FCPUControlled:=FALSE;
    end;
   
  //Ereignisse
  IF Assigned(GameBoard.FOnNewTurn) THEN GameBoard.FOnNewTurn;
end;
//------------------------------------------------------------------------------
function TPlayer.GetMovable: Boolean;
var i: byte;
begin
  result:=FALSE;
  FOR i:=0 to 3 //Sobald auch nur ein Pinguin noch beweglich ist, result=TRUE
    do IF Pinguin[i].Movable=TRUE THEN result:=TRUE;
end;
//------------------------------------------------------------------------------
function TPlayer.GetAllPinguinsPosed: Boolean;
var i: byte;
begin
  result:=TRUE;
  FOR i:=0 to 3 //Sobald auch nur ein Pinguin noch beweglich ist, result=TRUE
    do IF Pinguin[i].Posed=FALSE THEN result:=FALSE;
end;
//------------------------------------------------------------------------------
procedure TPlayer.SetGameBoard(Value: TGameBoard);
begin
  //Wenn noch kein GameBoard zugewiesen wurde, wird FGameBoard=eingegbener Wert gesetzt
  IF GameBoard=nil THEN FGameBoard:=Value;
end;
//------------------------------------------------------------------------------
function TPlayer.GetTimeStats: TClientTimeStats;
begin
  result:=FTimeStats;
  result.Path:=self.ClientPath;
end;





//------------------------------------------------------------------------------

procedure TPlayer.StartClient;
begin
  try
    DosCommand.CommandLine:=GameBoard.Player[Number].ClientPath;
    DosCommand.Execute;
    //Ereignisse
    IF Assigned(GameBoard.FOnNewTurn) THEN GameBoard.FOnNewTurn;
  except
    on Exception do ShowMessage('Fehler beim Starten von Client '+IntToStr(self.Number));
  end;
end;

//------------------------------------------------------------------------------

procedure TPlayer.SendToClient(command: byte);
var s: String;
begin
//"0" - Keine Aktion, der andere Spieler ist an der Reihe
//"1" - Die Connectorschicht erwartet einen Zug
//"2" - Der Spielerclient soll sich beenden
//"3" - Der Spielerclient soll sich zur�cksetzen
  IF GameBoard.Player[Number].CPUControlled=TRUE
    THEN begin
      s:=IntToStr(Command);
      IF Command=1
        THEN begin
          S:=S+' '+GameBoard.EncodeBoard(Number);
          GameBoard.FMyTimer.ResetTime;
          FClientIsDrawing:=TRUE;
          GameBoard.Timer_ClientDraw.Enabled:=TRUE;
        end;
//      ShowMessage('Sent to Client'+IntToStr(self.Number)+': '+s);  test
      DosCommand.SendLine(s,TRUE);
    end;
end;
//------------------------------------------------------------------------------
procedure TPlayer.SendToClient(S: String);
begin
  IF (GameBoard.Player[Number].CPUControlled=TRUE) AND (S<>'')
    THEN begin
      DosCommand.SendLine(s,TRUE);
    end;
end;















//------------------------------------------------------------------------------
//------------------------------------------------------------------------------






//###########################################
//#################  TPINGUIN  ##############
//###########################################


//############  FELDER ABRUFEN  #############

function TPinguin.GetTargetsInDirection(Direction: TDirection): TPointArray;
begin
  result:=GameBoard.GetTargetsInDirection(Position.X,Position.Y,Direction);
end;

//------------------------------------------------------------------------------

function TPinguin.GetAllTargets: TPointArray;
begin
  result:=GameBoard.GetAllTargets(Position.X,Position.Y);
end;

//############  EIGENSCHAFTEN  ##############


function TPinguin.GetMovable: Boolean;
var i: TDirection;
    NextField: TPoint;
begin
  result:=FALSE;
  //�berpr�ft in alle 6 Richtungen, ob noch zumindest ein Feld frei ist.
  FOR i:=0 to 5 do
    begin
      NextField:=GameBoard.GetNextFieldInDirection(Position.X,Position.Y,i);
      IF (GameBoard.Field[NextField.X,NextField.Y].valid=TRUE)
        AND (GameBoard.Field[NextField.X,NextField.Y].Full=FALSE)
          THEN begin
            result:=TRUE;
            exit;
          end;
    end;
end;

//------------------------------------------------------------------------------

function TPinguin.GetFish: byte;
begin
  result:=0;
  IF (Posed=TRUE)
    THEN result:=GameBoard.Field[Position.X,Position.Y].Fish;
end;
//------------------------------------------------------------------------------
function TPinguin.GetAllFish: integer;
begin
  result:=0;
  GameBoard.GetAllFish(Position.X,Position.Y);
end;
//------------------------------------------------------------------------------
procedure TPinguin.SetPosition(Value: TPoint);
begin
  IF (GameBoard.Field[Value.X,Value.Y].valid=TRUE)
      THEN FPosition:=Value;
end;
//------------------------------------------------------------------------------
procedure TPinguin.SetGameBoard(Value: TGameBoard);
begin
  //Wenn noch kein GameBoard zugewiesen wurde, wird FGameBoard=eingegbener Wert gesetzt
  IF GameBoard=nil THEN FGameBoard:=Value;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//###########################################
//###########  KLASSENUNABH�NGIG  ###########
//###########################################

function CheckClientPath(var Path: String; ShowMessages: Boolean): Boolean;
begin
  result:=FALSE;
  IF (Copy(Path,1,4)='java')
    THEN Delete(Path,1,5);
  IF (FileExists(Path)=TRUE)
    THEN begin
      result:=TRUE;
      IF (CheckFileEnding(Path,'.exe',false)=TRUE)
        THEN ;
      IF (CheckFileEnding(Path,'.class',false)=TRUE)
        THEN IF (Copy(Path,1,4)<>'java') THEN Path:='java '+Path;
    end
    ELSE IF ShowMessages=TRUE THEN ShowMessage('F�r Client angegebene Datei existiert nicht.');
end;

//------------------------------------------------------------------------------

function AddTwoTimeStats(const TimeStats0, TimeStats1: TClientTimeStats): TClientTimeStats;
begin
  with result do
    begin
      //ERSTER ZUG
      FirstAverage:=0;
      IF TimeStats0.FirstAverage=0 THEN FirstAverage:=TimeStats1.FirstAverage;
      IF TimeStats1.FirstAverage=0 THEN FirstAverage:=TimeStats0.FirstAverage;
      IF FirstAverage=0
        THEN FirstAverage:=round((TimeStats0.FirstAverage + TimeStats1.FirstAverage) / 2);

      FirstMaximum:=0;
      IF TimeStats0.FirstMaximum=0 THEN FirstMaximum:=TimeStats1.FirstMaximum;
      IF TimeStats1.FirstMaximum=0 THEN FirstMaximum:=TimeStats0.FirstMaximum;
      IF FirstMaximum=0
        THEN begin
          IF TimeStats0.FirstMaximum > TimeStats1.FirstMaximum
            THEN FirstMaximum:=TimeStats0.FirstMaximum
            ELSE begin
              //1>0 oder beide gleich
              IF TimeStats1.FirstMaximum > TimeStats0.FirstMaximum
                THEN FirstMaximum:=TimeStats1.FirstMaximum;
            end;
        end;

      FirstMinimum:=0;
      IF TimeStats0.FirstMinimum=0 THEN FirstMinimum:=TimeStats1.FirstMinimum;
      IF TimeStats1.FirstMinimum=0 THEN FirstMinimum:=TimeStats0.FirstMinimum;
      IF FirstMinimum=0
        THEN begin
          IF TimeStats0.FirstMinimum < TimeStats1.FirstMinimum
            THEN FirstMinimum:=TimeStats0.FirstMinimum
            ELSE begin
              //1<0 oder beide gleich
              IF TimeStats1.FirstMinimum < TimeStats0.FirstMinimum
                THEN FirstMinimum:=TimeStats1.FirstMinimum;
            end;
        end;

      //SONSTIGE Z�GE
      Average:=0;
      IF TimeStats0.Average=0 THEN Average:=TimeStats1.Average;
      IF TimeStats1.Average=0 THEN Average:=TimeStats0.Average;
      IF Average=0
        THEN Average:=round((TimeStats0.Average + TimeStats1.Average) / 2);

      Maximum:=0;
      IF TimeStats0.Maximum=0 THEN Maximum:=TimeStats1.Maximum;
      IF TimeStats1.Maximum=0 THEN Maximum:=TimeStats0.Maximum;
      IF Maximum=0
        THEN begin
          IF TimeStats0.Maximum > TimeStats1.Maximum
            THEN Maximum:=TimeStats0.Maximum
            ELSE begin
              //1>0 oder beide gleich
              IF TimeStats1.Maximum > TimeStats0.Maximum
                THEN Maximum:=TimeStats1.Maximum;
            end;
        end;

      Minimum:=0;
      IF TimeStats0.Minimum=0 THEN Minimum:=TimeStats1.Minimum;
      IF TimeStats1.Minimum=0 THEN Minimum:=TimeStats0.Minimum;
      IF Minimum=0
        THEN begin
          IF TimeStats0.Minimum < TimeStats1.Minimum
            THEN Minimum:=TimeStats0.Minimum
            ELSE begin
              //1<0 oder beide gleich
              IF TimeStats1.Minimum < TimeStats0.Minimum
                THEN Minimum:=TimeStats1.Minimum;
            end;
        end;

      //GESAMTES SPIEL
      
      SumAverage:=0;
      IF TimeStats0.SumAverage=0 THEN SumAverage:=TimeStats1.SumAverage;
      IF TimeStats1.SumAverage=0 THEN SumAverage:=TimeStats0.SumAverage;
      IF SumAverage=0
        THEN SumAverage:=round((TimeStats0.SumAverage*TimeStats0.NumberOfGames + TimeStats1.SumAverage*TimeStats1.NumberOfGames)
                                                  / (TimeStats0.NumberOfGames+TimeStats1.NumberOfGames));

      NumberOfGames:=TimeStats0.NumberOfGames+TimeStats1.NumberOfGames;

      SumMaximum:=0;
      IF TimeStats0.SumMaximum=0 THEN Maximum:=TimeStats1.SumMaximum;
      IF TimeStats1.SumMaximum=0 THEN Maximum:=TimeStats0.SumMaximum;
      IF SumMaximum=0
        THEN begin
          IF TimeStats0.SumMaximum > TimeStats1.SumMaximum
            THEN SumMaximum:=TimeStats0.SumMaximum
            ELSE begin
              //1>0 oder beide gleich
              SumMaximum:=TimeStats1.SumMaximum;
            end;
        end;

      SumMinimum:=0;
      IF TimeStats0.SumMinimum=0 THEN SumMinimum:=TimeStats1.SumMinimum;
      IF TimeStats1.SumMinimum=0 THEN SumMinimum:=TimeStats0.SumMinimum;
      IF SumMinimum=0
        THEN begin
          IF TimeStats0.SumMinimum < TimeStats1.SumMinimum
            THEN SumMinimum:=TimeStats0.SumMinimum
            ELSE begin
              //1<0 oder beide gleich
              IF TimeStats1.SumMinimum < TimeStats0.SumMinimum
                THEN SumMinimum:=TimeStats1.SumMinimum;
            end;
        end;
    end;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

initialization //Wird ganz am Anfang bei Initialisierung der Unit aufgerufen

  randomize;

end.
