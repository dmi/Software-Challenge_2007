unit Unit_Server_Testreihe_AddClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Unit_TGameBoard;

type
  TBit = 0..1;
  TForm_Testreihe_AddClient = class(TForm)
    Ed_ClientPath: TEdit;
    Btn_OpenDialog: TButton;
    La_ClientPath: TLabel;
    Ed_Name: TEdit;
    La_Name: TLabel;
    OpenDialog_ClientPath: TOpenDialog;
    La_Title: TLabel;
    Btn_OK: TButton;
    Btn_Cancel: TButton;
    CB_Java: TCheckBox;
    Btn_CountUp: TButton;
    Btn_DN: TButton;
    procedure Btn_DNClick(Sender: TObject);
    procedure Btn_CountUpClick(Sender: TObject);
    procedure OpenDialog_ClientPathCanClose(Sender: TObject;
      var CanClose: Boolean);
    procedure Btn_OpenDialogClick(Sender: TObject);
    procedure Ed_ClientPathKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Ed_NameKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Submit(Sender: TObject); //�bertr�gt den Eintrag
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

  function AddClientToList(Player: TBit; Name,Path: String; Java, Check: Boolean): Boolean;
            //Check gibt an, ob Name und Pfad auf Richtigkeit gepr�ft werden sollen
  function CheckDoubleName(const Name: String): Boolean; //Name doppelt?

var
  Form_Testreihe_AddClient: TForm_Testreihe_AddClient;
  PlayerGlobal: TBit;

implementation

uses Unit_Server_Testreihe, Unit_CustomProc;

{$R *.dfm}

procedure TForm_Testreihe_AddClient.Submit(Sender: TObject);
begin
  IF AddClientToList(PlayerGlobal,Ed_Name.Text,Ed_ClientPath.Text,CB_Java.Checked,TRUE)=TRUE
    THEN close;
end;
//------------------------------------------------------------------------------
procedure TForm_Testreihe_AddClient.Btn_CancelClick(Sender: TObject);
begin
  close;
end;
//------------------------------------------------------------------------------
procedure TForm_Testreihe_AddClient.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form_Testreihe.ListBoxSelect(Sender);
  Form_Testreihe.Enabled:=TRUE;
end;
//------------------------------------------------------------------------------
procedure TForm_Testreihe_AddClient.FormShow(Sender: TObject);
var S: String;
begin
  IF (Ed_Name.Text='') AND (Ed_ClientPath.Text='')
    THEN begin
      IF ((PlayerGlobal=0) AND (Form_TestReihe.ListBox_Player0.ItemIndex>=0))
        OR ((PlayerGlobal=1) AND (Form_TestReihe.ListBox_Player1.ItemIndex=-1) AND (Form_TestReihe.ListBox_Player0.ItemIndex>=0))
        THEN begin
          Ed_Name.Text:=Client0[Form_TestReihe.ListBox_Player0.ItemIndex].Name;
          Ed_ClientPath.Text:=Client0[Form_TestReihe.ListBox_Player0.ItemIndex].Path;
        end;
      IF (PlayerGlobal=1) AND (Form_TestReihe.ListBox_Player1.ItemIndex>=0)
        OR ((PlayerGlobal=0) AND (Form_TestReihe.ListBox_Player0.ItemIndex=-1) AND (Form_TestReihe.ListBox_Player1.ItemIndex>=0))
        THEN begin
          Ed_Name.Text:=Client1[Form_TestReihe.ListBox_Player1.ItemIndex].Name;
          Ed_ClientPath.Text:=Client1[Form_TestReihe.ListBox_Player1.ItemIndex].Path;
        end;
    end;
  //Doppelte umbenennen
  IF Ed_Name.Text<>''
    THEN begin
      S:=Ed_Name.Text;
      WHILE CheckDoubleName(S)=TRUE
        do CountUp(S,'_');
      Ed_Name.Text:=S;
    end;
  //Markieren
  Ed_Name.SelectAll;
  Ed_Name.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TForm_Testreihe_AddClient.Ed_NameKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key=13 THEN Ed_ClientPath.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TForm_Testreihe_AddClient.Ed_ClientPathKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key=13 THEN Btn_OK.SetFocus;
end;


//------------------------------------------------------------------------------

function AddClientToList(Player: TBit; Name,Path: String; Java, Check: Boolean): Boolean;
var i: integer;
begin
  result:=FALSE;
  IF Check=TRUE
    THEN begin
      //�berpr�fen, ob Name doppelt und ob Pfad korrekt
      IF CheckDoubleName(Name)=TRUE
        THEN ShowMessage('Es existiert bereits ein Client mit diesem Namen.');
      IF (Name='') OR (Name=' ') OR (Name='  ') OR (Name='   ') OR (Name='    ')
        THEN begin
          ShowMessage('Bitte geben Sie einen Namen f�r den neuen Client ein!');
          exit;
        end;
      IF (Java=FALSE) AND (CheckClientPath(Path,Check)=FALSE)
        THEN exit;
    end;
  //Hinzuf�gen
  IF Player=0
    THEN begin
      i:=Form_Testreihe.Listbox_Player0.Count;
//      i:=0;  //i erh�lt den Index des neuen Strings
      Client0[i].Name:=Name;
      Client0[i].Path:=Path;
      Client0[i].Java:=Java;
      Form_Testreihe.Listbox_Player0.Items.Add(Name);
      Form_Testreihe.Listbox_Player0.ItemIndex:=i;
      Form_Testreihe.ListBoxSelect(Form_Testreihe.ListBox_Player0);
      result:=TRUE;
    end;
  IF Player=1
    THEN begin
      i:=Form_Testreihe.Listbox_Player1.Count;
      Client1[i].Name:=Name;
      Client1[i].Path:=Path;
      Client1[i].Java:=Java;
      Form_Testreihe.Listbox_Player1.Items.Add(Name);
      Form_Testreihe.Listbox_Player1.ItemIndex:=i;
      Form_Testreihe.ListBoxSelect(Form_Testreihe.ListBox_Player1);
      result:=TRUE;
    end;       
end;


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


procedure TForm_Testreihe_AddClient.Btn_OpenDialogClick(Sender: TObject);
begin
  OpenDialog_ClientPath.Execute;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe_AddClient.OpenDialog_ClientPathCanClose(
  Sender: TObject; var CanClose: Boolean);
var S: String;
begin
  Ed_ClientPath.Text:=OpenDialog_ClientPath.Filename;
  IF Ed_Name.Text=''
    THEN begin
      Ed_Name.Text:=RemoveLastPartFromFilePath( GetLastPartOfFilePath(Ed_ClientPath.Text));
      S:=Ed_Name.Text;
      WHILE CheckDoubleName(S)=TRUE
        do CountUp(S,'_');
      Ed_Name.Text:=S;
    end;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe_AddClient.Btn_CountUpClick(Sender: TObject);
var Name:String;
begin
  Name:=Ed_Name.Text;
  CountUp(Name,'_');
  Ed_Name.Text:=Name;
end;

//------------------------------------------------------------------------------

procedure TForm_Testreihe_AddClient.Btn_DNClick(Sender: TObject);
var S: String;
begin
  Ed_Name.Text:=RemoveLastPartFromFilePath( GetLastPartOfFilePath(Ed_ClientPath.Text));
  S:=Ed_Name.Text;
  WHILE CheckDoubleName(S)=TRUE
    do CountUp(S,'_');
  Ed_Name.Text:=S;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


function CheckDoubleName(const Name: String): Boolean;
var i: Integer;
begin
  result:=FALSE;
  FOR i:=0 to High(Client0) do
    IF Client0[i].Name=Name
      THEN result:=TRUE;
  FOR i:=0 to High(Client1) do
    IF Client1[i].Name=Name
      THEN result:=TRUE;
end;



end.
