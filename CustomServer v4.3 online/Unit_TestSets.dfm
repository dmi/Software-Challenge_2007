object Form_TestSets: TForm_TestSets
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Testreihen - Einstellungen'
  ClientHeight = 173
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox: TGroupBox
    Left = 8
    Top = 8
    Width = 337
    Height = 125
    TabOrder = 1
    object La_GameSum: TLabel
      Left = 40
      Top = 34
      Width = 83
      Height = 13
      Caption = 'Spiele insgesamt:'
    end
    object La_Games: TLabel
      Left = 8
      Top = 18
      Width = 162
      Height = 13
      Caption = 'Anzahl der Spiele pro Begegnung:'
    end
    object La_Wait: TLabel
      Left = 8
      Top = 56
      Width = 102
      Height = 13
      Caption = 'Verz'#246'gerter Zug: aus'
    end
    object La_AfterGame: TLabel
      Left = 9
      Top = 72
      Width = 176
      Height = 13
      Caption = 'Nach jedem Spiel: Clients neustarten'
    end
    object La_StartPlayer: TLabel
      Left = 8
      Top = 88
      Width = 62
      Height = 13
      Caption = 'Startspieler: '
    end
    object La_SavePath: TLabel
      Left = 8
      Top = 104
      Width = 70
      Height = 13
      Caption = 'Speicherpfad: '
    end
    object Ed_SavePath: TEdit
      Left = 80
      Top = 104
      Width = 249
      Height = 13
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      Text = 'Ed_SavePath'
    end
  end
  object Btn_OK: TButton
    Left = 136
    Top = 144
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = Btn_OKClick
  end
end
