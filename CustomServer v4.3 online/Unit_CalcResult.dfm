object Form_CalcResult: TForm_CalcResult
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Testreihen - Auswertung'
  ClientHeight = 404
  ClientWidth = 542
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object La_Player1Name: TLabel
    Left = 216
    Top = 16
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Spieler 1:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object La_Player0Name: TLabel
    Left = 216
    Top = 0
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Spieler 0:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object La_Player0Path: TLabel
    Left = 264
    Top = 0
    Width = 75
    Height = 13
    Caption = 'La_Player0Path'
    OnDblClick = La_PathClick
  end
  object La_Player1Path: TLabel
    Left = 264
    Top = 16
    Width = 75
    Height = 13
    Caption = 'La_Player1Path'
    OnDblClick = La_PathClick
  end
  object SG_Result: TStringGrid
    Left = 8
    Top = 96
    Width = 505
    Height = 300
    ColCount = 32
    DefaultColWidth = 100
    RowCount = 32
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    TabOrder = 8
    OnSelectCell = SG_ResultSelectCell
  end
  object RG_Player0: TRadioGroup
    Left = 8
    Top = 32
    Width = 121
    Height = 57
    Caption = 'Anzeige - Spieler 0'
    ItemIndex = 0
    Items.Strings = (
      'Siege'
      'Siege (prozentual)'
      'Vorsprung')
    TabOrder = 0
    OnClick = RadioGroupSelection
  end
  object RG_Player1: TRadioGroup
    Left = 136
    Top = 32
    Width = 121
    Height = 57
    Caption = 'Anzeige - Spieler 1'
    Items.Strings = (
      'Siege'
      'Siege (prozentual)'
      'Vorsprung')
    TabOrder = 1
    OnClick = RadioGroupSelection
  end
  object Btn_Save: TButton
    Left = 448
    Top = 40
    Width = 73
    Height = 22
    Caption = 'Speichern'
    TabOrder = 6
    OnClick = Btn_SaveLoadClick
  end
  object Btn_Load: TButton
    Left = 448
    Top = 66
    Width = 73
    Height = 22
    Caption = #214'ffnen'
    TabOrder = 7
    OnClick = Btn_SaveLoadClick
  end
  object CB_Colorize: TCheckBox
    Left = 264
    Top = 72
    Width = 73
    Height = 17
    Caption = 'Einf'#228'rben'
    Enabled = False
    TabOrder = 3
    Visible = False
    OnClick = CB_ColorizeClick
  end
  object Btn_TimeStats: TButton
    Left = 374
    Top = 40
    Width = 67
    Height = 22
    Caption = 'Zeiten'
    TabOrder = 4
    OnClick = Btn_TimeStatsClick
  end
  object Btn_TestSettings: TButton
    Left = 374
    Top = 66
    Width = 67
    Height = 22
    Caption = 'Einstellungen'
    TabOrder = 5
    OnClick = Btn_TestSettingsClick
  end
  object RG_General: TRadioGroup
    Left = 264
    Top = 32
    Width = 105
    Height = 45
    Caption = 'Allgemeines'
    Items.Strings = (
      #220'brige Schollen'
      'Unentschieden')
    TabOrder = 2
    OnClick = RadioGroupSelection
  end
  object OpenDialog: TOpenDialog
    Filter = 'Testreihen-Ergebnisse(*.cst)|*.cst'
    InitialDir = 'Saves'
    OnCanClose = DialogCanClose
    Left = 504
    Top = 64
  end
  object SaveDialog: TSaveDialog
    Filter = 'Testreihen-Ergebnisse(*.cst)|*.cst'
    InitialDir = 'Saves'
    OnCanClose = DialogCanClose
    Left = 504
    Top = 32
  end
end
