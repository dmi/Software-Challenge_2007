object Form_Testreihe_AddClient: TForm_Testreihe_AddClient
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Client hinzuf'#252'gen'
  ClientHeight = 133
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object La_ClientPath: TLabel
    Left = 16
    Top = 76
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pfad:'
  end
  object La_Name: TLabel
    Left = 11
    Top = 44
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Name:'
  end
  object La_Title: TLabel
    Left = 168
    Top = 10
    Width = 98
    Height = 16
    Caption = 'Client hinzuf'#252'gen'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Ed_ClientPath: TEdit
    Left = 48
    Top = 74
    Width = 321
    Height = 21
    TabOrder = 1
    OnKeyUp = Ed_ClientPathKeyUp
  end
  object Btn_OpenDialog: TButton
    Left = 376
    Top = 74
    Width = 25
    Height = 21
    Caption = '...'
    TabOrder = 2
    OnClick = Btn_OpenDialogClick
  end
  object Ed_Name: TEdit
    Left = 48
    Top = 42
    Width = 145
    Height = 21
    TabOrder = 0
    OnKeyUp = Ed_NameKeyUp
  end
  object Btn_OK: TButton
    Left = 232
    Top = 104
    Width = 89
    Height = 25
    Caption = 'OK'
    TabOrder = 3
    OnClick = Submit
  end
  object Btn_Cancel: TButton
    Left = 328
    Top = 104
    Width = 89
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 4
    OnClick = Btn_CancelClick
  end
  object CB_Java: TCheckBox
    Left = 320
    Top = 52
    Width = 49
    Height = 17
    Caption = 'Java'
    TabOrder = 5
  end
  object Btn_CountUp: TButton
    Left = 200
    Top = 44
    Width = 18
    Height = 18
    Caption = '+'
    TabOrder = 6
    OnClick = Btn_CountUpClick
  end
  object Btn_DN: TButton
    Left = 224
    Top = 44
    Width = 18
    Height = 18
    Hint = 'Dateiname'
    Caption = 'DN'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    OnClick = Btn_DNClick
  end
  object OpenDialog_ClientPath: TOpenDialog
    Filter = 'PaP-Client (*.exe)|*.exe|Alle Dateien|*'
    InitialDir = 'Clients'
    OnCanClose = OpenDialog_ClientPathCanClose
    Left = 376
    Top = 40
  end
end
