unit Unit_Server_About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, jpeg, Shellapi;

type
  TForm_About = class(TForm)
    Img_Packeis: TImage;
    La_Caption: TLabel;
    La_Text: TLabel;
    La_Version_Caption: TLabel;
    La_Text_Caption: TLabel;
    La_Version_Main: TLabel;
    La_Contact_Caption: TLabel;
    La_Mail: TLabel;
    GroupBox1: TGroupBox;
    La_Licence_Caption: TLabel;
    La_OpenSource: TLabel;
    Btn_Quit: TButton;
    procedure Btn_QuitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure La_MailClick(Sender: TObject);
    procedure La_MailMouseLeave(Sender: TObject);
    procedure La_MailMouseEnter(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form_About: TForm_About;

implementation

uses unit_Server_Main;

{$R *.dfm}

procedure TForm_About.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Form_Server.Enabled:=TRUE;
end;

procedure TForm_About.La_MailMouseEnter(Sender: TObject);
begin
  La_Mail.Font.Color:=clRed;
end;

procedure TForm_About.La_MailMouseLeave(Sender: TObject);
begin
  La_Mail.Font.Color:=clBlue;
end;

procedure TForm_About.La_MailClick(Sender: TObject);
var command: string;
begin
Command := 'mailto:'+La_Mail.Caption+'?subject=PaP-Server';
  ShellExecute(0, nil, PChar(Command), nil, nil, SW_SHOWNORMAL);end;

procedure TForm_About.FormShow(Sender: TObject);
begin
  La_Version_Main.Caption:='Server: '+Unit_Server_Main.Version;
end;

procedure TForm_About.Btn_QuitClick(Sender: TObject);
begin
  close;
end;

end.

